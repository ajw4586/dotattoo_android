package com.dotattoo.android.net.req;

import com.dotattoo.android.net.core.RetrofitFactory;
import com.dotattoo.android.net.res.Common;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by ajw45 on 2016-03-28.
 */
public class PushService {

    private static Push instance;

    public interface Push {

        @POST("/push/register")
        @FormUrlEncoded
        Call<Common> registerPushToken(@Field("registration_id") String pushToken,
                                       @Field("access_token") String accessToken);
    }

    public static Push getInstance() {
        return instance != null ? instance : create();
    }

    private static Push create() {
        instance = RetrofitFactory.getInstance().getRetrofit().create(Push.class);
        return instance;
    }
}
