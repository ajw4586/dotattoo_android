package com.dotattoo.android.net.req;

import com.dotattoo.android.net.core.RetrofitFactory;
import com.dotattoo.android.net.res.Profile;
import com.dotattoo.android.net.res.Session;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by ajw45 on 2016-01-27.
 */
public class AuthService {


    public static final String FACEBOOK = "facebook";
    public static final String KAKAO = "kakao";

    private static final String KAKAO_OAUTH_REDIRECT = "http://52.79.95.175/login";

    private static Auth instance;

    public interface Auth {
        @POST("/login")
        @FormUrlEncoded
        Call<Session> fbLogin(@Field("type") String loginType,
                              @Field("access_token") String accessToken,
                              @Field("user_id") String userId);

        @POST("/login")
        @FormUrlEncoded
        Call<Session> kakaoLogin(@Field("type") String loginType,
                                 @Field("access_token") String accessToken);

        @GET("/user/{user_id}")
        Call<Profile> getInfo(@Path("user_id") String userId);
    }

    public static Auth getInstance() {
        return instance != null ? instance : create();
    }

    private static Auth create() {
        instance = RetrofitFactory.getInstance().getRetrofit().create(Auth.class);
        return instance;
    }
}
