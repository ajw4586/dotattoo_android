package com.dotattoo.android.net.core;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ajw45 on 2016-02-01.
 */
public class RetrofitFactory {

    public static final String ENDPOINT = "http://" + "52.79.95.175";

    private static RetrofitFactory instance;

    private Retrofit retrofit;


    public static RetrofitFactory getInstance() {
        return instance != null ? instance : new RetrofitFactory();
    }

    private RetrofitFactory() {
        HttpLoggingInterceptor logger = new HttpLoggingInterceptor();
        logger.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder client = new OkHttpClient.Builder()
                .addInterceptor(logger);

        retrofit = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client.build())
                .build();
        instance = this;
    }


    public Retrofit getRetrofit() {
        return retrofit;
    }

}
