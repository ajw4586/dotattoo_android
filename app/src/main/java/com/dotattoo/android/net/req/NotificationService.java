package com.dotattoo.android.net.req;

import com.dotattoo.android.net.core.RetrofitFactory;
import com.dotattoo.android.net.res.Notifications;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by ajw45 on 2016-06-07.
 */
public class NotificationService {

    private static Notification instance;

    public interface Notification {

        @GET("/notification")
        Call<Notifications> getNotifications(@Query("access_token") String accessToken);
    }

    public static Notification getInstance() {
        return instance != null ? instance : create();
    }

    public static Notification create() {
        return instance = RetrofitFactory.getInstance().getRetrofit().create(Notification.class);
    }


}
