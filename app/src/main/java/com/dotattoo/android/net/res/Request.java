package com.dotattoo.android.net.res;

/**
 * Created by ajw45 on 2016-04-03.
 */
public class Request extends Common {

    public RequestData data;

    public class RequestData {
        public String _id;
        public Member tattooist_id;
        public Member user_id;
        public Info info;
        public String time;

        public class Info {
            public String photo;
            public String tattoo_name;
            public String genre;
            public String place;
            public String part;
            public String date;
            public String term;
            public int price;
            public int reservation_price;
        }
    }
}
