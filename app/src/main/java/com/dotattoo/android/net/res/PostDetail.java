package com.dotattoo.android.net.res;

import java.util.List;

/**
 * Created by ajw45 on 2016-02-28.
 */
public class PostDetail extends Common {

    public List<Post> data;

    public static class Post {
        public String _id;
        public String category;
        public String title;
        public String comment;
        public String part;
        public String tatooist;
        public String photo_url;
        public Reaction reaction;
        public Timeline.Post.Info info;

        public class Reaction {
            //  provided when access_token passed to API.
            public boolean liked;
            public List<String> share;
            public List<String> like;
            public List<Timeline.Comment> comment;
        }
    }

}
