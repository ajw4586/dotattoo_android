package com.dotattoo.android.net.req;

import com.dotattoo.android.net.core.RetrofitFactory;
import com.dotattoo.android.net.res.CommentList;
import com.dotattoo.android.net.res.Common;
import com.dotattoo.android.net.res.PostDetail;
import com.dotattoo.android.net.res.Timeline;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by ajw45 on 2016-01-28.
 */
public class PostService {


    public static final String SORT_LATEST = "latest";
    public static final String SORT_POPULAR = "popular";

    private static Post instance;

    public interface Post {

        @GET("/timeline")
        Call<Timeline> getTimeline(@Query("access_token") String accessToken);

        /**
         * Get post information without liked value.
         * @param postId want to get post id.
         * @return able to call method instance
         */
        @GET("/post/{post_id}")
        Call<PostDetail> getPost(@Path("post_id") String postId);

        /**
         * Get post information with liked value.
         * Recommend to use this method than non-access_token method.
         * @param postId want to get post_id.
         * @param accessToken current user's access_token
         * @return able to call method instance
         */
        @GET("/post/{post_id}")
        Call<PostDetail> getPost(@Path("post_id") String postId,
                                 @Query("access_token") String accessToken);

        @DELETE("/post/{post_id}")
        Call<Common> deletePost(@Path("post_id") String postId,
                                    @Query("access_token") String accessToken);

        @POST("/post")
        @Multipart
        Call<Common> uploadPost(@Part MultipartBody.Part filename,
                                @Part("access_token") RequestBody accessToken,
                                @Part("comment") RequestBody comment,
                                @Part("part") RequestBody part,
                                @Part("title") RequestBody title);

        @GET("liked_post")
        Call<Timeline> getLikedPost(@Query("access_token") String accessToken);

        @POST("/post/{id}/like")
        @FormUrlEncoded
        Call<Common> like(@Path("id") String postId,
                             @Field("access_token") String accessToken);

        @DELETE("/post/{id}/like")
        Call<Common> undoLike(@Path("id") String postId,
                              @Query("access_token") String accessToken);

        @POST("/post/{id}/comment")
        @FormUrlEncoded
        Call<Common> comment(@Path("id") String postId,
                          @Field("access_token") String accessToken,
                          @Field("comment") String comment);

        @GET("/post/{id}/comment")
        Call<CommentList> getPostComment(@Path("id") String postId);

        @DELETE("/post/{id}/comment")
        Call<Common> deleteComment(@Path("id") String postId,
                                   @Query("access_token") String accessToken);
    }

    public static Post getInstance() {
        return instance != null ? instance : create();
    }

    private static Post create() {
        return instance = RetrofitFactory.getInstance().getRetrofit().create(Post.class);
    }
}
