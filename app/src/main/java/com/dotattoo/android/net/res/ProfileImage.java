package com.dotattoo.android.net.res;

/**
 * Created by ajw45 on 2016-07-02.
 */
public class ProfileImage extends Common {

    public ProfileUrl data;

    public class ProfileUrl {
        public String profile_img;
    }
}
