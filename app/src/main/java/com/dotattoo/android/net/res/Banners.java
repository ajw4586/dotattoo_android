package com.dotattoo.android.net.res;

import java.util.List;

/**
 * Created by ajw45 on 2016-06-03.
 */
public class Banners extends Common {

    public List<Banner> data;

    public static class Banner {
        public String _id;
        public String img_url;
        public String web_link;
    }
}
