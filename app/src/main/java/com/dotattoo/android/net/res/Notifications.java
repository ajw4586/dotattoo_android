package com.dotattoo.android.net.res;

import java.util.List;

/**
 * Created by ajw45 on 2016-04-06.
 */
public class Notifications extends Common {

    public List<Notification> data;

    public static class Notification {
        public String path;
        public Member user_id;
        public Member target_user_id;
        public String action;
        public String time;
    }
}
