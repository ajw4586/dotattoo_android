package com.dotattoo.android.net.req;

import com.dotattoo.android.net.core.RetrofitFactory;
import com.dotattoo.android.net.res.Common;
import com.dotattoo.android.net.res.Profile;
import com.dotattoo.android.net.res.ProfileImage;
import com.dotattoo.android.net.res.TopUser;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by ajw45 on 2016-02-09.
 */
public class MemberService {


    private static Member instance;

    public interface Member {
        @GET("/user/{user_id}")
        Call<Profile> getUserInfo(@Path("user_id") String userId
                                  //        TODO Add this soon.
                                  /**@Query("access_token") String accessToken*/);

        @POST("/user/{user_id}/update")
        @FormUrlEncoded
        Call<Common> updateUserInfo(@Path("user_id") String userId,
                                    @Field("access_token") String accessToken,
                                    @FieldMap Map<String ,String> info);

        @POST("/user/{user_id}/update/image")
        @Multipart
        Call<ProfileImage> updateUserProfileImage(@Path("user_id") String userId,
                                                  @Part MultipartBody.Part photo,
                                                  @Part("access_token") RequestBody accessToken);

        @GET("/users/top")
        Call<TopUser> getTopUser();
    }

    public static Member getInstance() {
        return instance != null ? instance : create();
    }

    private static Member create() {
        instance = RetrofitFactory.getInstance().getRetrofit().create(Member.class);
        return instance;
    }
}
