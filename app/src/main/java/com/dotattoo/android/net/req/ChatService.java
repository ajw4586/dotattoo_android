package com.dotattoo.android.net.req;

import com.dotattoo.android.net.core.RetrofitFactory;
import com.dotattoo.android.net.res.ChatList;
import com.dotattoo.android.net.res.ChatRoom;
import com.dotattoo.android.net.res.SendResult;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by ajw45 on 2016-03-01.
 */
public class ChatService {

    private static Chat instance;

    public interface Chat {

        /**
         * Get chatting history between specified user.
         *
         * @param accessToken use to check user request is valid.
         * @return chat list
         */
        @GET("/message/{user_id}")
        Call<ChatList> getMessageHistory(@Path("user_id") String userId,
                                       @Query("access_token") String accessToken);

        @GET("/message/list")
        Call<ChatRoom> getChatRoom(@Query("access_token") String accessToken);

        @POST("/message/text")
        @FormUrlEncoded
        Call<SendResult> sendTextMessage(@Field("target_user_id") String userId,
                                         @Field("access_token") String accessToken,
                                         @Field("message") String messageContent);

        @POST("/message/app_link")
        @FormUrlEncoded
        Call<SendResult> sendAppLinkMessage(@Field("target_user_id") String userId,
                                            @Field("access_token") String accessToken,
                                            @Field("link") String link);

        @POST("/message/photo")
        @Multipart
        Call<SendResult> sendPhotoMessage(@Part("target_user_id") RequestBody userId,
                                      @Part("access_token") RequestBody accessToken,
                                      @Part("message\"; filename=\"message_photo.jpg\" ")RequestBody photoContent);
    }

    public static Chat getInstance() {
        return instance != null ? instance : create();
    }

    private static Chat create() {
        return instance = RetrofitFactory.getInstance().getRetrofit().create(Chat.class);
    }
}
