package com.dotattoo.android.net.req;

import com.dotattoo.android.net.core.RetrofitFactory;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by ajw45 on 2016-02-10.
 */
public class SearchService {

    public static final String SEARCH_POST = "post";
    public static final String SEARCH_USER = "user";

    private static Search instance;

    public interface Search {
        @GET("/search?search_type=" + SEARCH_POST)
        Call<com.dotattoo.android.net.res.Search.PostSearchResult> searchPost(@Query("key") String query,
                                                                              @Query("access_token") String accessToken);

        @GET("/search?search_type=" + SEARCH_USER)
        Call<com.dotattoo.android.net.res.Search.MemberSearchResult> searchMember(@Query("key") String query,
                                                                                  @Query("access_token") String accessToken);
    }

    public static Search getInstance() {
        return instance != null ? instance : create();
    }

    private static Search create() {
        return RetrofitFactory.getInstance().getRetrofit().create(Search.class);
    }
}
