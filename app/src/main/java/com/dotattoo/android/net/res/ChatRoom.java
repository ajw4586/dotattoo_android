package com.dotattoo.android.net.res;

import java.util.List;

/**
 * Created by ajw45 on 2016-03-06.
 */
public class ChatRoom extends Common {

    public List<Room> data;

    public class Room {
        public Member from;
        public Member to;
        public String message;
        public String type;
        public String time;
    }
}
