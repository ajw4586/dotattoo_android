package com.dotattoo.android.net.res;

import java.util.List;

/**
 * Created by ajw45 on 2016-03-01.
 */
public class ChatList extends Common {

    public List<Chat> data;

    public static class Chat {

        public String from;
        public String to;
        public String type;
        //  maybe it can be a photo_url or plain text.
        //  It depends on type value.
        public String message;
        public String time;
        public AppLink app_link;
        public Request request;
    }

    public static class AppLink {
        public String type;
        public String link;
    }

    public static class Request {
        public String photo;
        public String name;
        public String tattooistName;
        public String date;
        public String price;
    }
}
