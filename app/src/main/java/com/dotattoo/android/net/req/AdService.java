package com.dotattoo.android.net.req;

import com.dotattoo.android.net.core.RetrofitFactory;
import com.dotattoo.android.net.res.Banners;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by ajw45 on 2016-06-30.
 */
public class AdService {


    private static Ad instance;

    public interface Ad {

        @GET("/banners")
        Call<Banners> getBanners(@Query("access_token") String accessToken);
    }


    public static Ad getInstance() {
        return instance != null ? instance : create();
    }

    private static Ad create() {
        instance = RetrofitFactory.getInstance().getRetrofit().create(Ad.class);
        return instance;
    }

}
