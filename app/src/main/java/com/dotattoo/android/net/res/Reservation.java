package com.dotattoo.android.net.res;

/**
 * Created by ajw45 on 2016-04-03.
 */
public class Reservation extends Common {

    public String _id;
    public Request request;
    public PurchaseUserInfo purchase_user_info;

    class PurchaseUserInfo {
        public String birthday;
        public String phone_num;
        public String user_name;
    }
}
