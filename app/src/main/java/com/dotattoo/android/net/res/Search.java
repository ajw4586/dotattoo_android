package com.dotattoo.android.net.res;

import java.util.ArrayList;

/**
 * Created by ajw45 on 2016-02-10.
 */
public class Search {

    public class PostSearchResult extends Common {
        public ArrayList<SearchPost> data;
    }
    public class MemberSearchResult extends Common {
        public ArrayList<SearchMember> data;
    }

    public static class SearchPost {
        public String _id;
        public String photo_url;
        public String title;
        public Timeline.Post.Info info;
    }

    public static class SearchMember {
        public String _id;
        public String profile_img;
        public String name;
    }
}
