package com.dotattoo.android.net.res;

import java.util.List;

/**
 * Created by ajw45 on 2016-01-28.
 */
public class Timeline extends Common {

    public List<Post> data;

    public static class Post {
        public String _id;
        public String category;
        public String title;
        public String comment;
        public String part;
        public String photo_url;
        public TimelineReaction reaction;
        public Info info;

        public class TimelineReaction {
            public boolean liked;
            public List<String> like;
            public List<BasicComment> comment;
            public List<String> share;
        }

        public class Reaction {
            //  provided when access_token passed to API.
            public boolean liked;
            public List<Member> share;
            public List<Member> like;
            public List<Comment> comment;
            //public List share;
        }
        public class Info {
            public Member user_id;
            public String post_time;
        }
    }

    public static class Comment {
        public String comment;
        public Member commentBy;
        public String comment_time;
    }

    public static class BasicComment {
        public String comment;
        public String commentBy;
        public String comment_time;
    }

}
