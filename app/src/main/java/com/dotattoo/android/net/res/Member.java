package com.dotattoo.android.net.res;

/**
 * Created by ajw45 on 2016-02-10.
 */
public class Member extends Common {


    public String _id;
    public String name;
    public String profile_img;
    public String comment;
    public String phone;
    public String type;
    public Tattooist tattooist;

    public static class Tattooist {
        public String comment;
        public String place;
        public Status status;

        public static class Status {
            public boolean show_contact;
            public boolean show_consult;
        }
    }
}
