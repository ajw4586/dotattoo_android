package com.dotattoo.android.net.req;

import com.dotattoo.android.net.core.RetrofitFactory;
import com.dotattoo.android.net.res.Common;
import com.dotattoo.android.net.res.Request;
import com.dotattoo.android.net.res.Requests;
import com.dotattoo.android.net.res.Reservation;
import com.dotattoo.android.net.res.Reservations;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by ajw45 on 2016-04-03.
 */
public class TattooService {


    private static Tattoo instance;

    public interface Tattoo {

        @GET("/tattoo/reservation/list")
        Call<Reservations> getReservationList(@Query("access_token") String accessToken);

        @GET("/tattoo/request/list")
        Call<Requests> getRequestList(@Query("access_token") String accessToken);

        @GET("/tattoo/reservation")
        Call<Reservation> getReservation(@Query("reservation_id") String reservationId);

        @POST("/tattoo/reservation")
        @FormUrlEncoded
        Call<Common> reservation(@Field("access_token") String accessToken,
                                 @Field("request_id") String requestId,
                                 @Field("phone_num") String phoneNum,
                                 @Field("birthday") String birthday,
                                 @Field("user_name") String userName);

        @GET("/tattoo/request")
        Call<Request> getRequest(@Query("request_id") String requestId);

        @POST("/tattoo/request")
        @Multipart
        Call<Common> request(@Part("access_token") RequestBody accessToken,
                             @Part("tattooist_id") RequestBody tattooistId,
                             @Part("user_id") RequestBody userId,
                             @Part("photo\": filename=\"dotattoo\"") RequestBody photo,
                             @Part("tattoo_name") RequestBody tattooName,
                             @Part("genre") RequestBody genre,
                             @Part("place") RequestBody place,
                             @Part("part") RequestBody part,
                             @Part("date") RequestBody date,
                             @Part("term") RequestBody term,
                             @Part("price") RequestBody price);

        @POST("tattoo/tattooist/request")
        @FormUrlEncoded
        Call<Common> requestTattooist(@Field("access_token") String accessToken,
                                      @Field("real_name") String realName,
                                      @Field("place") String place,
                                      @Field("address") String address,
                                      @Field("email") String email,
                                      @Field("phone") String phone,
                                      @Field("introduction") String introduction,
                                      @Field("bank_owner") String bankOwner,
                                      @Field("bank_name") String bankNamer,
                                      @Field("bank_account") String bankAccount);
    }

    public static Tattoo getInstance() {
        return instance != null ? instance : create();
    }

    private static Tattoo create() {
        instance = RetrofitFactory.getInstance().getRetrofit().create(Tattoo.class);
        return instance;
    }

}
