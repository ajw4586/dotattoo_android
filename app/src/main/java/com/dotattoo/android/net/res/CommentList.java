package com.dotattoo.android.net.res;

import java.util.List;

/**
 * Created by ajw45 on 2016-03-17.
 */
public class CommentList extends Common {

    public SimpleData data;

    public class SimpleData {
        public SimpleCommentReaction reaction;
    }
    public class SimpleCommentReaction {
        public List<Timeline.Comment> comment;
    }
}
