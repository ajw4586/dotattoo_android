package com.dotattoo.android.net.res;

import java.util.List;

/**
 * Created by ajw45 on 2016-02-01.
 */
public class Profile extends Common {

    public String _id;
    public String name;
    public String type;
    public String profile_img;
    public String phone;
    public List<Posts> posts;
    public Member.Tattooist tattooist;

    public class Posts {
        public String photo_url;
        public String _id;
    }

}
