package com.dotattoo.android;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.dotattoo.android.core.base.BaseActivity;
import com.dotattoo.android.dialog.PlacePickerDialog;
import com.dotattoo.android.net.req.TattooService;
import com.dotattoo.android.net.res.Common;
import com.dotattoo.android.pref.SessionPref;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TattooistRequest extends BaseActivity implements View.OnFocusChangeListener,
        PlacePickerDialog.OnPlaceSelectedListener, View.OnClickListener {


    private static final int LAYOUT_RESOURCE = R.layout.activity_tattooist_request;

    private EditText[] fields = new EditText[9];
    private static final int[] fields_id = {
            R.id.tattooist_request_field_1,
            R.id.tattooist_request_field_2,
            R.id.tattooist_request_field_3,
            R.id.tattooist_request_field_4,
            R.id.tattooist_request_field_5,
            R.id.tattooist_request_field_6_name,
            R.id.tattooist_request_field_6_bank,
            R.id.tattooist_request_field_6_account,
            R.id.tattooist_request_field_7
    };
    private ImageView submit;

    private PlacePickerDialog placePickerDialog;

    private TattooService.Tattoo tattooSErvice;

    private int place_1_selectPosition, place_2_selectPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setLayoutResourceId(LAYOUT_RESOURCE);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void init() {
        tattooSErvice = TattooService.getInstance();
        placePickerDialog = PlacePickerDialog.newInstance();
        placePickerDialog.setOnPlaceSelectedListener(this);
    }

    @Override
    public void initView() {
        for(int i = 0; i < 9; i++) {
            fields[i] = (EditText) findViewById(fields_id[i]);
        }
        fields[3].setOnFocusChangeListener(this);
        fields[6].setOnFocusChangeListener(this);
        submit = (ImageView) findViewById(R.id.tattooist_request_submit);
        submit.setOnClickListener(this);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if(hasFocus) {
            switch (v.getId()) {
                case R.id.tattooist_request_field_4:
                    placePickerDialog.show(getSupportFragmentManager());
                    break;
                case R.id.tattooist_request_field_6_bank:
                    break;
            }
        }
    }

    @Override
    public void onPlaceSelected(String name, int[] indexs) {
        switch (indexs.length) {
            //  level 1 selected
            case 1:
                place_1_selectPosition = indexs[0];
                fields[3].setText(fields[3].getText().toString() + name);
                break;
            //  level 2 selected
            case 2:
                place_1_selectPosition = indexs[0];
                place_2_selectPosition = indexs[1];
                fields[3].setText(fields[3].getText().toString() + name);
                break;
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tattooist_request_submit:
                tattooSErvice.requestTattooist(SessionPref.getInstance(this).getAccessToken(),
                        fields[0].getText().toString(),
                        fields[3].getText().toString(),
                        fields[4].getText().toString(),
                        fields[2].getText().toString(),
                        fields[1].getText().toString(),
                        fields[8].getText().toString(),
                        fields[5].getText().toString(),
                        fields[6].getText().toString(),
                        fields[7].getText().toString()).enqueue(new Callback<Common>() {
                    @Override
                    public void onResponse(Call<Common> call, Response<Common> response) {
                        if(response.isSuccessful() && response.body().status.equals("OK")) {
                            finish();
                        } else {
                            onFailure(call, new Throwable());
                        }
                    }

                    @Override
                    public void onFailure(Call<Common> call, Throwable t) {
                        Toast.makeText(TattooistRequest.this, "문제가 발생했습니다. 잠시 후에 다시 시도해 주세요.", Toast.LENGTH_SHORT).show();
                    }
                });
                break;
        }
    }
}
