package com.dotattoo.android;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dotattoo.android.core.base.BaseActivity;
import com.dotattoo.android.dialog.PlacePickerDialog;
import com.dotattoo.android.util.FinishTrigger;
import com.dotattoo.android.util.ImageUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.text.DecimalFormat;
import java.util.Calendar;

public class Request extends BaseActivity implements View.OnClickListener {


    public static final String USER_ID = "user_id";
    private static final int LAYOUT_RESOURCE = R.layout.activity_request;
    private static final int REQUEST_CODE_PHOTO = 1;

    private EditText[] fieldsSelector = new EditText[3];
    private int[] fieldsSelectorIds = {
            R.id.request_select_1,
            R.id.request_select_2,
            R.id.request_select_3
    };
    private EditText[] optionFields = new EditText[3];
    private int[] optionFieldIds = {
            R.id.request_field_1,
            R.id.request_field_2,
            R.id.request_field_3
    };

    private EditText[] fields = new EditText[4];
    private int[] fieldsIds = {
            R.id.request_field_4_1,
            R.id.request_field_4_2,
            R.id.request_field_4_3,
            R.id.request_field_5
    };
    private EditText tattooName;
    private Button cancel, submit;
    private SimpleDraweeView photo;
    private PlacePickerDialog placePickerDialog;
    private TextView won;

    private Uri imgUri;
    private File imgFile;
    private String user_id;
    private int genreSelectPosition = 0, partSelectPosition = 0;
    private int place_1_selectPosition = 0, place_2_selectPosition = 0;
    private String priceResult = "";

    //  for currency format
    private DecimalFormat currencyFormatter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setLayoutResourceId(LAYOUT_RESOURCE);
        activeFresco(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void init() {
        user_id = getIntent().getStringExtra(USER_ID);
        currencyFormatter = new DecimalFormat("###,###");
        FinishTrigger.getInstance().registerFinishTrigger(new FinishTrigger.OnConfirmListener() {
            @Override
            public void onConfirm() {
                finish();
            }
        });
    }

    /**
     * Wrap the result data to intent instance.
     * @return
     */
    Intent wrapData(Intent i) {
        i.putExtra(RequestConfirm.DATA[0], user_id);
        i.putExtra(RequestConfirm.DATA[1], imgFile);
        i.putExtra(RequestConfirm.DATA[2], tattooName.getText().toString());
        if(optionFields[0].getText().toString().isEmpty()) {
            i.putExtra(RequestConfirm.DATA[3], fieldsSelector[0].getText().toString());
        } else {
            i.putExtra(RequestConfirm.DATA[3], fieldsSelector[0].getText().toString()
                    + " [" + optionFields[0].getText().toString() + "]");
        }
        i.putExtra(RequestConfirm.DATA[4], fieldsSelector[1].getText().toString()
                + optionFields[1].getText().toString());
        if(optionFields[2].getText().toString().isEmpty()) {
            i.putExtra(RequestConfirm.DATA[5], fieldsSelector[2].getText().toString());
        } else {
            i.putExtra(RequestConfirm.DATA[5], fieldsSelector[2].getText().toString()
                    + " [" + optionFields[2].getText().toString() + "]");
        }
        i.putExtra(RequestConfirm.DATA[6], fields[0].getText().toString()
                        + " "+ fields[1].getText().toString());
        i.putExtra(RequestConfirm.DATA[7], fields[2].getText().toString() + "시간");
        i.putExtra(RequestConfirm.DATA[8], fields[3].getText().toString() + "원");
        return i;
    }

    @Override
    public void initView() {
        int[] arrayIds = { R.array.genre, 0, R.array.tattoo_part };
        for(int i = 0; i < 3; i++) {
            fieldsSelector[i] = (EditText) findViewById(fieldsSelectorIds[i]);
            fields[i] = (EditText) findViewById(fieldsIds[i]);
            optionFields[i] = (EditText) findViewById(optionFieldIds[i]);

            fieldsSelector[i].setOnClickListener(this);
            //  set to show date/time picker dialog
            fields[i].setOnClickListener(this);
        }
        won = (TextView) findViewById(R.id.won);

        fields[3] = (EditText) findViewById(fieldsIds[3]);
        fields[3].addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() == 0) {
                    priceResult = "0";
                    fields[3].setText(priceResult);
                    fields[3].setSelection(1);
                } else if(!s.toString().equals(priceResult)) {
                    fields[3].removeTextChangedListener(this);
                    priceResult = currencyFormatter.format(Long.parseLong(s.toString()
                            .replaceAll(",", "")));
                    fields[3].setText(priceResult);
                    fields[3].setSelection(priceResult.length());
                    fields[3].addTextChangedListener(this);
                    if(fields[3].getGravity() != Gravity.RIGHT) {
                        fields[3].setGravity(Gravity.RIGHT);
                    }
                    if(won.getVisibility() == View.GONE) {
                        won.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        cancel = (Button) findViewById(R.id.request_cancel);
        submit = (Button) findViewById(R.id.request_send);
        cancel.setOnClickListener(this);
        submit.setOnClickListener(this);

        photo = (SimpleDraweeView) findViewById(R.id.request_tattoo_photo);
        photo.setOnClickListener(this);
        tattooName = (EditText) findViewById(R.id.request_tattoo_name);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.request_cancel:
                finish();
                break;
            case R.id.request_send:
                if(!isFieldsEmpty()) {
                    Intent i = new Intent(this, RequestConfirm.class);
                    i = wrapData(i);
                    startActivity(i);
                } else {
                    Toast.makeText(Request.this, getString(R.string.check_field_message),
                            Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.request_tattoo_photo:
                callGallery();
                break;
            case R.id.request_select_1:
                showSelectGenreDialog();
                break;
            case R.id.request_select_2:
                showSelectPlaceDialog();
                break;
            case R.id.request_select_3:
                showSelectPartDialog();
                break;
            case R.id.request_field_4_1:
                showDatePickerDialog();
                break;
            case R.id.request_field_4_2:
                showTimePickerDialog();
                break;
        }
    }

    void callGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, REQUEST_CODE_PHOTO);
    }

    //  Check fields before sending request
    boolean isFieldsEmpty() {
        if(imgFile == null) {
            return true;
        }
        if(tattooName.getText().toString().isEmpty()) {
            return true;
        }
        for(EditText e : fields) {
            if(e.getText().toString().isEmpty()) {
                return true;
            }
        }
        for(EditText e : fieldsSelector) {
            if(e.getText().toString().isEmpty()) {
                return true;
            }
        }
        return false;
    }

    void showSelectGenreDialog() {
        final String[] genres = getResources().getStringArray(R.array.genre);
        new AlertDialog.Builder(this)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        fieldsSelector[0].setText(genres[which]);
//                        fieldsSelector[0].setTextSize(18);
                        genreSelectPosition = which;
                        if(which == genres.length - 1) {
                            optionFields[0].setVisibility(View.VISIBLE);
                        } else if(optionFields[0].getVisibility() == View.VISIBLE) {
                            optionFields[0].setVisibility(View.GONE);
                        }
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setSingleChoiceItems(genres, genreSelectPosition, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        fieldsSelector[0].setText(genres[which]);
//                        fieldsSelector[0].setTextSize(18);
                        genreSelectPosition = which;
                        if(which == genres.length - 1) {
                            optionFields[0].setVisibility(View.VISIBLE);
                        } else if(optionFields[0].getVisibility() == View.VISIBLE) {
                            optionFields[0].setVisibility(View.GONE);
                        }
                        dialog.dismiss();
                    }
                }).create().show();
    }

    void showSelectPlaceDialog() {
        Log.d("placepickerr", "selected position is " + place_1_selectPosition + " " + place_2_selectPosition);
        placePickerDialog = PlacePickerDialog.newInstance();
        placePickerDialog.setSelection(new int[] {place_1_selectPosition, place_2_selectPosition});
        placePickerDialog.setOnPlaceSelectedListener(new PlacePickerDialog.OnPlaceSelectedListener() {
            @Override
            public void onPlaceSelected(String name, int[] indexs) {
                Log.d("listener", "length " + indexs.length);
                switch (indexs.length) {
                    //  level 1 selected
                    case 1:
                        place_1_selectPosition = indexs[0];
                        fieldsSelector[1].setText(name);
                        if(optionFields[1].getVisibility() != View.VISIBLE) {
                            optionFields[1].setVisibility(View.VISIBLE);
                        }
                        break;
                    //  level 2 selected
                    case 2:
                        place_1_selectPosition = indexs[0];
                        place_2_selectPosition = indexs[1];
                        fieldsSelector[1].setText(name);
                        if(optionFields[1].getVisibility() != View.VISIBLE) {
                            optionFields[1].setVisibility(View.VISIBLE);
                        }
                        break;
                }
            }
        });
        placePickerDialog.show(getSupportFragmentManager());
    }

    void showSelectPartDialog() {
        final String[] parts = getResources().getStringArray(R.array.tattoo_part);
        new AlertDialog.Builder(this)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        fieldsSelector[2].setText(parts[which]);
//                        fieldsSelector[2].setTextSize(18);
                        partSelectPosition = which;
                        if(which == parts.length - 1) {
                            optionFields[2].setVisibility(View.VISIBLE);
                        } else if(optionFields[2].getVisibility() == View.VISIBLE) {
                            optionFields[2].setVisibility(View.GONE);
                        }
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setSingleChoiceItems(parts, partSelectPosition, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        fieldsSelector[2].setText(parts[which]);
//                        fieldsSelector[2].setTextSize(18);
                        partSelectPosition = which;
                        if(which == parts.length - 1) {
                            optionFields[2].setVisibility(View.VISIBLE);
                        } else if(optionFields[2].getVisibility() == View.VISIBLE) {
                            optionFields[2].setVisibility(View.GONE);
                        }
                        dialog.dismiss();
                    }
                }).create().show();
    }

    void showDatePickerDialog() {
        Calendar cal = Calendar.getInstance();
        DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                fields[0].setText(year + "년 " + (monthOfYear+1) + "월 " + dayOfMonth + "일");
                fields[1].callOnClick();
            }
        }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_YEAR))
                .show(getFragmentManager(), "datePickerDialog");
    }
    void showTimePickerDialog() {
        Calendar cal = Calendar.getInstance();
        TimePickerDialog.newInstance(new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
                fields[1].setText(hourOfDay + "시 " + minute + "분");
            }
        }, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), false)
                .show(getFragmentManager(), "timePickerDialog");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_CODE_PHOTO && resultCode == RESULT_OK && data != null) {
            imgUri = data.getData();
            imgFile = new File(Environment.getExternalStorageDirectory(), ".dotattoo_photo_tmp.png");
            UCrop.of(imgUri, Uri.fromFile(imgFile)).start(this);
        }
        if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            imgFile = ImageUtils.saveImage(this, UCrop.getOutput(data));
            photo.setImageURI(Uri.fromFile(imgFile));
        } else if (resultCode == UCrop.RESULT_ERROR) {
            Toast.makeText(this, "사진을 불러오지 못했습니다.", Toast.LENGTH_SHORT).show();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}
