package com.dotattoo.android;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.dotattoo.android.core.base.BaseActivity;

public class Splash extends BaseActivity {


    private static final int LAYOUT_RESOURCE = R.layout.activity_splash;

    private Runnable delayed;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setLayoutResourceId(LAYOUT_RESOURCE);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void init() {
        handler = new Handler();
        delayed = new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(Splash.this, Login.class));
                finish();
            }
        };
        handler.postDelayed(delayed, 2000);
    }

    @Override
    public void initView() {

    }

    @Override
    public void initMenu() {

    }

    @Override
    protected void onStop() {
        super.onStop();
        handler.removeCallbacks(delayed);
    }
}
