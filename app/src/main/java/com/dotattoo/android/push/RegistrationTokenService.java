package com.dotattoo.android.push;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.dotattoo.android.R;
import com.dotattoo.android.net.req.PushService;
import com.dotattoo.android.net.res.Common;
import com.dotattoo.android.pref.SessionPref;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ajw45 on 2016-03-28.
 */
public class RegistrationTokenService extends IntentService {


    public RegistrationTokenService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        RegisterThread registerThread = new RegisterThread(this) {
            @Override
            public void onTokenReceive(String token) {
                Toast.makeText(RegistrationTokenService.this, "GCM Registration token: " + token,
                        Toast.LENGTH_SHORT).show();
            }
        };
        registerThread.start();
    }

    public static class RegisterThread extends Thread {


        private Context mContext;
        private PushService.Push pushService;

        public RegisterThread(Context c) {
            mContext = c;
            pushService = PushService.getInstance();
        }

        @Override
        public void run() {
            super.run();
            try {
                String token = InstanceID.getInstance(mContext).getToken(
                        mContext.getString(R.string.gcm_defaultSenderId),
                        GoogleCloudMessaging.INSTANCE_ID_SCOPE);
                registerToServer(token);
                onTokenReceive(token);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        void registerToServer(String token) {
            pushService.registerPushToken(token,
                    SessionPref.getInstance(mContext).getAccessToken())
                    .enqueue(new Callback<Common>() {
                        @Override
                        public void onResponse(Call<Common> call, Response<Common> response) {
                            if (response.isSuccessful() && response.body().status.equals("OK")) {
                                //  Do nothing
                                Log.d("Register GCM Thread", "Registration ID를 서버에 저장했습니다.");
                            } else {
                                onFailure(call, new Throwable());
                            }
                        }

                        @Override
                        public void onFailure(Call<Common> call, Throwable t) {
                            Log.d("Register GCM Thread", "푸시 서버에 연결할 수 없습니다.");
                        }
                    });
        }

        public void  onTokenReceive(String token) {

        }
    }

}
