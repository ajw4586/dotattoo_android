package com.dotattoo.android.push;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.dotattoo.android.util.notification.PushNotificationFactory;
import com.google.android.gms.gcm.GcmListenerService;

/**
 * Created by ajw45 on 2016-03-28.
 */
public class PushNotificationService extends GcmListenerService {


    private static final int COMMENT_NOTIFICATION = 100;
    private static final int AD_NOTIFICATION = 101;

    NotificationManager notiManager;

    public PushNotificationService() {
        super();
    }

    @Override
    public void onMessageReceived(String from, Bundle data) {
        notiManager = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);
        String message_type = data.getString("type");
        switch (message_type) {
            //  댓글 알림
            case "comment":
                receiveCommontPush(data);
                break;
            //  채팅 메세지 알림
            case "chat":
                receiveChatMessage(data);
                break;
            //  광고 알림
            case "ad":
                receiveAd(data);
                break;
            default:
                Log.w(getClass().getSimpleName(), "푸시알림을 처리하지 못했습니다.");
                break;
        }
    }

    void receiveCommontPush(Bundle data) {
        String message = data.getString("message");
        android.app.Notification commentNoti = PushNotificationFactory
                .createCommentNotification(this, data.getString("user_name"));
        notiManager.notify(COMMENT_NOTIFICATION, commentNoti);
    }

    void receiveChatMessage(Bundle Data) {

    }

    void receiveAd(Bundle data) {
        String pushMessage = data.getString("message");
        String contentMessage = data.getString("message_content");
        android.app.Notification adNoti = PushNotificationFactory
                .createAdNotification(this, pushMessage, contentMessage);
        PushNotificationFactory.PushWakeLock.acquireCpuWakeLock(this);
        PushNotificationFactory.PushWakeLock.releaseCpuLock();
        notiManager.notify(AD_NOTIFICATION, adNoti);
    }
}
