package com.dotattoo.android.push;

import android.content.Intent;

/**
 * Created by ajw45 on 2016-04-04.
 */
public class InstanceIDService extends com.google.android.gms.iid.InstanceIDListenerService {


    @Override
    public void onTokenRefresh() {
        startService(new Intent(this, RegistrationTokenService.class));
    }
}
