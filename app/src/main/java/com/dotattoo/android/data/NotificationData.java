package com.dotattoo.android.data;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.dotattoo.android.adapter.AlarmAdapter;
import com.dotattoo.android.net.req.NotificationService;
import com.dotattoo.android.net.res.Notifications;
import com.dotattoo.android.pref.SessionPref;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ajw45 on 2016-06-09.
 */
public class NotificationData {

    private static NotificationData instance;

    private Context mContext;
    private NotificationService.Notification notificationService;
    private AlarmAdapter alarmAdapter;
    private DataUpdateCallback callback;


    public static NotificationData getInstance(Context context) {
        return instance != null ? instance : new NotificationData(context);
    }

    private NotificationData(Context context) {
        this.mContext = context;
        notificationService = NotificationService.getInstance();
        alarmAdapter = new AlarmAdapter();
    }

    public AlarmAdapter getAdapter() {
        return alarmAdapter;
    }

    public void setCallback(DataUpdateCallback callback) {
        this.callback = callback;
    }

    public void updateData() {
        notificationService.getNotifications(SessionPref.getInstance(mContext).getAccessToken())
                .enqueue(new Callback<Notifications>() {
                    @Override
                    public void onResponse(Call<Notifications> call, Response<Notifications> response) {
                        if(response.isSuccessful() && response.body().status.equals("OK")) {
                            alarmAdapter.updateData(response.body().data);
                            if(callback != null) {
                                callback.onUpdated();
                            }
                        } else {
                            onFailure(call, new Throwable());
                        }
                    }

                    @Override
                    public void onFailure(Call<Notifications> call, Throwable t) {
                        Toast.makeText(mContext, "알림을 업데이트할 수 없습니다. 다시 시도해 주세요.",
                                Toast.LENGTH_SHORT).show();
                        Log.d("notification", "onFailure: reason " + t.getMessage());
                        if(callback != null) {
                            callback.onFailed();
                        }
                    }
                });
    }


}
