package com.dotattoo.android.data;

import android.content.Context;
import android.widget.Toast;

import com.dotattoo.android.net.req.MemberService;
import com.dotattoo.android.net.res.Profile;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ajw45 on 2016-02-13.
 */
public class UserInfoData {

    private static UserInfoData instance;
    private static MemberService.Member memberService;
    private Context context;

    private Profile profile = null;

    private DataUpdateCallback updateCallback;


    public static UserInfoData getInstance(Context context) {
        return instance != null ? instance : new UserInfoData(context);
    }

    /**
     * clear timeline data instance
     * It is used to shut down application
     */
    public static void clear() {
        instance = null;
    }

    private UserInfoData(Context context) {
        memberService = MemberService.getInstance();
        this.context = context;
        instance = this;
    }

    public void setCallback(DataUpdateCallback callback) {
        this.updateCallback = callback;
    }

    public void removeCallback() {
        this.updateCallback = null;
    }

    public void requestUserInfo(String user_id) {
        if(user_id == null) {
            Toast.makeText(context, "유저 정보를 불러올 수 없습니다.", Toast.LENGTH_SHORT).show();
            return;
        }
        Call<Profile> memberCall = memberService.getUserInfo(user_id);
        memberCall.enqueue(new Callback<Profile>() {
            @Override
            public void onResponse(Call<Profile> call, Response<Profile> response) {
                if(response.isSuccessful() == response.body().status.equals("OK")) {
                    profile = response.body();
                    if(updateCallback != null)
                        updateCallback.onUpdated();
                } else {
                    //  update failed
                }
            }

            @Override
            public void onFailure(Call<Profile> call, Throwable t) {

            }
        });
    }

    public Profile getProfile() {
        return profile;
    }
}
