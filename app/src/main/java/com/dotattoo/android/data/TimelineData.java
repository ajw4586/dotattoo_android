package com.dotattoo.android.data;

import android.content.Context;
import android.util.Log;

import com.dotattoo.android.adapter.TimelineAdapter;
import com.dotattoo.android.core.base.BaseActivity;
import com.dotattoo.android.core.base.BaseFragment;
import com.dotattoo.android.net.req.PostService;
import com.dotattoo.android.net.res.Timeline;
import com.dotattoo.android.pref.SessionPref;
import com.dotattoo.android.tab.home.TimelineFragment;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ajw45 on 2016-02-13.
 */
public class TimelineData {

    private PostService.Post postService;
    private TimelineAdapter adapter;
    private Context context;
    private static TimelineData instance;

    private static DataUpdateCallback callback;

    //  Add activity in this method parameter
    //  to prevent context leak and glide exception.
    /**
     * Only use when instance was created.
     * @return
     */
    public static TimelineData getInstance(BaseActivity activity) {
        return instance != null ? instance.updateInstance(activity)
                : new TimelineData(activity);
    }

    public static TimelineData getInstance(BaseFragment fragment) {
        return instance != null ? instance.updateInstance(fragment)
                : new TimelineData(fragment);
    }

    /**
     * clear timeline data instance
     * It is used to shut down application
     */
    public static void clear() {
        instance = null;
    }

    public TimelineAdapter getTimelineAdapter() {
        return adapter;
    }

    public void setCallback(DataUpdateCallback callback) {
        this.callback = callback;
    }

    public void removeCallback() {
        callback = null;
    }

   private TimelineData(BaseFragment fragment) {
       postService = PostService.getInstance();
       this.context = fragment.getContext();
       adapter = new TimelineAdapter(fragment);
       if(fragment instanceof TimelineFragment) {
           ((TimelineFragment) fragment).addOnMenuFABStateChangeListener(adapter);
       }
       requestTimeline();
       instance = this;
   }

    private TimelineData(BaseActivity activity) {
        postService = PostService.getInstance();
        this.context = activity;
        adapter = new TimelineAdapter(activity);
        requestTimeline();
        instance = this;
    }

    TimelineData updateInstance(BaseActivity activity) {
        this.context = activity;
        this.adapter.updateInstance(activity);
        return this;
    }

    TimelineData updateInstance(BaseFragment fragment) {
        this.context = fragment.getContext();
        this.adapter.updateInstance(fragment);
        return this;
    }

    public void requestTimeline() {
        Call<Timeline> timelineCall = postService.getTimeline(SessionPref.getInstance(context).getAccessToken());
        timelineCall.enqueue(new Callback<Timeline>() {
            @Override
            public void onResponse(Call<Timeline> call, Response<Timeline> response) {
                if(response.isSuccessful() && response.body().status.equals("OK")) {
                    adapter.updateData(response.body().data);
                    if(callback != null)
                        callback.onUpdated();
                } else {
                    onFailure(call, new Throwable());
                }
            }

            @Override
            public void onFailure(Call<Timeline> call, Throwable t) {
                Log.e("timeline", "failed " + t.toString());
                //  change swipeupdatelayout state to false.
                if(callback != null)
                    callback.onFailed();
            }
        });
    }
}
