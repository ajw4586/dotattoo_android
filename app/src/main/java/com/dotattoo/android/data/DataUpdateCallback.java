package com.dotattoo.android.data;

/**
 * Created by ajw45 on 2016-06-09.
 */
public interface DataUpdateCallback {

    public void onUpdated();
    public void onFailed();
}
