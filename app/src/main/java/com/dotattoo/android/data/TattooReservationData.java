package com.dotattoo.android.data;

import android.content.Context;
import android.util.Log;

import com.dotattoo.android.adapter.TattooReservationAdapter;
import com.dotattoo.android.net.req.TattooService;
import com.dotattoo.android.net.res.Reservations;
import com.dotattoo.android.pref.SessionPref;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ajw45 on 2016-06-18.
 */
public class TattooReservationData {


    private static TattooReservationData instance;

    private Context mContext;
    private TattooReservationAdapter adapter;
    private TattooService.Tattoo tattooService;
    private DataUpdateCallback callback;

    public static TattooReservationData getInstance(Context context) {
        return instance != null ? instance : new TattooReservationData(context);
    }

    private TattooReservationData(Context context) {
        mContext = context;
        adapter = new TattooReservationAdapter();
        tattooService = TattooService.getInstance();
        instance = this;
    }

    public void setCallback(DataUpdateCallback callback) {
        this.callback = callback;
    }

    public void updateData() {
        tattooService.getReservationList(SessionPref.getInstance(mContext).getAccessToken())
                .enqueue(new Callback<Reservations>() {
                    @Override
                    public void onResponse(Call<Reservations> call, Response<Reservations> response) {
                        if(response.isSuccessful() && response.body().status.equals("OK")) {
                            adapter.updateData(response.body().data);
                            if(callback != null) {
                                callback.onUpdated();
                            }
                        } else {
                            onFailure(call, new Throwable());
                        }
                    }

                    @Override
                    public void onFailure(Call<Reservations> call, Throwable t) {
                        Log.e("getReservationList", "onFailure: ", t);
                        if(callback != null) {
                            callback.onFailed();
                        }
                    }
                });
    }

    public TattooReservationAdapter getAdapter() {
        return adapter;
    }
}
