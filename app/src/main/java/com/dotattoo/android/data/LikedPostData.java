package com.dotattoo.android.data;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.dotattoo.android.adapter.LikedPostAdapter;
import com.dotattoo.android.net.req.PostService;
import com.dotattoo.android.net.res.Timeline;
import com.dotattoo.android.pref.SessionPref;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ajw45 on 2016-06-07.
 */
public class LikedPostData {

    public static LikedPostData instance;

    private Context mContext;
    private LikedPostAdapter adapter;
    private PostService.Post postService;
    private DataUpdateCallback callback;

    public static LikedPostData getInstance(Context context) {
        return instance != null ? instance.updateInstance(context) : new LikedPostData(context);
    }

    private LikedPostData(Context context) {
        mContext = context;
        adapter = new LikedPostAdapter();
        postService = PostService.getInstance();
    }

    private LikedPostData updateInstance(Context context) {
        mContext = context;
        return this;
    }

    public int getDataSize() {
        return getAdapter().getItemCount();
    }

    public void setCallback(DataUpdateCallback callback) {
        this.callback = callback;
    }

    public LikedPostAdapter getAdapter() {
        return adapter;
    }

    public void updateData() {
        postService.getLikedPost(SessionPref.getInstance(mContext).getAccessToken())
                .enqueue(new Callback<Timeline>() {
                    @Override
                    public void onResponse(Call<Timeline> call, Response<Timeline> response) {
                        if(response.isSuccessful() && response.body().status.equals("OK")) {
                            adapter.updateData(response.body().data);
                            Log.d("likedPost", "onResponse: response data length: " + response.body().data.size());
                            if(callback != null) {
                                callback.onUpdated();
                            }
                        } else {
                            onFailure(call, new Throwable());
                        }
                    }

                    @Override
                    public void onFailure(Call<Timeline> call, Throwable t) {
                        Toast.makeText(mContext, "관심타투를 불러올 수 없습니다.", Toast.LENGTH_SHORT).show();
                        if(callback != null) {
                            callback.onFailed();
                        }
                    }
                });
    }
}
