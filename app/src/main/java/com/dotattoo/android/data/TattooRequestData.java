package com.dotattoo.android.data;

import android.content.Context;
import android.util.Log;

import com.dotattoo.android.adapter.TattooRequestAdapter;
import com.dotattoo.android.net.req.TattooService;
import com.dotattoo.android.net.res.Requests;
import com.dotattoo.android.pref.SessionPref;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ajw45 on 2016-06-18.
 */
public class TattooRequestData {

    private static TattooRequestData instance;

    private Context mContext;
    private TattooRequestAdapter adapter;
    private TattooService.Tattoo tattooService;
    private DataUpdateCallback callback;

    public static TattooRequestData getInstance(Context context) {
        return instance != null ? instance : new TattooRequestData(context);
    }

    private TattooRequestData(Context context) {
        mContext = context;
        adapter = new TattooRequestAdapter();
        tattooService = TattooService.getInstance();
        instance = this;
    }

    public void setCallback(DataUpdateCallback callback) {
        this.callback = callback;
    }


    public void updateData() {
        tattooService.getRequestList(SessionPref.getInstance(mContext).getAccessToken())
                .enqueue(new Callback<Requests>() {
                    @Override
                    public void onResponse(Call<Requests> call, Response<Requests> response) {
                        if(response.isSuccessful() && response.body().status.equals("OK")) {
                            adapter.updateData(response.body().data);
                            if(callback != null) {
                                callback.onUpdated();
                            }
                        } else {
                            onFailure(call, new Throwable());
                        }
                    }

                    @Override
                    public void onFailure(Call<Requests> call, Throwable t) {
                        Log.e("getRequestList", "onFailure: ", t);
                        if(callback != null) {
                            callback.onFailed();
                        }
                    }
                });
    }

    public TattooRequestAdapter getAdapter() {
        return adapter;
    }

}
