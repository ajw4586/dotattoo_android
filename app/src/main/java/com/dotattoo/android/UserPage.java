package com.dotattoo.android;

import android.*;
import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bartoszlipinski.recyclerviewheader2.RecyclerViewHeader;
import com.dotattoo.android.adapter.UserPostsAdapter;
import com.dotattoo.android.core.base.BaseActivity;
import com.dotattoo.android.data.DataUpdateCallback;
import com.dotattoo.android.data.UserInfoData;
import com.dotattoo.android.net.res.Profile;
import com.dotattoo.android.util.GridMarginDecoration;
import com.dotattoo.android.util.ImageUtils;
import com.dotattoo.android.util.Utils;
import com.facebook.drawee.view.SimpleDraweeView;


public class UserPage extends BaseActivity implements View.OnClickListener {


    private static final int CALL_PERMISSION_REQ_CODE = 345;

    public static final int LAYOUT_RESOURCE = R.layout.activity_user_page;
    public static final String USER_ID = "user_id";
    public static final String PROFILE_IMAGE = "profile_image";
    public static final String TATTOOIST = "tattooist";
    public static final String USER = "user";

    private SimpleDraweeView profileImage, call, message, bg;
    private TextView userName, comment, place;
    private Button contact;

    private RecyclerView postsList;
    private RecyclerViewHeader listHeader;

    private UserPostsAdapter photoAdapter;
    private GridLayoutManager glManager;

    private LinearLayout contact_container;

    private String userType = "";
    private String profile_img;
    private String phone_num;

    private UserInfoData userInfoData;

    String user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setLayoutResourceId(LAYOUT_RESOURCE);
        activeFresco(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getUserReqInfo();
    }

    @Override
    public void init() {
        userInfoData = UserInfoData.getInstance(this);
        photoAdapter = new UserPostsAdapter(this);

        userInfoData.setCallback(new DataUpdateCallback() {
            @Override
            public void onUpdated() {
                //  update user info at this time.
                Profile info = userInfoData.getProfile();
                userName.setText(info.name);
                userType = info.type;
                //  If tattooist, show contact button.
                if (userType.equals(TATTOOIST)) {
                    if(info.tattooist.status.show_contact) {
                        contact_container.setVisibility(View.VISIBLE);
                    } else {
                        contact_container.setVisibility(View.GONE);
                    }
                    Log.d("Show state", "onUpdated: show_consult state:"  + info.tattooist.status.show_consult);
                    if(info.tattooist.status.show_consult) {
                        contact.setVisibility(View.VISIBLE);
                    } else {
                        contact.setVisibility(View.GONE);
                    }
                    try {
                        phone_num = info.phone;
                        call.setVisibility(View.VISIBLE);
                        message.setVisibility(View.VISIBLE);
                    } catch (NullPointerException e) {}
                    try {
                        comment.setText(info.tattooist.comment);
                        place.setText(info.tattooist.place);
                    } catch (NullPointerException e) {}
                    place.setVisibility(View.VISIBLE);
                    comment.setVisibility(View.VISIBLE);
                }
                //  If user has any post.
                if(info.posts != null) {
                    photoAdapter.updateData(info.posts);
                }
                profileImage.setImageURI(Utils.createImageURI(info.profile_img));
//            previous code for blur sample
//                    .bitmapTransform(new BlurTransformation(this, 25, 3), new ColorFilterTransformation(this, 0x33000000))
                bg.setController(ImageUtils.requestBlurImage(UserPage.this, info.profile_img,
                        bg.getWidth(), bg.getHeight(), bg));
            }

            @Override
            public void onFailed() {

            }
        });
    }

    /**
     * Call to check if user info needs to update.
     * Profile instance must be not null required.
     *
     * @param info user's profile info
     * @return is need update
     */
    boolean needReload(Profile info) {
        if(info._id.equals(getIntent().getStringExtra(USER_ID)) ||
                !info._id.equals(user_id)) {
            return true;
        }
        return false;
    }

    /**
     * Get and Request call about user information to server.
     * if user info already exists, just recycle it.
     */
    void getUserReqInfo() {
        Intent i = getIntent();
        user_id = i.getStringExtra(USER_ID);
        profile_img = i.getStringExtra(PROFILE_IMAGE);
        Profile profile = userInfoData.getProfile();
        if(profile == null) {
            userInfoData.requestUserInfo(user_id);
        } else if(needReload(profile)) {
            userInfoData.requestUserInfo(user_id);
        } else {
            //  If user has any post.
            if(profile.posts != null) {
                photoAdapter.updateData(profile.posts);
            }

            userName.setText(profile.name);
            userType = profile.type;
            //  If tattooist, show contact button.
            if (userType.equals(TATTOOIST)) {
                try {
                    comment.setText(profile.tattooist.comment);
                    place.setText(profile.tattooist.place);
                    if(profile.phone != null)
                        contact.setVisibility(View.VISIBLE);
                    place.setVisibility(View.VISIBLE);
                    comment.setVisibility(View.VISIBLE);
                } catch (NullPointerException e) {}
            } else {
                //  Normal User
            }
            profileImage.setImageURI(Utils.createImageURI(profile.profile_img));
//            previous code for blur sample
//                    .bitmapTransform(new BlurTransformation(this, 25, 3), new ColorFilterTransformation(this, 0x33000000))
            bg.setController(ImageUtils.requestBlurImage(this, profile.profile_img,
                    bg.getWidth(), bg.getHeight(), bg));
        }

        //  get profile image
        if(profile_img != null) {
            profileImage.setImageURI(Utils.createImageURI(profile_img));
//            previous code for blur sample
//                    .bitmapTransform(new BlurTransformation(this, 25, 3), new ColorFilterTransformation(this, 0x33000000))
            bg.setController(ImageUtils.requestBlurImage(this, profile_img,
                    bg.getWidth(), bg.getHeight(), bg));
        }
    }

    @Override
    public void initView() {
        postsList = (RecyclerView) findViewById(R.id.user_page_posts_list);
        postsList.addItemDecoration(new GridMarginDecoration(3, true));
        listHeader = (RecyclerViewHeader) findViewById(R.id.user_page_list_header);
        glManager = new GridLayoutManager(this, 4);

        postsList.setLayoutManager(glManager);
        postsList.setAdapter(photoAdapter);
        listHeader.attachTo(postsList);

        //  initialize header view.
        bg = (SimpleDraweeView) findViewById(R.id.user_page_bg);
        profileImage = (SimpleDraweeView) findViewById(R.id.user_page_profile_img);
        userName = (TextView) findViewById(R.id.user_page_name);
        comment = (TextView) findViewById(R.id.user_page_tattooist_comment);
        place = (TextView) findViewById(R.id.user_page_tattooist_place);
        contact = (Button) findViewById(R.id.user_page_tattooist_contact);
        contact_container = (LinearLayout) findViewById(R.id.user_page_contact_container);
        contact.setOnClickListener(this);
        call = (SimpleDraweeView) findViewById(R.id.user_page_call);
        message = (SimpleDraweeView) findViewById(R.id.user_page_message);
        call.setOnClickListener(this);
        message.setOnClickListener(this);

        getUserReqInfo();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.user_page_call:
                showContactNotiAlert(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if(isCallPermissionGranted()) {
                            dialToContact();
                        } else {
                            // Should we show an explanation?
                            if (ActivityCompat.shouldShowRequestPermissionRationale(UserPage.this,
                                    Manifest.permission.CALL_PHONE)) {
                                dialToContact();

                                // Show an expanation to the user *asynchronously* -- don't block
                                // this thread waiting for the user's response! After the user
                                // sees the explanation, try again to request the permission.
                            } else {
                                ActivityCompat.requestPermissions(UserPage.this,
                                        new String[]{Manifest.permission.CALL_PHONE},
                                        CALL_PERMISSION_REQ_CODE);
                            }

                        }
                    }
                });
                break;
            case R.id.user_page_message:
                showContactNotiAlert(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if(phone_num != null) {
                            Intent message = new Intent(Intent.ACTION_SENDTO);
                            message.putExtra("sms_body", "두타투에서 작품사진 보고 연락드렸습니다. ");
                            message.setData(Uri.parse("smsto:" + phone_num));
                            startActivity(message);
                        }
                    }
                });
                break;
            case R.id.user_page_tattooist_contact:
                Intent chat = new Intent(this, Chat.class);
                chat.putExtra(Chat.USER_ID, user_id);
                chat.putExtra(Chat.USER_NAME, userName.getText().toString());
                chat.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(chat);
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case CALL_PERMISSION_REQ_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    dialToContact();
                } else {
                    new AlertDialog.Builder(this)
                            .setTitle(R.string.call_permission_warning)
                            .setMessage(R.string.permission_solution)
                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .create().show();
                }
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    void dialToContact() {
        if(phone_num != null) {
            Intent call = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone_num));
            startActivity(call);
        }
    }

    void showContactNotiAlert(DialogInterface.OnClickListener clickListener) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.contact_noti_alert_title)
                .setMessage(R.string.contact_noti_alert_message)
                .setPositiveButton(R.string.ok, clickListener)
                .create().show();
    }

    boolean isCallPermissionGranted() {
        int state = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
        return state == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void initMenu() {

    }


}
