package com.dotattoo.android;

import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.Animatable;
import android.os.Build;
import android.os.Bundle;
import android.transition.TransitionInflater;
import android.transition.TransitionSet;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dotattoo.android.core.base.BaseActivity;
import com.dotattoo.android.dialog.CommentDialog;
import com.dotattoo.android.dialog.ConsultDialog;
import com.dotattoo.android.net.req.ChatService;
import com.dotattoo.android.net.req.PostService;
import com.dotattoo.android.net.res.Common;
import com.dotattoo.android.pref.SessionPref;
import com.dotattoo.android.util.ImageUtils;
import com.dotattoo.android.util.TimeFormatter;
import com.dotattoo.android.util.Utils;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.image.ImageInfo;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostDetail extends BaseActivity implements View.OnClickListener {


    public static final String POST_ID = "post_id";

    private static final int LAYOUT_RESOURCE = R.layout.activity_post_detail;

    private SimpleDraweeView photo;
    private TextView comment, userName, time, title;
    private SimpleDraweeView userImage, consult;

    //  part
    private SimpleDraweeView partImage;
    private TextView part;
    private HashMap<String, Integer> partImgMap;
    private String[] partArray;
    private int[] partImgSource = {
            R.drawable.part_face,
            R.drawable.part_neck,
            R.drawable.part_body,
            R.drawable.part_back,
            //  어깨
            R.drawable.part_left_sholder,
            R.drawable.part_right_sholder,
            //  팔
            R.drawable.part_left_arm,
            R.drawable.part_right_arm,
            //  손
            R.drawable.part_left_hand,
            R.drawable.part_right_hand,
            //  다리
            R.drawable.part_left_leg,
            R.drawable.part_right_leg,
            //  발
            R.drawable.part_left_foot,
            R.drawable.part_right_foot,
            //  etc
            R.drawable.part_etc
    };

    //  reaction area
    private ImageView like, save, share;
    private TextView likeCount, saveCount, shareCount;

    //  comments area
    private TextView moreComments;
    private RelativeLayout[] comments = new RelativeLayout[3];
    private int[] commentsId = {
            R.id.post_detail_comment_1,
            R.id.post_detail_comment_2,
            R.id.post_detail_comment_3
    };
    private Animation likeAnim;

    private PostService.Post postService;

    String post_id, user_id, photo_url;
    boolean isLiked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setLayoutResourceId(LAYOUT_RESOURCE);
        activeFresco(true);
        super.onCreate(savedInstanceState);
        initAnimation();
    }

    void initAnimation() {
        if(Build.VERSION.SDK_INT >= 21) {
            TransitionSet photoAnim = (TransitionSet) TransitionInflater
                    .from(this).inflateTransition(R.transition.move_image);
            TransitionSet photoBackAnim = (TransitionSet) TransitionInflater
                    .from(this).inflateTransition(R.transition.move_image_back);
            //  set begin transition
            getWindow().setSharedElementEnterTransition(photoAnim);
            getWindow().setEnterTransition(TransitionInflater.from(this)
                    .inflateTransition(R.transition.slide_forward_transition));
            //  set back transition
            getWindow().setSharedElementReturnTransition(photoBackAnim);
            getWindow().setReturnTransition(TransitionInflater.from(this)
                    .inflateTransition(R.transition.slide_back_transition));
        }
    }

    @Override
    public void init() {
        postService = PostService.getInstance();
        partImgMap = new HashMap<>();
        partArray = getResources().getStringArray(R.array.tattoo_part);
        for(int i = 0; i < 16; i++) {
            if(i == 0)
            continue;
            partImgMap.put(partArray[i], partImgSource[i - 1]);
        }
        likeAnim = AnimationUtils.loadAnimation(this, R.anim.like_pop);
    }

    void getPostInfo() {
        Intent i = getIntent();
        post_id = i.getStringExtra(POST_ID);
        postService.getPost(post_id, SessionPref.getInstance(this).getAccessToken()).enqueue(new Callback<com.dotattoo.android.net.res.PostDetail>() {
            @Override
            public void onResponse(Call<com.dotattoo.android.net.res.PostDetail> call,
                                   Response<com.dotattoo.android.net.res.PostDetail> response) {
                if(response.isSuccessful() && response.body().status.equals("OK") ) {
                    bindPostInfo(response);
                    bindComments(response);
                } else {
                    onFailure(call, new Throwable());
                }
            }

            @Override
            public void onFailure(Call<com.dotattoo.android.net.res.PostDetail> call, Throwable t) {
                Toast.makeText(PostDetail.this, "게시글을 불러올 수 없습니다.", Toast.LENGTH_LONG).show();
            }
        });
    }

    void bindComments(Response<com.dotattoo.android.net.res.PostDetail> response) {
        if(!response.body().data.get(0).reaction.comment.isEmpty()) {
            //  visible comments
            switch (response.body().data.get(0).reaction.comment.size()) {
                case 0:
                    break;
                case 1:
                    bindCommentAt(response, 0);
                    break;
                case 2:
                    bindCommentAt(response, 0);
                    bindCommentAt(response, 1);
                    break;
                case 3:
                    bindCommentAt(response, 0);
                    bindCommentAt(response, 1);
                    bindCommentAt(response, 2);
                    break;
                default:
                    bindCommentAt(response, 0);
                    bindCommentAt(response, 1);
                    bindCommentAt(response, 2);
                    moreComments.setVisibility(View.VISIBLE);
                    break;
            }
        }
    }

    void bindCommentAt(Response<com.dotattoo.android.net.res.PostDetail> response, int position) {
        comments[position].setVisibility(View.VISIBLE);
        ((TextView) comments[position].findViewById(R.id.comment_user_name))
                .setText(response.body().data.get(0).reaction.comment.get(position).commentBy.name);
        ((TextView) comments[position].findViewById(R.id.comment_message))
                .setText(response.body().data.get(0).reaction.comment.get(position).comment);
        ((SimpleDraweeView) comments[position].findViewById(R.id.comment_user_profile_img))
                .setImageURI(Utils.createImageURI(response.body()
                        .data.get(0).reaction.comment.get(position).commentBy.profile_img));
    }

    void bindPostInfo(Response<com.dotattoo.android.net.res.PostDetail> response) {
        if(response.body().data.get(0).info.user_id.tattooist.status.show_consult) {
            consult.setVisibility(View.VISIBLE);
        } else {
            consult.setVisibility(View.GONE);
        }
        user_id = response.body().data.get(0).info.user_id._id;
        //  set the photo_url variable
        photo_url = response.body().data.get(0).photo_url;

        userName.setText(response.body().data.get(0).info.user_id.name);
        comment.setText(response.body().data.get(0).comment);
        title.setText(response.body().data.get(0).title);
        time.setText(TimeFormatter.getTimeAgo(TimeFormatter
                .convertStringToTimeMillis(response.body().data.get(0).info.post_time)));
        //  load image
        ControllerListener photoListener = new BaseControllerListener<ImageInfo>() {

            @Override
            public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {
                if (imageInfo == null) {
                    return;
                }
                if(imageInfo.getWidth() > imageInfo.getHeight()) {
                    //  set photo imageview width / height to fit screen in same ratio.
                    Point size = getDisplaySize();
                    photo.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
                    photo.getLayoutParams().height = imageInfo.getHeight() * size.x / imageInfo.getWidth();
                    photo.setAspectRatio(imageInfo.getWidth() / imageInfo.getHeight());
                }
            }

            @Override
            public void onIntermediateImageSet(String id, ImageInfo imageInfo) {
                if (imageInfo == null) {
                    return;
                }
                //  set photo imageview width / height to fit screen in same ratio.
                if(imageInfo.getWidth() > imageInfo.getHeight()) {
                    //  set photo imageview width / height to fit screen in same ratio.
                    Point size = getDisplaySize();
                    photo.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
                    photo.getLayoutParams().height = imageInfo.getHeight() * size.x / imageInfo.getWidth();
                    photo.setAspectRatio(imageInfo.getWidth() / imageInfo.getHeight());
                }
            }
        };
        DraweeController photoLoader = Fresco.newDraweeControllerBuilder()
                .setControllerListener(photoListener)
                .setUri(Utils.createImageURI(photo_url))
                .setOldController(photo.getController())
                .build();
        photo.setController(photoLoader);
        userImage.setImageURI(Utils.createImageURI(response.body()
                .data.get(0).info.user_id.profile_img));
        //  reaction
        likeCount.setText("" + response.body().data.get(0).reaction.like.size());

        isLiked = response.body().data.get(0).reaction.liked;
        if(isLiked) {
            like.setImageResource(R.drawable.btn_post_like_done);
        } else {
            like.setImageResource(R.drawable.btn_post_like);
        }
        //  bind part
        try {
            part.setText(response.body().data.get(0).part);
            partImage.getHierarchy().setPlaceholderImage(partImgMap.get(part.getText().toString()));
        } catch (Exception e) {}
    }

    Point getDisplaySize() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size;
    }

    @Override
    public void initView() {
        userName = (TextView) findViewById(R.id.post_detail_user_name);
        photo = (SimpleDraweeView) findViewById(R.id.post_detail_photo);
        userImage = (SimpleDraweeView) findViewById(R.id.post_detail_user_profile_img);
        comment = (TextView) findViewById(R.id.post_detail_comment);
        time = (TextView) findViewById(R.id.post_detail_time);
        title = (TextView) findViewById(R.id.post_detail_title);
        consult = (SimpleDraweeView) findViewById(R.id.post_detail_consult);
        consult.setOnClickListener(this);

        //  reaction init
        like = (ImageView) findViewById(R.id.post_like);
        like.setOnClickListener(this);
        likeCount = (TextView) findViewById(R.id.post_like_count);
        share = (ImageView) findViewById(R.id.post_share);
        shareCount = (TextView) findViewById(R.id.post_share_count);

        //  comment init
        moreComments = (TextView) findViewById(R.id.post_detail_more_comments);
        moreComments.setOnClickListener(this);
        for(int i = 0; i < 3; i++) {
            comments[i] = (RelativeLayout) findViewById(commentsId[i]);
            comments[i].setVisibility(View.GONE);
        }
        //  part
        part = (TextView) findViewById(R.id.post_detail_part);
        partImage = (SimpleDraweeView) findViewById(R.id.post_detail_part_image);

        getPostInfo();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.post_detail_more_comments:
                CommentDialog.showCommentDialog(getSupportFragmentManager(), post_id);
                break;
            case R.id.post_like:
                likePost();
                break;
            case R.id.post_detail_consult:
                ConsultDialog.showConsultDialog(getSupportFragmentManager(),
                        user_id, userName.getText().toString(), post_id, photo_url);
                break;
        }
    }

    void likePost() {
        //  undo like / like request
        if(isLiked) {
            like.setImageResource(R.drawable.btn_post_like);
            like.startAnimation(likeAnim);

            likeCount.setText("" + (Integer.parseInt(likeCount.getText().toString()) - 1));
            postService.undoLike(post_id, SessionPref.getInstance(this).getAccessToken())
                    .enqueue(new Callback<Common>() {
                        @Override
                        public void onResponse(Call<Common> call, Response<Common> response) {
                            if(response.isSuccessful() && response.body().status.equals("OK")) {
                                isLiked = false;
                            } else {
                                onFailure(call, new Throwable());
                            }
                        }

                        @Override
                        public void onFailure(Call<Common> call, Throwable t) {
                            likeCount.setText("" + (Integer.parseInt(likeCount.getText().toString()) + 1));
                            like.setImageResource(R.drawable.btn_post_like_done);
                        }
                    });
        } else {
            //  like
            likeCount.setText("" + (Integer.parseInt(likeCount.getText().toString()) + 1));

            like.startAnimation(likeAnim);
            like.setImageResource(R.drawable.btn_post_like_done);
            postService.like(post_id, SessionPref.getInstance(this).getAccessToken())
                    .enqueue(new Callback<Common>() {
                        @Override
                        public void onResponse(Call<Common> call, Response<Common> response) {
                            if(response.isSuccessful() && response.body().status.equals("OK")) {
                                isLiked = true;
                            } else {
                                onFailure(call, new Throwable());
                            }
                        }

                        @Override
                        public void onFailure(Call<Common> call, Throwable t) {
                            likeCount.setText("" + (Integer.parseInt(likeCount.getText().toString()) - 1));
                            like.setImageResource(R.drawable.btn_post_like);
                        }
                    });
        }
    }

    @Override
    public void initMenu() {

    }
}
