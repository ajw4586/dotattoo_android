package com.dotattoo.android;

import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;

import com.dotattoo.android.core.base.BaseActivity;

public class AboutDotattoo extends BaseActivity {


    private static final int LAYOUT_RESOURCE = R.layout.activity_about_dotattoo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setLayoutResourceId(LAYOUT_RESOURCE);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void init() {

    }

    @Override
    public void initView() {
        TextView toolbar = (TextView) findViewById(R.id.toolbar_title);
        toolbar.setTextSize(20);
        toolbar.setTypeface(null, Typeface.BOLD);
        toolbar.setText("ABOUT");
    }
}
