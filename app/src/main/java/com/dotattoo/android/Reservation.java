package com.dotattoo.android;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dotattoo.android.core.base.BaseActivity;
import com.dotattoo.android.net.req.TattooService;
import com.dotattoo.android.net.res.Common;
import com.dotattoo.android.net.res.Request;
import com.dotattoo.android.pref.SessionPref;
import com.dotattoo.android.util.TimeFormatter;
import com.dotattoo.android.util.Utils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Reservation extends BaseActivity implements View.OnClickListener {


    public static final String  REQUEST_ID = "reservation";

    private static final int LAYOUT_RESOURCE = R.layout.activity_reservate;

    private SimpleDraweeView tattooPhoto;
    private TextView tattooName;
    private TextView reservatePrice, totalPrice;
    private Button reservation;
    private EditText[] userInfoFields = new EditText[3];
    private TextView[] tattooInfoLabels = new TextView[5];
    private EditText birthdayField, phoneField, usernameField;

    private int[] tattooInfoIds = {
            R.id.reservation_field_1,
            R.id.reservation_field_2,
            R.id.reservation_field_3,
            R.id.reservation_field_4,
            R.id.reservation_field_5
    };
    private int[] userInfoIds = {
            R.id.reservation_birthday_field,
            R.id.reservation_phone_num_field,
            R.id.reservation_purchase_user_name_field
    };

    private String requestId, photoUrl;
    private TattooService.Tattoo tattooService;

    private Calendar birthday;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setLayoutResourceId(LAYOUT_RESOURCE);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void init() {
        tattooService = TattooService.getInstance();
        requestId = getIntent().getStringExtra(REQUEST_ID);
        birthday = Calendar.getInstance();
    }

    @Override
    public void initView() {
        tattooPhoto = (SimpleDraweeView) findViewById(R.id.reservation_tattoo_photo);
        reservation = (Button) findViewById(R.id.reservation);
        reservation.setOnClickListener(this);
        tattooName = (TextView) findViewById(R.id.reservation_tattoo_name);
        for(int i = 0; i < 5; i++) {
            tattooInfoLabels[i] = (TextView) findViewById(tattooInfoIds[i]);
            if(i < 3) {
                userInfoFields[i] = (EditText) findViewById(userInfoIds[i]);
            }
        }
        totalPrice = (TextView) findViewById(R.id.reservation_total_price);
        reservatePrice = (TextView) findViewById(R.id.reservation_reservate_price);
        birthdayField = (EditText) findViewById(R.id.reservation_birthday_field);
        phoneField = (EditText) findViewById(R.id.reservation_phone_num_field);
        usernameField = (EditText) findViewById(R.id.reservation_purchase_user_name_field);
        birthdayField.setOnClickListener(this);

        //  get reservation info.
        if(requestId != null) {
            getRequest(requestId);
        }
    }

    void getRequest(String reservationId) {
        tattooService.getRequest(reservationId)
                .enqueue(new Callback<Request>() {
                    @Override
                    public void onResponse(Call<Request> call, Response<Request> response) {
                        if(response.isSuccessful() && response.body().status.equals("OK")) {
                            try {
                                photoUrl = response.body().data.info.photo;
                                tattooPhoto.setImageURI(Utils.createImageURI(photoUrl));
                            } catch (NullPointerException e) {}
                            tattooName.setText(response.body().data.info.tattoo_name);
                            tattooInfoLabels[0].setText(response.body().data.info.genre);
                            tattooInfoLabels[1].setText(response.body().data.info.part);
                            tattooInfoLabels[2].setText(response.body().data.info.place);
                            makeReservateDateString(TimeFormatter.parseDateString(response.body().data.info.date));
                            tattooInfoLabels[4].setText(Utils.formatWon(String.valueOf(response.body().data.info.price)) + "원");
                            totalPrice.setText(Utils.formatWon(String.valueOf(response.body().data.info.price)) + "원");
                            reservatePrice.setText(Utils.formatWon(String.valueOf(response.body().data.info.reservation_price)) + "원");
                        } else {
                            onFailure(call, new Throwable());
                        }
                    }

                    @Override
                    public void onFailure(Call<Request> call, Throwable t) {
                        try {
                            Log.e("Reservation", "onFailure: ", t);
                        } catch (NullPointerException e) {}
                        Toast.makeText(Reservation.this, "네트워크 연결 상태를 확인해주세요.",
                                Toast.LENGTH_SHORT).show();
                    }
                });
    }

    void makeReservateDateString(Calendar cal) {
        tattooInfoLabels[3].setText(cal.get(Calendar.YEAR) + "년 "
                + cal.get(Calendar.MONTH)+1 + "월 "
                + cal.get(Calendar.DATE) + "일 "
                + cal.get(Calendar.HOUR) + "시 "
                + cal.get(Calendar.MINUTE) + "분 ");
    }

    void showConfirmAlert() {
        new AlertDialog.Builder(this)
                .setTitle("위 정보로 예약을 하시겠습니까?")
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd");
                        String birthday_string = format.format(birthday.getTime());
                        tattooService.reservation(SessionPref.getInstance(Reservation.this).getAccessToken(),
                                requestId, phoneField.getText().toString(), birthday_string, usernameField.getText().toString())
                                .enqueue(new Callback<Common>() {
                                    @Override
                                    public void onResponse(Call<Common> call, Response<Common> response) {
                                        if(response.isSuccessful() && response.body().status.equals("OK")) {
                                            Intent i = new Intent(Reservation.this, ReservationResult.class);
                                            i.putExtra(ReservationResult.TATTOO_NAME, tattooName.getText());
                                            i.putExtra(ReservationResult.PRICE, reservatePrice.getText());
                                            i.putExtra(ReservationResult.PHOTO, photoUrl);
                                            startActivity(i);
                                            finish();
                                        } else {
                                            onFailure(call, new Throwable());
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<Common> call, Throwable t) {
                                        Toast.makeText(Reservation.this,
                                                "문제가 발생했습니다. 잠시 후 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
                                    }
                                });
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create().show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.reservation:
                if(birthdayField.getText().toString().equals("")
                        || phoneField.getText().toString().equals("")
                        || usernameField.getText().toString().equals("")) {
                    Toast.makeText(this, "입력란을 모두 채워주세요!", Toast.LENGTH_SHORT).show();
                    return;
                }
                showConfirmAlert();
                break;
            case R.id.reservation_birthday_field:
                Calendar cal = Calendar.getInstance();
                DatePickerDialog dialog = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        birthday.set(year, monthOfYear, dayOfMonth);
                        birthdayField.setText(year + "년 " + (monthOfYear+1) + "월 " + dayOfMonth + "일");
                    }
                }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE));
                dialog.showYearPickerFirst(true);
                dialog.show(getFragmentManager(), "datepicker");
                break;
        }
    }
}
