package com.dotattoo.android;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.dotattoo.android.core.base.BaseActivity;
import com.dotattoo.android.net.req.TattooService;
import com.dotattoo.android.net.res.Common;
import com.dotattoo.android.pref.SessionPref;
import com.dotattoo.android.util.FinishTrigger;
import com.dotattoo.android.util.Utils;
import com.facebook.drawee.view.SimpleDraweeView;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestConfirm extends BaseActivity implements View.OnClickListener {


    public static final String[] DATA = {
            "user_id", "tattoo_image", "tattoo_name", "genre", "place", "part", "date", "term", "price"
    };

    private static final int LAYOUT_RESOURCE = R.layout.activity_request_confirm;

    private TattooService.Tattoo tattooService;

    private Button send;
    private SimpleDraweeView photo;
    private TextView[] fields = new TextView[6];
    private int[] fieldIds = {
            R.id.confirm_tattoo_name,
            R.id.confirm_field_1,
            R.id.confirm_field_2,
            R.id.confirm_field_3,
            R.id.confirm_field_4,
            R.id.confirm_field_5
    };

    private String user_id;
    private File imgFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setLayoutResourceId(LAYOUT_RESOURCE);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void init() {
        tattooService = TattooService.getInstance();
    }

    @Override
    public void initView() {
        send = (Button) findViewById(R.id.request_send);
        send.setOnClickListener(this);
        photo = (SimpleDraweeView) findViewById(R.id.confirm_tattoo_photo);
        for(int i = 0; i < 6; i++) {
            fields[i] = (TextView) findViewById(fieldIds[i]);
        }

        //  get data from request screen
        Intent data = getIntent();
        if(data != null) {
            user_id = data.getStringExtra(DATA[0]);
            imgFile = (File) data.getSerializableExtra(DATA[1]);
            photo.setImageURI(Uri.fromFile(imgFile));
            for(int i = 0; i < 6; i++) {
                if(i < 4) {
                    fields[i].setText(data.getStringExtra(DATA[i + 2]));
                } else if(i == 4) {
                    fields[i].setText(data.getStringExtra(DATA[6]) + " "
                            + data.getStringExtra(DATA[7]));
                } else if(i == 5) {
                    fields[5].setText(data.getStringExtra(DATA[8]));
                }
            }
        }
    }

    void sendRequest() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy년 MM월 dd일 HH시 mm분");
        try {
            tattooService.request(
                    Utils.createTextRequestBody(SessionPref.getInstance(this).getAccessToken()),
                    Utils.createTextRequestBody(SessionPref.getInstance(this).getUserId()),
                    Utils.createTextRequestBody(user_id),
                    Utils.createPhotoRequestBody(imgFile),
                    Utils.createTextRequestBody(fields[0].getText().toString()),
                    Utils.createTextRequestBody(fields[1].getText().toString()),
                    Utils.createTextRequestBody(fields[2].getText().toString()),
                    Utils.createTextRequestBody(fields[3].getText().toString()),
                    Utils.createTextRequestBody(sdf.parse(fields[4].getText().toString()).toString()),
                    Utils.createTextRequestBody(getIntent().getStringExtra(DATA[7])
                            .replace("day", "")),
                    Utils.createTextRequestBody(fields[5].getText().toString()
                            .substring(0, fields[5].getText().length() - 1).replace(",", ""))
            ).enqueue(new Callback<Common>() {
                @Override
                public void onResponse(Call<Common> call, Response<Common> response) {
                    if (response.isSuccessful() && response.body().status.equals("OK")) {
                        if(FinishTrigger.getInstance().getListener() != null) {
                            FinishTrigger.getInstance().getListener().onConfirm();
                        }
                        finish();
                    } else {
                        onFailure(call, new Throwable());
                    }
                }

                @Override
                public void onFailure(Call<Common> call, Throwable t) {
                    Toast.makeText(RequestConfirm.this,
                            "견적을 보내지 못했습니다.", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (ParseException e) {
            Toast.makeText(RequestConfirm.this, "견적을 보내지 못했습니다.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v)                 {
        switch (v.getId()) {
            case R.id.request_send:
                sendRequest();
                break;
        }
    }
}
