package com.dotattoo.android;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dotattoo.android.core.base.BaseActivity;
import com.dotattoo.android.data.TimelineData;
import com.dotattoo.android.data.UserInfoData;
import com.dotattoo.android.net.req.MemberService;
import com.dotattoo.android.net.res.Common;
import com.dotattoo.android.net.res.Profile;
import com.dotattoo.android.net.res.ProfileImage;
import com.dotattoo.android.pref.AccountPref;
import com.dotattoo.android.pref.SessionPref;
import com.dotattoo.android.util.Utils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditUserInfo extends BaseActivity implements View.OnClickListener {


    public static final String USERNAME = "name";
    public static final String COMMENT = "comment";
    public static final String PLACE = "place";
    public static final String PHONE = "phone";
    public static final String IMAGE = "image";
    public static final String USER_ID = "user_id";

    private static final String FORM_DATA = "multipart/form-data";

    private static final int REQUEST_CODE_PHOTO = 1;
    private static final int LAYOUT_RESOURCE = R.layout.activity_edit_user_info;

    private SimpleDraweeView profileImage;
    private EditText username, comment, place, phone;
    private Button submit;

    private MemberService.Member member;

    private Uri imgUri;

    private String user_id, access_token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setLayoutResourceId(LAYOUT_RESOURCE);
        activeFresco(true);
        super.onCreate(savedInstanceState);
    }

    void getArgs() {
        Intent i = getIntent();
        String username = i.getStringExtra(USERNAME);
        String comment = i.getStringExtra(COMMENT);
        String place = i.getStringExtra(PLACE);
        String phone = i.getStringExtra(PHONE);
        user_id = SessionPref.getInstance(this).getUserId();

        this.username.setText(username);
        this.comment.setText(comment);
        this.place.setText(place);
        this.phone.setText(phone);
        String profile_img = i.getStringExtra(IMAGE);
        if(profile_img != null) {
            profileImage.setImageURI(Utils.createImageURI(profile_img));
        }

    }

    void callGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, REQUEST_CODE_PHOTO);
    }

    @Override
    public void init() {
        member = MemberService.getInstance();
        access_token = SessionPref.getInstance(this).getAccessToken();
    }

    @Override
    public void initView() {
        profileImage = (SimpleDraweeView) findViewById(R.id.edit_user_info_profile_image);
        username = (EditText) findViewById(R.id.edit_user_info_name);
        comment = (EditText) findViewById(R.id.edit_user_info_comment);
        place = (EditText) findViewById(R.id.edit_user_info_place);
        phone = (EditText) findViewById(R.id.edit_user_info_phone);
        submit = (Button) findViewById(R.id.edit_user_info_submit);
        profileImage.setOnClickListener(this);
        submit.setOnClickListener(this);

        getArgs();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edit_user_info_profile_image:
                callGallery();
                break;
            case R.id.edit_user_info_submit:
                Map<String, String> fields = new HashMap<>();
                fields.put(PHONE, phone.getText().toString());
                fields.put(COMMENT, comment.getText().toString());
                fields.put(PLACE, place.getText().toString());
                fields.put(USERNAME, username.getText().toString());
                member.updateUserInfo(user_id, access_token, fields)
                .enqueue(new Callback<Common>() {
                    @Override
                    public void onResponse(Call<Common> call, Response<Common> response) {
                        if(response.isSuccessful() && response.body().status.equals("OK")) {
                            saveUserInfo();
                        } else {
                            onFailure(call, new Throwable());
                        }
                    }

                    @Override
                    public void onFailure(Call<Common> call, Throwable t) {
                        Log.d("EditUserInfo", "onFailure: " + t.getMessage());
                        Toast.makeText(EditUserInfo.this, "프로필 정보를 변경하지 못했습니다.",
                                Toast.LENGTH_SHORT).show();
                    }
                });
                break;
        }
    }

    void saveUserInfo() {
        member.getUserInfo(user_id).enqueue(new Callback<Profile>() {
            @Override
            public void onResponse(Call<Profile> call, Response<Profile> response) {
                if (response.isSuccessful() && response.body().status.equals("OK")) {
                    AccountPref.getInstance(EditUserInfo.this).saveUserInfo(response.body());
                    TimelineData.getInstance(EditUserInfo.this).requestTimeline();
                    UserInfoData.getInstance(EditUserInfo.this).requestUserInfo(user_id);
                    Toast.makeText(EditUserInfo.this, "프로필 정보를 변경했습니다.", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    onFailure(call, new Throwable());
                }
            }

            @Override
            public void onFailure(Call<Profile> call, Throwable t) {
                Log.d("EditUserInfo", "onFailure: " + t.getMessage());
                Toast.makeText(EditUserInfo.this, "프로필 정보를 변경하지 못했습니다.",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void initMenu() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CODE_PHOTO && resultCode == RESULT_OK && data != null) {
            imgUri = data.getData();

            Uri tempUrl = Uri.fromFile(new File(getCacheDir(), "crop"));
            //  resize image
            UCrop.Options cropOptions = new UCrop.Options();
            cropOptions.setToolbarColor(Color.BLACK);
            UCrop.of(imgUri, tempUrl)
                    .withOptions(cropOptions)
                    .withAspectRatio(1, 1)
                    .start(this);
        }
        //  cropped image
        if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            final Uri resultUri = UCrop.getOutput(data);
            profileImage.setImageURI(resultUri);

            //  save cropped image
            File f = new File(Environment.getExternalStorageDirectory(), ".tmpCrop.png");
            FileOutputStream out = null;
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(),resultUri);
                out = new FileOutputStream(f);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
                // PNG is a lossless format, the compression factor (100) is ignored
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            MultipartBody.Part photo = MultipartBody.Part.createFormData("photo", f.getName()
                    ,RequestBody.create(MediaType.parse(FORM_DATA), f));
            RequestBody accessToken = RequestBody.create(MediaType.parse(FORM_DATA), access_token);

            member.updateUserProfileImage(user_id, photo, accessToken)
                    .enqueue(new Callback<ProfileImage>() {
                        @Override
                        public void onResponse(Call<ProfileImage> call, Response<ProfileImage> response) {
                            if(response.isSuccessful() && response.body().status.equals("OK")) {
                                Toast.makeText(EditUserInfo.this, "프로필 이미지를 변경했습니다.", Toast.LENGTH_SHORT).show();
                                TimelineData.getInstance(EditUserInfo.this).requestTimeline();
                                AccountPref account = AccountPref.getInstance(EditUserInfo.this);
                                Profile info = account.getUserInfo();
                                info.profile_img = response.body().data.profile_img;
                                account.saveUserInfo(info);
                            } else {
                                onFailure(call, new Throwable("프로필 이미지를 업로드하지 못했습니다."));
                            }
                        }

                        @Override
                        public void onFailure(Call<ProfileImage> call, Throwable t) {
                            Toast.makeText(EditUserInfo.this, "프로필 이미지를 업로드하지 못했습니다.", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else if (resultCode == UCrop.RESULT_ERROR) {
            Toast.makeText(EditUserInfo.this, "프로필 이미지를 업로드하지 못했습니다.", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Get real path for picked image
     * @param uri
     * @return
     */
    public File getPathFromUri(Uri uri){
        Cursor cursor = getContentResolver().query(uri, null, null, null, null );
        cursor.moveToNext();
        String path = cursor.getString( cursor.getColumnIndex( "_data" ) );
        cursor.close();
        return new File(path);
    }

}
