package com.dotattoo.android;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.dotattoo.android.core.base.BaseActivity;
import com.dotattoo.android.net.req.AuthService;
import com.dotattoo.android.net.req.MemberService;
import com.dotattoo.android.net.res.Profile;
import com.dotattoo.android.net.res.Session;
import com.dotattoo.android.pref.AccountPref;
import com.dotattoo.android.pref.SessionPref;
import com.dotattoo.android.util.view.DotattooKakaoLoginButton;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.kakao.auth.ISessionCallback;
import com.kakao.util.exception.KakaoException;

import java.util.Arrays;

import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends BaseActivity {


    private static final int LAYOUT_RESOURCE = R.layout.activity_login;
    private static final int GUIDE_WITH_LOGIN = R.layout.activity_guide;

    private ViewPager page;
    private CircleIndicator indicator;
    private SimpleImagePageAdapter adapter;

    private LoginManager fb_loginMgr;
    private CallbackManager fb_callbackMgr;

    private ISessionCallback kakaoCallback;

    private AuthService.Auth authService;
    private MemberService.Member memberService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setLayoutResourceId(GUIDE_WITH_LOGIN);
        super.onCreate(savedInstanceState);
        initKakao();
        initFacebook();
    }

    @Override
    public void init() {
        //  process if auto login is available.
        autoLogin();

        adapter = new SimpleImagePageAdapter(this);
        authService = AuthService.getInstance();
        memberService = MemberService.getInstance();
        fb_loginMgr = LoginManager.getInstance();
        fb_callbackMgr = CallbackManager.Factory.create();
    }

    @Override
    public void initView() {
        page = (ViewPager) findViewById(R.id.page);
        page.setAdapter(adapter);
        indicator = (CircleIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(page);
    }

    /**
     * Process auto login if SessionPref has session data.
     * Check and if is valid, go to main.
     */
    void autoLogin() {
        Log.d("자동로그인", "자동로그인을 시도합니다.");
        if(!SessionPref.getInstance(getApplicationContext()).getAccessToken().equals("")) {
            startActivity(new Intent(Login.this, Home.class));
            finish();
        }
    }

    /**
     * Initialize facebook login
     */
    void initFacebook() {
        fb_loginMgr.registerCallback(fb_callbackMgr,
                new FacebookCallback<LoginResult>() {

            //  request OAuth with API Server
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken token = loginResult.getAccessToken();
                String user_id = token.getUserId();
                String access_token = token.getToken();
                //  request server to auth this user and process login.
                authService.fbLogin(AuthService.FACEBOOK, access_token, user_id)
                        .enqueue(new Callback<Session>() {
                            @Override
                            public void onResponse(Call<Session> call, Response<Session> response) {
                                if(response.isSuccessful() && response.body().status.equals("OK")) {
                                    //  save session information
                                    saveSessionInfo(response.body());
                                    saveUserInfo(response.body().user_id);
                                } else {
                                    onFailure(call, new Throwable("Failed to login/register to dotattoo server."));
                                }
                            }

                            @Override
                            public void onFailure(Call<Session> call, Throwable t) {
                                alertLoginFailed();
                            }
                        });
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                alertLoginFailed();
            }
        });
    }

    public void alertLoginFailed() {
        AlertDialog alert = new AlertDialog.Builder(this)
                .setMessage(R.string.login_failed)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        alert.show();
    }

    /**
     * Save the current dotattoo session information.
     * It is common for any login way.
     * @param session required session info instance to save.
     */
    void saveSessionInfo(Session session) {
        SessionPref.getInstance(this).saveSession(session);
    }

    /**
     * Initialize kakao login
     */
    void initKakao() {
        kakaoCallback = new ISessionCallback() {
            @Override
            public void onSessionOpened() {
                String access_token = com.kakao.auth.Session.getCurrentSession().getAccessToken();
                Call<Session> kakaoResponseCall = authService.kakaoLogin(AuthService.KAKAO, access_token);
                kakaoResponseCall.enqueue(new Callback<Session>() {
                    @Override
                    public void onResponse(Call<Session> call, Response<Session> response) {
                        if(response.isSuccessful() && response.body().status.equals("OK")) {
                            saveSessionInfo(response.body());
                            saveUserInfo(response.body().user_id);
                        } else {
                            onFailure(call, new Throwable());
                        }
                    }

                    @Override
                    public void onFailure(Call<Session> call, Throwable t) {
                        Log.e("kakao login", "onFailure: ", t);
                        alertLoginFailed();
                    }
                });
            }

            @Override
            public void onSessionOpenFailed(KakaoException exception) {
                try {
                    Log.e("kakao login", "onSessionOpenFailed: " + exception.getMessage());
                } catch (NullPointerException e) {}
            }
        };
        com.kakao.auth.Session.getCurrentSession().addCallback(kakaoCallback);
    }

    @Override
    public void initMenu() {

    }

    void saveUserInfo(String userId) {
        Call<Profile> memberCall = memberService.getUserInfo(userId);
        memberCall.enqueue(new Callback<Profile>() {
            @Override
            public void onResponse(Call<Profile> call, Response<Profile> response) {
                if(response.isSuccessful() && response.body().status.equals("OK")) {
                    AccountPref.getInstance(Login.this).saveUserInfo(response.body());
                    startActivity(new Intent(Login.this, Home.class));
                    finish();
                } else {
                    onFailure(call, new Throwable());
                }
            }

            @Override
            public void onFailure(Call<Profile> call, Throwable t) {
                alertLoginFailed();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (com.kakao.auth.Session.getCurrentSession().handleActivityResult(requestCode, resultCode, data)) {
            return;
        }

            super.onActivityResult(requestCode, resultCode, data);
        fb_callbackMgr.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //  remove kakao callback
        com.kakao.auth.Session.getCurrentSession().removeCallback(kakaoCallback);
    }

    class SimpleImagePageAdapter extends PagerAdapter implements View.OnClickListener {

        private LayoutInflater inflater;
        private int[] imgResources = {
                R.drawable.guide_1,
                R.drawable.guide_2,
                R.drawable.guide_3,
                R.drawable.guide_4,
                R.drawable.guide_5
        };

        //  login activity's views
        private ImageButton fb;
        private DotattooKakaoLoginButton kakao;


        public SimpleImagePageAdapter(Context context) {
            inflater = LayoutInflater.from(context);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View rootView = null;
            //  show login activity
            if(position == 5) {
                rootView = inflater.inflate(LAYOUT_RESOURCE, container, false);
                fb = (ImageButton) rootView.findViewById(R.id.login_facebook);
                kakao = (DotattooKakaoLoginButton) rootView.findViewById(R.id.login_kakao);
                fb.setOnClickListener(this);
                kakao.setOnClickListener(this);
                //  For test
                Button test = (Button) rootView.findViewById(R.id.login_test);
                test.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(Login.this, Home.class));
                    }
                });

                container.addView(rootView);
                return rootView;
            }
            rootView = inflater.inflate(R.layout.full_image, container, false);

            ((ImageView) rootView.findViewById(R.id.image)).setImageResource(imgResources[position]);
            container.addView(rootView);
            return rootView;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.login_facebook:
                    fb_loginMgr.logInWithReadPermissions(Login.this, Arrays.asList("public_profile"));
                    break;
                case R.id.login_kakao:

                    break;
            }
        }

        @Override
        public int getCount() {
            return 6;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }

}
