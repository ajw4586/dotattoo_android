package com.dotattoo.android.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dotattoo.android.R;
import com.dotattoo.android.Reservation;
import com.dotattoo.android.net.res.Request;
import com.dotattoo.android.util.Utils;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ajw45 on 2016-06-18.
 */
public class TattooRequestAdapter extends RecyclerView.Adapter<TattooRequestAdapter.TattooRequest> {


    private static final int ITEMVIEW_LAYOUT_RESOURCE = R.layout.tattoo_request;

    private ArrayList<Request.RequestData> data;

    public TattooRequestAdapter() {
        data = new ArrayList<>();
    }

    public void updateData(List<Request.RequestData> data) {
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public TattooRequest onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(parent.getContext())
                .inflate(ITEMVIEW_LAYOUT_RESOURCE, parent, false);
        return new TattooRequest(item);
    }

    @Override
    public void onBindViewHolder(TattooRequest holder, final int position) {
        try {
            holder.photo.setImageURI(Utils.createImageURI(data.get(position).info.photo));
        } catch (NullPointerException e) {}
        holder.tattooName.setText(data.get(position).info.tattoo_name);
        holder.tattooist.setText(data.get(position).tattooist_id.name);
        holder.price.setText(Utils.formatWon(String.valueOf(data.get(position).info.price)) + "원");
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), Reservation.class);
                i.putExtra(Reservation.REQUEST_ID, data.get(position)._id);
                v.getContext().startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class TattooRequest extends RecyclerView.ViewHolder {
        public SimpleDraweeView photo;
        public TextView tattooName, tattooist, price;
        public TattooRequest(View itemView) {
            super(itemView);
            photo = (SimpleDraweeView) itemView.findViewById(R.id.request_tattoo_photo);
            tattooName = (TextView) itemView.findViewById(R.id.request_tattoo_name);
            tattooist = (TextView) itemView.findViewById(R.id.request_tattooist_name);
            price = (TextView) itemView.findViewById(R.id.request_price);
        }
    }
}
