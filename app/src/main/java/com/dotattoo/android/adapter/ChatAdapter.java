package com.dotattoo.android.adapter;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dotattoo.android.R;
import com.dotattoo.android.Reservation;
import com.dotattoo.android.net.res.ChatList;
import com.dotattoo.android.util.TimeFormatter;
import com.dotattoo.android.util.Utils;
import com.facebook.drawee.view.SimpleDraweeView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ajw45 on 2016-02-28.
 */
public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.Chat> {


    private static final int RECEIVE_MESSAGE_LAYOUT = R.layout.chat_message_receive;
    private static final int SEND_MESSAGE_LAYOUT  = R.layout.chat_message_send;
    private static final int REQUEST_MESSAGE_LAYOUT = R.layout.tattoo_request;
    private static final int TYPE_SEND_TEXT_MESSAGE = 1;
    private static final int TYPE_SEND_PHOTO_MESSAGE = 2;
    private static final int TYPE_RECEIVE_TEXT_MESSAGE = 3;
    private static final int TYPE_RECEIVE_PHOTO_MESSAGE = 4;
    private static final int TYPE_RECEIVE_REQUEST_MESSAGE = 5;
    private static final int TYPE_SEND_REQUEST_MESSAGE = 6;

    public static final String PHOTO_MESSAGE = "photo";
    public static final String TEXT_MESSAGE = "text";
    public static final String REQUEST_MESSAGE = "request";

    private ArrayList<ChatList.Chat> chatData;

    private String myId;

    private File tempImg;

    public ChatAdapter(String myId) {
        chatData = new ArrayList<>();
        this.myId = myId;
    }

    public void updateData(List<ChatList.Chat> data) {
        this.chatData.clear();
        this.chatData.addAll(data);
    }

    /**
     * Add a single message.
     *
     * @param message what to add
     */
    public void addData(ChatList.Chat message) {
        chatData.add(message);
        //  notify adapter to refresh
        //  index can get the list's size -1
        notifyItemInserted(chatData.size() - 1);
    }

    public void addData(ChatList.Chat message, File img) {
        tempImg = img;
        addData(message);
    }

    public void addSentTime(String time, int index) {
        chatData.get(index).time = time;
        notifyItemChanged(index);
        Log.d(getClass().getSimpleName(), "invoke addSentTime on index " + index);
    }

    @Override
    public Chat onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View item = null;
        switch (viewType) {
            case TYPE_RECEIVE_PHOTO_MESSAGE:
            case TYPE_RECEIVE_TEXT_MESSAGE:
                Log.d(getClass().getSimpleName(), "onCreateViewHolder: type= photo");
                item = inflater.inflate(RECEIVE_MESSAGE_LAYOUT, parent, false);
                break;
            case TYPE_SEND_PHOTO_MESSAGE:
            case TYPE_SEND_TEXT_MESSAGE:
                item = inflater.inflate(SEND_MESSAGE_LAYOUT, parent, false);
                break;
        }
        if(viewType == TYPE_RECEIVE_REQUEST_MESSAGE) {
            item = inflater.inflate(REQUEST_MESSAGE_LAYOUT, parent, false);
            return new Request(item);
        } else if(viewType == TYPE_SEND_REQUEST_MESSAGE) {
            item = inflater.inflate(REQUEST_MESSAGE_LAYOUT, parent, false);
            return new Request(item);
        }
        return new Chat(item);
    }

    @Override
    public void onBindViewHolder(Chat holder, final int position) {
        switch (getItemViewType(position)) {
            case TYPE_RECEIVE_PHOTO_MESSAGE:
            case TYPE_SEND_PHOTO_MESSAGE:
                holder.photo.setVisibility(View.VISIBLE);
                try {
                    holder.photo.setImageURI(Utils.createImageURI(chatData.get(position).message));
                } catch (NullPointerException e) {
                    Log.d("ChatAdapter show photo", "onBindViewHolder: Photo data is null");
                    if(tempImg != null) {
                        holder.photo.setImageURI(Uri.fromFile(tempImg));
                    }
                }
                break;
            case TYPE_RECEIVE_TEXT_MESSAGE:
            case TYPE_SEND_TEXT_MESSAGE:
                holder.message.setVisibility(View.VISIBLE);
                holder.message.setText(chatData.get(position).message);
                try {
                    holder.time.setText(TimeFormatter.makePretty(chatData.get(position).time));
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                break;
            //  Handle request messages
            case TYPE_RECEIVE_REQUEST_MESSAGE:
            case TYPE_SEND_REQUEST_MESSAGE:
                ((Request) holder).photo.setImageURI(Utils.createImageURI(chatData
                        .get(position).request.photo));
                ((Request) holder).tattooName.setText(chatData.get(position).request.name);
                ((Request) holder).tattooistName.setText(chatData.get(position)
                        .request.tattooistName);
                ((Request) holder).price.setText(chatData.get(position).request.price + "원");
                ((Request) holder).itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(v.getContext(), Reservation.class);
                        i.putExtra(Reservation.REQUEST_ID, chatData.get(position).message);
                        v.getContext().startActivity(i);
                    }
                });
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        int type = 0;
        //  recognize who is sent.
        if(chatData.get(position).from.equals(myId)) {
            //  determine message type
            if(chatData.get(position).type.equals(PHOTO_MESSAGE)) {
                type = TYPE_SEND_PHOTO_MESSAGE;
            } else if(chatData.get(position).type.equals(TEXT_MESSAGE)) {
                type = TYPE_SEND_TEXT_MESSAGE;
            } else if(chatData.get(position).type.equals(REQUEST_MESSAGE)) {
                type = TYPE_SEND_REQUEST_MESSAGE;
            }
         } else if(chatData.get(position).to.equals(myId)) {
            if(chatData.get(position).type.equals(PHOTO_MESSAGE)) {
                type = TYPE_RECEIVE_PHOTO_MESSAGE;
            } else if(chatData.get(position).type.equals(TEXT_MESSAGE)) {
                type = TYPE_RECEIVE_TEXT_MESSAGE;
            } else if(chatData.get(position).type.equals(REQUEST_MESSAGE)) {
                type = TYPE_RECEIVE_REQUEST_MESSAGE;
            }
        }
        return type;
    }

    @Override
    public int getItemCount() {
        return chatData.size();
    }

    public class Chat extends RecyclerView.ViewHolder {
        public TextView message;
        public TextView time;
        public SimpleDraweeView photo;
        public Chat(View itemView) {
            super(itemView);
            init();
        }
        void init() {
            message = (TextView) itemView.findViewById(R.id.chat_message);
            time = (TextView) itemView.findViewById(R.id.chat_message_time);
            photo = (SimpleDraweeView) itemView.findViewById(R.id.chat_photo);
        }
    }

    public class Request extends Chat {
        public SimpleDraweeView photo;
        public TextView tattooName, tattooistName, price;
        public Request(View itemView) {
            super(itemView);
        }
        @Override
        void init() {
            photo = (SimpleDraweeView) itemView.findViewById(R.id.request_tattoo_photo);
            tattooName = (TextView) itemView.findViewById(R.id.request_tattoo_name);
            tattooistName = (TextView) itemView.findViewById(R.id.request_tattooist_name);
            price = (TextView) itemView.findViewById(R.id.request_price);
        }
    }
}
