package com.dotattoo.android.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dotattoo.android.PostDetail;
import com.dotattoo.android.R;
import com.dotattoo.android.net.res.Timeline;
import com.dotattoo.android.util.Utils;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ajw45 on 2016-03-08.
 */
public class LikedPostAdapter extends RecyclerView.Adapter<LikedPostAdapter.LikedPost> {


    private static final int ITEM_LAYOUT_RESOURCE = R.layout.layout_simple_photo;

    private ArrayList<Timeline.Post> data;

    public LikedPostAdapter() {
        data = new ArrayList<>();
    }

    public void updateData(List<Timeline.Post> data) {
        this.data.clear();
        this.data.addAll(data);
        Log.d("Adapter", "updateData: count: " + data.size());
        notifyDataSetChanged();
    }

    @Override
    public LikedPost onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(ITEM_LAYOUT_RESOURCE, parent, false);
        return new LikedPost(itemView);
    }

    @Override
    public void onBindViewHolder(final LikedPost holder, final int position) {
        ((SimpleDraweeView) (holder.itemView)).setImageURI(Utils.createImageURI(data.get(position).photo_url));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent post = new Intent(holder.itemView.getContext(), PostDetail.class);
                post.putExtra(PostDetail.POST_ID, data.get(position)._id);
                holder.itemView.getContext().startActivity(post);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class LikedPost extends RecyclerView.ViewHolder {
        public LikedPost(View itemView) {
            super(itemView);
        }
    }
}
