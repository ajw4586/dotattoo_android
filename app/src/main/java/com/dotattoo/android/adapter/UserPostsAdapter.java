package com.dotattoo.android.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dotattoo.android.PostDetail;
import com.dotattoo.android.R;
import com.dotattoo.android.net.res.Profile;
import com.dotattoo.android.util.Utils;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ajw45 on 2016-02-25.
 */
public class UserPostsAdapter extends RecyclerView.Adapter<UserPostsAdapter.Post> {


    private static final int ITEMVIEW_LAYOUT_RESOURCE = R.layout.layout_simple_photo;

    private Context mContext;

    private ArrayList<Profile.Posts> photoUrls;

    public UserPostsAdapter(Context context) {
        photoUrls = new ArrayList<>();
        mContext = context;
    }

    public void updateData(List<Profile.Posts> urls) {
        photoUrls.clear();
        photoUrls.addAll(urls);
        notifyDataSetChanged();
    }

    @Override
    public Post onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(ITEMVIEW_LAYOUT_RESOURCE, parent, false);
        return new Post(itemView);
    }

    @Override
    public void onBindViewHolder(Post holder, final int position) {
        ((SimpleDraweeView) holder.itemView).setImageURI(Utils.createImageURI(photoUrls
                .get(position).photo_url));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, PostDetail.class);
                i.putExtra(PostDetail.POST_ID, photoUrls.get(position)._id);
                mContext.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return photoUrls.size();
    }


    class Post extends RecyclerView.ViewHolder {
        public Post(View itemView) {
            super(itemView);
        }
    }
}
