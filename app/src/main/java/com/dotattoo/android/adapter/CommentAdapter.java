package com.dotattoo.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dotattoo.android.R;
import com.dotattoo.android.net.res.Timeline;
import com.dotattoo.android.util.TimeFormatter;
import com.dotattoo.android.util.Utils;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ajw45 on 2016-02-27.
 */
public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.Comment> {


    private static final int ITEMVIEW_LAYOUT_RESOURCE = R.layout.basic_comment;

    private ArrayList<Timeline.Comment> commentData;

    private Context mContext;

    public CommentAdapter(Context context) {
        mContext = context;
        commentData = new ArrayList<>();
    }

    public void updateData(List<Timeline.Comment> data) {
        commentData.clear();
        commentData.addAll(data);
        notifyDataSetChanged();
    }

    public void addData(Timeline.Comment comment) {
        commentData.add(comment);
        notifyItemInserted(getItemCount() - 1);
    }

    @Override
    public Comment onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext)
                .inflate(ITEMVIEW_LAYOUT_RESOURCE, parent, false);
        return new Comment(itemView);
    }

    @Override
    public void onBindViewHolder(Comment holder, int position) {
        holder.userName.setText(commentData.get(position).commentBy.name);
        holder.comment.setText(commentData.get(position).comment);
        holder.time.setText(TimeFormatter.makePretty(commentData.get(position).comment_time));
        holder.profileImg.setImageURI(Utils.createImageURI(commentData.get(position).
                commentBy.profile_img));
    }

    @Override
    public int getItemCount() {
        return commentData.size();
    }

    class Comment extends RecyclerView.ViewHolder {
        public TextView userName, comment, time;
        public SimpleDraweeView profileImg;
        public Comment(View itemView) {
            super(itemView);
            userName = (TextView) itemView.findViewById(R.id.comment_user_name);
            comment = (TextView) itemView.findViewById(R.id.comment_message);
            profileImg = (SimpleDraweeView) itemView.findViewById(R.id.comment_user_profile_img);
            time = (TextView) itemView.findViewById(R.id.comment_time);
        }
    }
}
