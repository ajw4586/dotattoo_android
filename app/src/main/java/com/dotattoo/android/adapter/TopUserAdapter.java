package com.dotattoo.android.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dotattoo.android.R;
import com.dotattoo.android.UserPage;
import com.dotattoo.android.net.res.Member;
import com.dotattoo.android.util.ImageUtils;
import com.dotattoo.android.util.Utils;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;


/**
 * Created by ajw45 on 2016-02-20.
 */
public class TopUserAdapter extends RecyclerView.Adapter<TopUserAdapter.TopUser> {


    private ArrayList<Member> data;

    private Context mContext;

    public TopUserAdapter(@NonNull Context context) {
        mContext = context;
        data = new ArrayList<>();
    }

    public void updateData(com.dotattoo.android.net.res.TopUser data) {
        this.data.clear();
        this.data.addAll(data.data);
        notifyDataSetChanged();
    }

    @Override
    public TopUser onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.top_user, parent, false);
        return new TopUser(v);
    }

    @Override
    public void onBindViewHolder(final TopUser holder, final int position) {

        //  get blurred background image by using profile image.
//                .bitmapTransform(new BlurTransformation(mContext, 25),
//                        new ColorFilterTransformation(mContext, 0x66000000))
        holder.profileBg.getHierarchy().setActualImageColorFilter(new PorterDuffColorFilter(0x66000000, PorterDuff.Mode.DARKEN));
        holder.profileBg.setController(ImageUtils.requestBlurImage(mContext, data.get(position).
                profile_img, holder.profileBg));
        holder.profileImg.setImageURI(Utils.createImageURI(data.get(position).profile_img));

        try {
            holder.userName.setText(data.get(position).name);
            holder.comment.setText(data.get(position).tattooist.comment);

        } catch (NullPointerException e) {}
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent user = new Intent(mContext, UserPage.class);
                user.putExtra(UserPage.USER_ID, data.get(position)._id);
                user.putExtra(UserPage.PROFILE_IMAGE, data.get(position).profile_img);
                mContext.startActivity(user);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class TopUser extends RecyclerView.ViewHolder {

        public TextView userName;
        public TextView comment;
        public SimpleDraweeView profileImg, profileBg;

        public TopUser(View itemView) {
            super(itemView);
            userName = (TextView) itemView.findViewById(R.id.top_user_name);
            comment = (TextView) itemView.findViewById(R.id.top_user_comment);
            profileBg = (SimpleDraweeView) itemView.findViewById(R.id.top_user_bg);
            profileImg = (SimpleDraweeView) itemView.findViewById(R.id.top_user_profile_img);
        }
    }
}
