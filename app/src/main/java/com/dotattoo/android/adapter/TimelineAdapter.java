package com.dotattoo.android.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dotattoo.android.PostDetail;
import com.dotattoo.android.R;
import com.dotattoo.android.UserPage;
import com.dotattoo.android.core.base.BaseActivity;
import com.dotattoo.android.core.base.BaseFragment;
import com.dotattoo.android.data.TimelineData;
import com.dotattoo.android.dialog.CommentDialog;
import com.dotattoo.android.net.req.PostService;
import com.dotattoo.android.net.res.Common;
import com.dotattoo.android.pref.SessionPref;
import com.dotattoo.android.tab.home.TimelineFragment;
import com.dotattoo.android.util.Utils;
import com.dotattoo.android.util.view.CircleProgressDrawable;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ajw45 on 2016-02-01.
 */
public class TimelineAdapter extends RecyclerView.Adapter<TimelineAdapter.Timeline>
        implements TimelineFragment.OnMenuFABStateChangeListener {


    private static final int ITEMVIEW_LAYOUT = R.layout.basic_timeline_post;

    private ArrayList<com.dotattoo.android.net.res.Timeline.Post> posts;

    private Activity activty;
    private Context mContext;
    private FragmentManager mFManager;

    private PostService.Post postService;

    private boolean canClickable = true;

    private Animation likeAnim;

    private TimelineAdapter(Context context) {
        mContext = context;
        posts = new ArrayList<>();
        postService = PostService.getInstance();

        likeAnim = AnimationUtils.loadAnimation(mContext, R.anim.like_pop);
    }

    /**
     * Update this resources to prevent memory leak and many other problem.
     * @param activity
     */
    public void updateInstance(BaseActivity activity) {
        this.activty = activity;
        this.mContext = activity;
        this.mFManager = activity.getSupportFragmentManager();
    }

    /**
     * Update this resources to prevent memory leak and many other problem.
     * @param fragment
     */
    public void updateInstance(BaseFragment fragment) {
        this.mFManager = fragment.getFragmentManager();
        this.mContext = fragment.getContext();
        this.activty = fragment.getActivity();
    }

    public TimelineAdapter(@NonNull BaseActivity activity) {
        this((Context) activity);
        this.activty = activity;
        mFManager = activity.getSupportFragmentManager();
    }

    public TimelineAdapter(@NonNull BaseFragment fragment) {
        this(fragment.getContext());
        activty = fragment.getActivity();
        mFManager = fragment.getFragmentManager();
    }

    public TimelineFragment.OnMenuFABStateChangeListener getClickableListener() {
        return this;
    }

    public void updateData(@NonNull List<com.dotattoo.android.net.res.Timeline.Post> posts) {
        this.posts.clear();
        this.posts.addAll(posts);
        notifyDataSetChanged();
    }

    @Override
    public Timeline onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(parent.getContext())
                .inflate(ITEMVIEW_LAYOUT, parent, false);
        return new Timeline(item);
    }

    @Override
    public void onBindViewHolder(final Timeline holder, final int position) {
        //  Get photo from given post url.
        holder.photo.getHierarchy().setProgressBarImage(new CircleProgressDrawable());
        holder.photo.setImageURI(Utils.createImageURI(posts.get(position).photo_url));
        holder.profileImg.setImageURI(Utils.createImageURI(posts.get(position)
                .info.user_id.profile_img));

        //  catch null information parsed from server.
        try {
            holder.userName.setText(posts.get(position).info.user_id.name);
            holder.comment.setText(posts.get(position).comment);
            holder.title.setText(posts.get(position).title);
        } catch (NullPointerException e) {}
        try {
            if (posts.get(position).reaction.liked) {
                Log.d(getClass().getSimpleName(), "onBindViewHolder: liked post " + position);
                holder.btn_like.setImageResource(R.drawable.btn_post_like_done);
            } else {
                holder.btn_like.setImageResource(R.drawable.btn_post_like);
            }
        } catch (NullPointerException e) {}
        //  set post reaction count
        try {
            holder.like_count.setText("" + posts.get(position).reaction.like.size());
        } catch (NullPointerException e) {}
        try {
            holder.comment_count.setText("" + posts.get(position).reaction.comment.size());
        } catch (NullPointerException e) {}
        try {
            holder.share_count.setText("" + posts.get(position).reaction.share.size());
        } catch (NullPointerException e) {}


        View.OnClickListener click = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!canClickable) {
                    Log.d(getClass().getSimpleName(), "onClick: blocked");
                    return;
                }
                switch (v.getId()) {
                    case R.id.post_like:
                        //  already liked state
                        if(posts.get(position).reaction.liked) {
                            unlikePost(holder, position);
                        //  request like
                        } else {
                            likePost(holder, position);
                        }
                        break;
                    //  show post detail and popup keyboard and move focus to comment field
                    case R.id.post_write_comment:
                    case R.id.post_write_comment_count:
                        CommentDialog.showCommentDialog(mFManager, posts.get(position)._id);
                    break;
                    //  show share options
                    case R.id.post_share:
                        break;
                    case R.id.post_overflow_btn:
                        //  Show dialog
                        showOptionDialog(position);
                        break;
                    //  show user profile
                    case R.id.post_user_img:
                    case R.id.post_user_name:
                        Intent userInfo = new Intent(mContext, UserPage.class);
                        userInfo.putExtra(UserPage.USER_ID, posts.get(position).info.user_id._id);
                        userInfo.putExtra(UserPage.PROFILE_IMAGE, posts.get(position).info.user_id.profile_img);
                        mContext.startActivity(userInfo);
                        break;
                    //  show post detail page
                    case R.id.post_image:
                        //  show detail page
                        Intent post = new Intent(mContext, PostDetail.class);

                        post.putExtra(PostDetail.POST_ID, posts.get(position)._id);
                        if(Build.VERSION.SDK_INT >= 21) {
                            activty.startActivity(post,
                                    ActivityOptionsCompat.makeSceneTransitionAnimation(activty,
                                            holder.photo, mContext.getString(R.string.post_image))
                                            .toBundle());
                        } else {
                            mContext.startActivity(post);
                        }
                        break;
                }
            }
        };


        //  Move to User Page
        holder.btn_like.setOnClickListener(click);
        holder.btn_comment.setOnClickListener(click);
        holder.comment_count.setOnClickListener(click);
        holder.btn_share.setOnClickListener(click);
        holder.btn_overflow.setOnClickListener(click);
        holder.photo.setOnClickListener(click);
        holder.userName.setOnClickListener(click);
        holder.profileImg.setOnClickListener(click);
    }

    void likePost(final Timeline holder, final int position) {
        holder.btn_like.setImageResource(R.drawable.btn_post_like_done);
        holder.btn_like.startAnimation(likeAnim);

        holder.like_count.setText("" + (Integer.parseInt(holder.like_count.getText().toString()) + 1));
        //  request like post API.
        Call<Common> like = postService.like(posts.get(position)._id,
                SessionPref.getInstance(mContext).getAccessToken());
        like.enqueue(new Callback<Common>() {
            @Override
            public void onResponse(Call<Common> call, Response<Common> response) {
                if(response.isSuccessful() && response.body().status.equals("OK")) {
                    Log.d(getClass().getSimpleName(), "onResponse: like success");
                    posts.get(position).reaction.liked = true;
                } else {
                    onFailure(call, new Throwable());
                }
            }

            @Override
            public void onFailure(Call<Common> call, Throwable t) {
                Log.d(getClass().getSimpleName(), "onFailure: like failed.");
                holder.btn_like.setImageResource(R.drawable.btn_post_like);
                holder.like_count.setText("" + (Integer.parseInt(holder.like_count.getText().toString()) - 1));
            }
        });
    }

    void unlikePost(final Timeline holder, final int position) {
        holder.btn_like.setImageResource(R.drawable.btn_post_like);
        holder.btn_like.startAnimation(likeAnim);
        //  increase like count
        holder.like_count.setText("" + (Integer.parseInt(holder.like_count.getText().toString()) - 1));

        //  Request cancel like post API
        Call<Common> undoLike = postService.undoLike(posts.get(position)._id,
                SessionPref.getInstance(mContext).getAccessToken());
        undoLike.enqueue(new Callback<Common>() {

            @Override
            public void onResponse(Call<Common> call, Response<Common> response) {
                if(response.isSuccessful() && response.body().status.equals("OK")) {
                    Log.d(getClass().getSimpleName(), "onResponse: unlike success");
                    posts.get(position).reaction.liked = false;
                } else {
                    onFailure(call, new Throwable());
                }
            }

            @Override
            public void onFailure(Call<Common> call, Throwable t) {
                Log.d(getClass().getSimpleName(), "onFailure: unlike failed");
                holder.btn_like.setImageResource(R.drawable.btn_post_like_done);
                //  decrease like count
                holder.like_count.setText("" + (Integer.parseInt(holder.like_count.getText().toString()) + 1));
            }
        });
    }

    void showOptionDialog(final int position) {
        CharSequence[] options = { mContext.getString(R.string.delete) };
        AlertDialog.Builder dialog = new AlertDialog.Builder(mContext)
                .setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //  show delete dialog
                        showDeleteDialog(position);
                        dialog.dismiss();
                    }
                });
        dialog.create().show();
    }

    void showDeleteDialog(final int position) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(mContext)
                .setMessage(R.string.ask_delete)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deletePost(posts.get(position)._id);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        dialog.create().show();
    }

    void deletePost(String postId) {
        postService.deletePost(postId, SessionPref.getInstance(mContext).getAccessToken())
        .enqueue(new Callback<Common>() {
            @Override
            public void onResponse(Call<Common> call, Response<Common> response) {
                if(response.isSuccessful() && response.body().status.equals("OK")) {
                    TimelineData.getInstance((BaseActivity) activty).requestTimeline();
                } else {
                    onFailure(call, new Throwable());
                }
            }

            @Override
            public void onFailure(Call<Common> call, Throwable t) {
                Toast.makeText(mContext, "게시글을 지울 수 없습니다.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onMenuFABStateChanged(boolean isActive) {
        Log.d(getClass().getSimpleName(), "onMenuFABStateChanged: called in adapter " + isActive);
        //  change active state for items to have always opposite state with fab.
        canClickable = !isActive;
    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

    class Timeline extends RecyclerView.ViewHolder {
        public SimpleDraweeView photo, profileImg;
        public TextView comment, title, userName, like_count, comment_count, share_count;
        public ImageView btn_like, btn_comment, btn_share, btn_overflow;

        public Timeline(View itemView) {
            super(itemView);
            photo = (SimpleDraweeView) itemView.findViewById(R.id.post_image);
            comment = (TextView) itemView.findViewById(R.id.post_comment);
            btn_like = (ImageView) itemView.findViewById(R.id.post_like);
            btn_comment = (ImageView) itemView.findViewById(R.id.post_write_comment);
            btn_share = (ImageView) itemView.findViewById(R.id.post_share);
            btn_overflow = (ImageButton) itemView.findViewById(R.id.post_overflow_btn);
            title = (TextView) itemView.findViewById(R.id.post_main_title);
            //  count
            like_count = (TextView) itemView.findViewById(R.id.post_like_count);
            comment_count = (TextView) itemView.findViewById(R.id.post_write_comment_count);
            share_count = (TextView) itemView.findViewById(R.id.post_share_count);
            //  top user info
            userName = (TextView) itemView.findViewById(R.id.post_user_name);
            profileImg = (SimpleDraweeView) itemView.findViewById(R.id.post_user_img);
        }
    }
}
