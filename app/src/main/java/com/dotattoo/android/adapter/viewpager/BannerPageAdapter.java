package com.dotattoo.android.adapter.viewpager;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dotattoo.android.R;
import com.dotattoo.android.net.res.Banners;
import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ajw45 on 2016-06-03.
 */
public class BannerPageAdapter extends PagerAdapter {


    private static final int BANNER_LAYOUT_RESOURCE = R.layout.main_banner;

    private Context mContext;
    private LayoutInflater inflater;

    private ArrayList<Banners.Banner> data;

    public BannerPageAdapter(Context context) {
        this.mContext = context;
        this.inflater = LayoutInflater.from(context);
        data = new ArrayList<>();
    }

    //  set data count by received data
    //  update viewpager for the new data set
    //  default offline banner will never remove
    public void updateData(List<Banners.Banner> targetData) {
        data.clear();
        data.addAll(targetData);
        notifyDataSetChanged();
    }

    //  First, load image from static resource
    //  then, load other images from network
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View v = null;
        v = inflater.inflate(BANNER_LAYOUT_RESOURCE, container, false);
        //  load from static resource
        if(position == 0) {
            Log.d("Banner", "instantiateItem: load static resource");
            SimpleDraweeView image = ((SimpleDraweeView) v.findViewById(R.id.main_banner_image));
            image.getHierarchy().setPlaceholderImage(R.drawable.sample_banner);
            image.getHierarchy().setActualImageScaleType(ScalingUtils.ScaleType.FIT_START);
            container.addView(v);
            return v;
        }

        return v;
    }

    //  one is app inner static banner
    @Override
    public int getCount() {
        return data.size() + 1;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        if(isViewFromObject(container.getChildAt(position), object)) {
            container.removeView((View) object);
        }
    }



    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}
