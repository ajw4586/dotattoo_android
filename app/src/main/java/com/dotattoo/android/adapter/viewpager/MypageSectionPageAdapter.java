package com.dotattoo.android.adapter.viewpager;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.dotattoo.android.R;
import com.dotattoo.android.tab.mypage.LikedPostFragment;
import com.dotattoo.android.tab.mypage.TattooRequestFragment;
import com.dotattoo.android.tab.mypage.TattooReservationFragment;

/**
 * Created by ajw45 on 2016-01-22.
 */
public class MypageSectionPageAdapter extends FragmentPagerAdapter {


    private LikedPostFragment likedPost;
    private TattooRequestFragment tattooRequest;
    private TattooReservationFragment tattooReservation;

    private Context context;

    public MypageSectionPageAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
        likedPost = new LikedPostFragment();
        tattooRequest = new TattooRequestFragment();
        tattooReservation = new TattooReservationFragment();
    }

    @Override
    public Fragment getItem(int position) {
        Fragment target = null;
        switch (position) {
            case 0:
                target = likedPost;
                break;
            case 1:
                target = tattooRequest;
                break;
            case 2:
                target = tattooReservation;
                break;
        };
        return target;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String[] titles = context.getResources().getStringArray(R.array.mypage_tab_name);
        return titles[position];
    }
}
