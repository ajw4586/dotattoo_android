package com.dotattoo.android.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dotattoo.android.R;
import com.dotattoo.android.net.res.Search;
import com.dotattoo.android.util.Utils;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by ajw45 on 2016-03-18.
 */
public class SearchResultAdapter extends RecyclerView.Adapter {

    public static final int POST_RESULT = 1;
    public static final int MEMBER_RESULT = 2;
    private static final int POST_SEARCH_RESULT_VIEW = R.layout.basic_post_search_result;
    private static final int MEMBER_SEARCH_RESULT_VIEW = R.layout.basic_member_search_result;

    private ArrayList<Search.SearchPost> postData;
    private ArrayList<Search.SearchMember> memberDAta;

    private int searchType = 0;

    public SearchResultAdapter(int searchType) {
        this.searchType = searchType;
        switch (searchType) {
            case POST_RESULT:
                postData = new ArrayList<>();
                break;
            case MEMBER_RESULT:
                memberDAta = new ArrayList<>();
                break;
        }
    }

    public void updateData(List data) {
        switch (getSearchType()) {
            case POST_RESULT:
                postData.clear();
                postData.addAll(data);
                notifyDataSetChanged();
                break;
            case MEMBER_RESULT:
                memberDAta.clear();
                memberDAta.addAll(data);
                notifyDataSetChanged();
                break;
        }
    }

    public int getSearchType() {
        return searchType;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        RecyclerView.ViewHolder holder = null;
        switch (getSearchType()) {
            case POST_RESULT:
                holder = new PostResult(inflater.inflate(POST_SEARCH_RESULT_VIEW, parent, false));
                break;
            case MEMBER_RESULT:
                holder = new MemberResult(inflater.inflate(MEMBER_SEARCH_RESULT_VIEW, parent, false));
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        //  bind view by search type
        if(getSearchType() == POST_RESULT) {
            PostResult result = (PostResult) holder;
            result.photo.setImageURI(Utils.createImageURI(postData.get(position).photo_url));
            result.profileImg.setImageURI(Utils.createImageURI(postData.get(position)
                    .info.user_id.profile_img));
            result.title.setText(postData.get(position).title);
            result.userName.setText(postData.get(position).info.user_id.name);
        } else if(getSearchType() == MEMBER_RESULT) {
            //  TODO bind member result view
        }
    }

    @Override
    public int getItemCount() {
        if(getSearchType() == POST_RESULT) {
            return postData.size();
        } else if(getSearchType() == MEMBER_RESULT) {
            return memberDAta.size();
        }
        return 0;
    }

    class PostResult extends RecyclerView.ViewHolder {
        public SimpleDraweeView photo, profileImg;
        public TextView userName, title;
        public PostResult(View itemView) {
            super(itemView);
            photo = (SimpleDraweeView) itemView.findViewById(R.id.post_search_result_image);
            profileImg = (SimpleDraweeView) itemView.findViewById(R.id.post_search_result_user_image);
            title = (TextView) itemView.findViewById(R.id.post_search_result_title);
            userName = (TextView) itemView.findViewById(R.id.post_search_result_user_name);
        }
    }
    class MemberResult extends RecyclerView.ViewHolder {
        public MemberResult(View itemView) {
            super(itemView);
        }
    }
}
