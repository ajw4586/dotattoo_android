package com.dotattoo.android.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import com.dotattoo.android.R;

/**
 * Created by ajw45 on 2016-04-10.
 */
public class SimpleListAdapter extends RecyclerView.Adapter<SimpleListAdapter.SimpleListItem> {


    private static final int ITEMVIEW_LAYOUT_RESOURCE = R.layout.simple_list_item;

    private String[] arrayData;
    private OnListItemClickListener listener;
    private int selected = -1;

    public interface OnListItemClickListener {
        public void onListItemClick(int position, String text);
    }

    public SimpleListAdapter(String[] array) {
        arrayData = array;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }

    public void setOnListItemClickListener(OnListItemClickListener listener) {
        this.listener = listener;
        Log.d("place picker adapter", "listener registered.");
    }

    @Override
    public SimpleListItem onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(ITEMVIEW_LAYOUT_RESOURCE, parent, false);
        return new SimpleListItem(v);
    }

    @Override
    public void onBindViewHolder(final SimpleListItem holder, final int position) {
        if(selected == position) {
            holder.radio.setChecked(true);
        }
        holder.text.setText(arrayData[position]);
        holder.radio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    //  call listener
                    if(listener != null) {
                        Log.d("placepicker adapter", "onCheckedChanged: called " + position);
                        listener.onListItemClick(position, holder.text.getText().toString());
                    }
                }
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.radio.setChecked(true);
                //  call listener
                if(listener != null) {
                    Log.d("placepicker adapter", "onCheckedChanged: called " + position);
                    listener.onListItemClick(position, holder.text.getText().toString());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayData.length;
    }

    class SimpleListItem extends RecyclerView.ViewHolder {
        public TextView text;
        public RadioButton radio;
        public SimpleListItem(View itemView) {
            super(itemView);
            text = (TextView) itemView.findViewById(R.id.simple_text);
            radio = (RadioButton) itemView.findViewById(R.id.simple_radio);
        }
    }
}
