package com.dotattoo.android.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dotattoo.android.R;
import com.dotattoo.android.net.res.Notifications;
import com.dotattoo.android.util.TimeFormatter;
import com.dotattoo.android.util.Utils;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ajw45 on 2016-04-06.
 */
public class AlarmAdapter extends RecyclerView.Adapter<AlarmAdapter.Alarm> {


    private static final String ACTION_COMMENT = "comment";
    private static final String ACTION_LIKE = "like";
    private static final int ITEMVIEW_LAYOUT_RESOURCE = R.layout.basic_alarm;

    private ArrayList<Notifications.Notification> data;

    public AlarmAdapter() {
        data = new ArrayList<>();
    }

    public void updateData(List<Notifications.Notification> data) {
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    public void addData(Notifications.Notification data) {
        this.data.add(data);
        notifyItemInserted(this.data.size());
    }

    @Override
    public Alarm onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(parent.getContext())
                .inflate(ITEMVIEW_LAYOUT_RESOURCE, parent, false);
        return new Alarm(item);
    }

    @Override
    public void onBindViewHolder(Alarm holder, int position) {
        holder.userImage.setImageURI(Utils.createImageURI(data.get(position).user_id.profile_img));
        holder.time.setText(TimeFormatter.makePretty(data.get(position).time));
        if(data.get(position).action.equals(ACTION_COMMENT)) {
            holder.message.setText(data.get(position).user_id.name + "님이 게시물에 댓글을 남겼습니다.");
        } else if(data.get(position).action.equals(ACTION_LIKE)) {
            holder.message.setText(data.get(position).user_id.name + "님이 게시물을 좋아합니다.");
        } else {
            holder.message.setText(data.get(position).user_id.name + "님으로부터 알림이 있습니다.");
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class Alarm extends RecyclerView.ViewHolder {
        public TextView message, time;
        public SimpleDraweeView userImage;
        public Alarm(View itemView) {
            super(itemView);
            message = (TextView) itemView.findViewById(R.id.alarm_message);
            time = (TextView) itemView.findViewById(R.id.alarm_time);
            userImage = (SimpleDraweeView) itemView.findViewById(R.id.alarm_image);
        }
    }
}
