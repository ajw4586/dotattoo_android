package com.dotattoo.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by ajw45 on 2016-03-01.
 */
public class ChatUserListAdapter extends RecyclerView.Adapter<ChatUserListAdapter.ChatUserList> {


    private Context mContext;

    public ChatUserListAdapter(Context context) {
        mContext = context;
    }

    @Override
    public ChatUserList onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(ChatUserList holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    class ChatUserList extends RecyclerView.ViewHolder {
        public ChatUserList(View itemView) {
            super(itemView);
        }
    }
}
