package com.dotattoo.android.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dotattoo.android.R;
import com.dotattoo.android.net.res.Reservation;
import com.dotattoo.android.util.Utils;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ajw45 on 2016-06-18.
 */
public class TattooReservationAdapter extends RecyclerView.Adapter<TattooReservationAdapter.TattooReservation> {


    private static final int ITEMVIEW_LAYOUT_RESOURCE = R.layout.item_tattoo_reservation;


    ArrayList<Reservation> data;

    public TattooReservationAdapter() {
        data = new ArrayList<>();
    }

    public void updateData(List<Reservation> data) {
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public TattooReservation onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(parent.getContext())
                .inflate(ITEMVIEW_LAYOUT_RESOURCE, parent, false);
        return new TattooReservation(item);
    }

    @Override
    public void onBindViewHolder(TattooReservation holder, int position) {
        holder.photo.setImageURI(Utils.createImageURI(data.get(position).request.data.info.photo));
        holder.reservationName.setText(data.get(position).request.data.info.tattoo_name);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class TattooReservation extends RecyclerView.ViewHolder {
        public SimpleDraweeView photo;
        public TextView reservationName;
        public TattooReservation(View itemView) {
            super(itemView);
            photo = (SimpleDraweeView) itemView.findViewById(R.id.reservation_tattoo_photo);
            reservationName = (TextView) itemView.findViewById(R.id.reservation_tattoo_name);
        }
    }
}
