package com.dotattoo.android.adapter.viewpager;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.dotattoo.android.R;
import com.dotattoo.android.tab.home.AlarmFragment;
import com.dotattoo.android.tab.home.FindFragment;
import com.dotattoo.android.tab.home.MypageFragment;
import com.dotattoo.android.tab.home.QnaFragment;
import com.dotattoo.android.tab.home.TimelineFragment;

/**
 * Created by ajw45 on 2016-01-21.
 */
public class HomePageAdapter extends FragmentPagerAdapter {

    private Context context;

    private TimelineFragment timeline;
    private FindFragment find;
    private QnaFragment qna;
    private AlarmFragment alarm;
    private MypageFragment mypage;

    public HomePageAdapter(FragmentManager fm, Context context) {
        super(fm);
        timeline = new TimelineFragment();
        find = new FindFragment();
        qna = new QnaFragment();
        alarm = new AlarmFragment();
        mypage = new MypageFragment();

        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment tab = null;
        switch (position) {
            case 0:
                tab = timeline;
                break;
            case 1:
                tab = find;
                break;
            case 2:
                tab = qna;
                break;
            case 3:
                tab = alarm;
                break;
            case 4:
                tab = mypage;
                break;
        }
        return tab;
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String[] tab_name = context.getResources().getStringArray(R.array.tab_name);
        return tab_name[position];
    }
}
