package com.dotattoo.android.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dotattoo.android.Chat;
import com.dotattoo.android.R;
import com.dotattoo.android.pref.SessionPref;
import com.dotattoo.android.util.TimeFormatter;
import com.dotattoo.android.util.Utils;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by ajw45 on 2016-03-06.
 */
public class ChatRoomAdapter extends RecyclerView.Adapter<ChatRoomAdapter.ChatRoom> {


    private static final int ITEMVIEW_LAYOUT_RESOURCE = R.layout.basic_chat_room;

    private Context mContext;
    private ArrayList<com.dotattoo.android.net.res.ChatRoom.Room> roomData;

    public ChatRoomAdapter(Context context) {
        roomData = new ArrayList<>();
        mContext = context;
    }

    public void updateData(List<com.dotattoo.android.net.res.ChatRoom.Room> data) {
        roomData.clear();
        roomData.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public ChatRoom onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(parent.getContext())
                .inflate(ITEMVIEW_LAYOUT_RESOURCE, parent, false);
        return new ChatRoom(item);
    }

    @Override
    public void onBindViewHolder(ChatRoom holder, final int position) {
        //  send by other
        if(roomData.get(position).to._id.equals(SessionPref.getInstance(mContext).getUserId())) {
            try {
                holder.profileImg.setImageURI(Utils.createImageURI(roomData.get(position)
                        .from.profile_img));
            } catch (NullPointerException e) {}
            holder.userName.setText(roomData.get(position).from.name);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(mContext, Chat.class);
                    i.putExtra(Chat.USER_ID, roomData.get(position).from._id);
                    i.putExtra(Chat.USER_NAME, roomData.get(position).from.name);
                    mContext.startActivity(i);
                }
            });
        }
        //  send by me
        else {
            try {
                holder.profileImg.setImageURI(Utils.createImageURI(roomData.get(position)
                        .to.profile_img));
            } catch (NullPointerException e) {}
            holder.userName.setText(roomData.get(position).to.name);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(mContext, Chat.class);
                    i.putExtra(Chat.USER_ID, roomData.get(position).to._id);
                    i.putExtra(Chat.USER_NAME, roomData.get(position).to.name);
                    mContext.startActivity(i);
                }
            });
         }
        switch (roomData.get(position).type) {
            case "text":
                holder.message.setText(roomData.get(position).message);
                break;
            case "request":
                holder.message.setText("타투 견적");
                break;
            case "photo":
                holder.message.setText("사진");
                break;
            case "reservation":
                holder.message.setText("타투 예약");
                break;
        }
        holder.lastTime.setText(TimeFormatter.makePretty(roomData.get(position).time));
    }

    @Override
    public int getItemCount() {
        return roomData.size();
    }

    class ChatRoom extends RecyclerView.ViewHolder {
        public TextView userName, message, lastTime;
        public SimpleDraweeView profileImg;
        public ChatRoom(View itemView) {
            super(itemView);
            userName = (TextView) itemView.findViewById(R.id.chat_room_user_name);
            message = (TextView) itemView.findViewById(R.id.chat_room_message);
            lastTime = (TextView) itemView.findViewById(R.id.chat_room_last_time);
            profileImg = (SimpleDraweeView) itemView.findViewById(R.id.chat_room_user_image);
        }
    }
}
