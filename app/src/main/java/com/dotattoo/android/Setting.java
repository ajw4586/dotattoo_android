package com.dotattoo.android;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.dotattoo.android.core.base.BaseActivity;
import com.dotattoo.android.pref.AccountPref;
import com.dotattoo.android.pref.SessionPref;
import com.zcw.togglebutton.ToggleButton;

public class Setting extends BaseActivity implements View.OnClickListener {


    private static final int LAYOUT_RESOUCE = R.layout.activity_setting;

    private ToggleButton push;
    private ViewGroup editProfile, aboutDotattoo, personalDataPolicy, license, officialNotification, tattooistRequest, logout;

    private AccountPref accountPref;
    private boolean push_alarm_state = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setLayoutResourceId(LAYOUT_RESOUCE);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void init() {
        accountPref = AccountPref.getInstance(this);
        push_alarm_state =accountPref.getPushAlarmState();
    }

    @Override
    public void initView() {
        TextView setting_label = (TextView) findViewById(R.id.toolbar_title);
        setting_label.setText("SETTING");
        setting_label.setTextSize(20);
        setting_label.setTypeface(null, Typeface.BOLD);
        editProfile = (ViewGroup) findViewById(R.id.edit_profile);
        editProfile.setOnClickListener(this);
        aboutDotattoo = (ViewGroup) findViewById(R.id.about_dotattoo);
        aboutDotattoo.setOnClickListener(this);
        personalDataPolicy = (ViewGroup) findViewById(R.id.personal_data_policy);
        personalDataPolicy.setOnClickListener(this);
        license = (ViewGroup) findViewById(R.id.license);
        license.setOnClickListener(this);
        officialNotification = (ViewGroup) findViewById(R.id.official_notification);
        officialNotification.setOnClickListener(this);
        tattooistRequest = (ViewGroup) findViewById(R.id.tattooist_request);
        tattooistRequest.setOnClickListener(this);
        logout = (ViewGroup) findViewById(R.id.logout);
        logout.setOnClickListener(this);
        push = (ToggleButton) findViewById(R.id.push_alarm);
        //  if push_alarm_state is false
        if(!push_alarm_state) {
            push.setToggleOff(false);
        }
        push.setOnToggleChanged(new ToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(boolean on) {
                accountPref.savePushAlarmState(on);
                Toast.makeText(Setting.this, "알람을 " + (on ? "켰습니다." : "껐습니다."), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edit_profile:
                Intent profile = new Intent(this, EditUserInfo.class);
                AccountPref account = AccountPref.getInstance(this);
                try {
                    profile.putExtra(EditUserInfo.USER_ID, SessionPref.getInstance(this).getUserId());
                    profile.putExtra(EditUserInfo.USERNAME, account.getUserInfo().name);
                    profile.putExtra(EditUserInfo.IMAGE, account.getUserInfo().profile_img);
                } catch (NullPointerException e) {
                    Log.d("setting", "onClick: editUserInfo core info null passed");
                } try {
                    profile.putExtra(EditUserInfo.COMMENT, account.getUserInfo().tattooist.comment);
                } catch (NullPointerException e) {
                    Log.d("setting", "onClick: editUserInfo comment null passed");
                } try {
                    profile.putExtra(EditUserInfo.PHONE, account.getUserInfo().phone);
                } catch (NullPointerException e) {
                    Log.d("setting", "onClick: editUserInfo phone num null passed");
                } try {
                    profile.putExtra(EditUserInfo.PLACE, account.getUserInfo().tattooist.place);
                } catch (NullPointerException e) {
                    Log.d("setting", "onClick: editUserInfo place null passed");
                }
                startActivity(profile);
                break;
            case R.id.about_dotattoo:
                startActivity(new Intent(this, AboutDotattoo.class));
                break;
            case R.id.personal_data_policy:
                startActivity(new Intent(this, PersonalDataPolicy.class));
                break;
            case R.id.license:
                startActivity(new Intent(this, License.class));
                break;
            case R.id.official_notification:
                startActivity(new Intent(this, OfficialNotification.class));
                break;
            case R.id.tattooist_request:
                startActivity(new Intent(this, TattooistRequest.class));
                break;
            case R.id.logout:
                logout();
                break;
        }
    }

    //  clear session and account pref
    //  then move to Login Activity.
    void logout() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.ask_logout)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(SessionPref.getInstance(Setting.this).clear()
                                && AccountPref.getInstance(Setting.this).clear()) {
                            startActivity(new Intent(Setting.this, Login.class));
                            finish();
                        } else {
                            Toast.makeText(Setting.this, "로그아웃 할 수 없습니다. 다시 시도해 주세요.", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create().show();
    }
}
