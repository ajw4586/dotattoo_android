package com.dotattoo.android;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dotattoo.android.core.base.BaseActivity;
import com.dotattoo.android.data.TimelineData;
import com.dotattoo.android.net.req.PostService;
import com.dotattoo.android.net.res.Common;
import com.dotattoo.android.pref.SessionPref;
import com.dotattoo.android.util.ImageUtils;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WritePost extends BaseActivity implements View.OnClickListener {


    private static final int LAYOUT_RES = R.layout.activity_write_post;
    private static final int REQUEST_CODE_PHOTO = 1;
    private static final String FORM_DATA = "multipart/form-data";

    private Toolbar toolbar;
    private TextView toolbar_title;
    private ImageView photo;
    private Button selectPart, upload;
    private EditText comment, title;

    private AlertDialog dialog;

    private Uri imgUri;
    private File imgFile;

    private PostService.Post uploadService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setLayoutResourceId(LAYOUT_RES);
        activeFresco(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void init() {
        uploadService = PostService.getInstance();
        createSelectPartDialog();
    }

    @Override
    public void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar.setTitle(getString(R.string.upload_post));
        toolbar_title.setText(getResources().getString(R.string.write_post));
        setSupportActionBar(toolbar);

        photo = (ImageView) findViewById(R.id.write_post_photo);
        photo.setOnClickListener(this);
        selectPart = (Button) findViewById(R.id.write_post_part);
        upload = (Button) findViewById(R.id.write_post_upload);
        selectPart.setOnClickListener(this);
        upload.setOnClickListener(this);
        comment = (EditText) findViewById(R.id.write_post_comment);
        title = (EditText) findViewById(R.id.write_post_main_title);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.write_post_photo:
                callGallery();
                break;
            case R.id.write_post_part:
                showSelectPartDialog();
                break;
            case R.id.write_post_upload:
                upload();
                break;
        }
    }

    void upload() {
        if(imgFile == null || this.selectPart.getText().toString().equals(getResources().getString(R.string.select_part))) {
            return;
        }
        RequestBody comment, accessToken, part, title;
        MultipartBody.Part photo;

        comment = RequestBody.create(MediaType.parse(FORM_DATA), this.comment.getText().toString());
        accessToken = RequestBody.create(MediaType.parse(FORM_DATA), SessionPref.getInstance(this).getAccessToken());
        part = RequestBody.create(MediaType.parse(FORM_DATA), this.selectPart.getText().toString());
        title = RequestBody.create(MediaType.parse(FORM_DATA), this.title.getText().toString());

        photo = MultipartBody.Part.createFormData("photo", imgFile.getName(),
                RequestBody.create(MediaType.parse(FORM_DATA), imgFile));
        //  upload
        uploadService.uploadPost(photo, accessToken, comment, part, title).enqueue(new Callback<Common>() {
            @Override
            public void onResponse(Call<Common> call, Response<Common> response) {
                if(response.isSuccessful() && response.body().status.equals("OK")) {
                    TimelineData.getInstance(WritePost.this).requestTimeline();
                    finish();
                } else {
                    onFailure(call, new Throwable("Unable to upload post."));
                }
            }

            @Override
            public void onFailure(Call<Common> call, Throwable t) {
                Toast.makeText(WritePost.this, "글을 업로드할 수 없습니다.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    void callGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, REQUEST_CODE_PHOTO);
    }

    @Override
    public void initMenu() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CODE_PHOTO && resultCode == RESULT_OK && data != null) {
            imgUri = data.getData();
            imgFile = new File(Environment.getExternalStorageDirectory(), ".dotattoo_photo_tmp" + new Date().getTime() + ".png");
            UCrop.of(imgUri, Uri.fromFile(imgFile)).start(this);
        }
        if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            imgFile = ImageUtils.saveImage(this, UCrop.getOutput(data));
            photo.setImageURI(UCrop.getOutput(data));
        } else if (resultCode == UCrop.RESULT_ERROR) {
            Toast.makeText(this, "사진을 불러오지 못했습니다.", Toast.LENGTH_SHORT).show();
        }
    }

    void createSelectPartDialog() {
        dialog = new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.select_part))
                .setSingleChoiceItems(R.array.tattoo_part, 0, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        selectPart.setText(getResources().getStringArray(R.array.tattoo_part)[which]);
                    }
                })
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
    }

    void showSelectPartDialog() {
        dialog.show();
    }

}
