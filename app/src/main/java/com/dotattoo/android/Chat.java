package com.dotattoo.android;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dotattoo.android.adapter.ChatAdapter;
import com.dotattoo.android.core.base.BaseActivity;
import com.dotattoo.android.net.req.ChatService;
import com.dotattoo.android.net.res.ChatList;
import com.dotattoo.android.net.res.SendResult;
import com.dotattoo.android.pref.SessionPref;
import com.dotattoo.android.util.ImageUtils;
import com.dotattoo.android.util.Utils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Chat extends BaseActivity implements View.OnClickListener {


    public static final String USER_ID = "user_id";
    public static final String USER_NAME = "user_name";

    private static final int REQUEST_CODE_PHOTO = 3;
    private static final int LAYOUT_RESOURCE = R.layout.activity_chat;
    private static final String TEXT = "text/plane";
    private static final String PHOTO = "image/jpg";

    private EditText inputField;
    private Button more, send;
    private TextView targetName;
    private LinearLayout moreMenu;
    private SimpleDraweeView request, image;

    private RecyclerView chatList;
    private LinearLayoutManager llManager;
    private ChatAdapter adapter;

    private ChatService.Chat chatService;

    private String user_id, userName;

    private Uri imgUri, resultUri;
    private boolean isMenuActive = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setLayoutResourceId(LAYOUT_RESOURCE);
        activeFresco(true);
        super.onCreate(savedInstanceState);
    }

    boolean getTargetUser() {
        user_id = getIntent().getStringExtra(USER_ID);
        userName = getIntent().getStringExtra(USER_NAME);
        return user_id != null;
    }

    /**
     * load chat history and update adapter
     */
    void loadChatHistory() {
        Call<ChatList> chatListCall = chatService.getMessageHistory(user_id, SessionPref.getInstance(this).getAccessToken());
        chatListCall.enqueue(new Callback<ChatList>() {
            @Override
            public void onResponse(Call<ChatList> call, Response<ChatList> response) {
                if(response.isSuccessful() && response.body().status.equals("OK")) {
                    //  update adapter
                    adapter.updateData(response.body().data);
                    chatList.scrollToPosition(adapter.getItemCount() - 1);
                } else {
                    onFailure(call, new Throwable());
                    Toast.makeText(Chat.this,
                            "메시지 불러오기에 실패하였습니다. 네트워크 연결 상태를 확인해 주세요.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ChatList> call, Throwable t) {
                Toast.makeText(Chat.this, "네트워크 연결 상태를 확인해주세요.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void init() {
        if(!getTargetUser()) {
            Toast.makeText(Chat.this, "오류가 발생했습니다. 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
            user_id = "";
        }
        chatService = ChatService.getInstance();
        adapter = new ChatAdapter(SessionPref.getInstance(this).getUserId());
    }

    @Override
    public void initView() {
        //  set title user name
        targetName = (TextView) findViewById(R.id.toolbar_title);
        if(userName != null) {
            targetName.setText(userName);
            targetName.setTypeface(null, Typeface.BOLD);
            targetName.setOnClickListener(this);
        }

        chatList = (RecyclerView) findViewById(R.id.chat_message_list);
        llManager = new LinearLayoutManager(this);
        chatList.setAdapter(adapter);
        chatList.setLayoutManager(llManager);
        send = (Button) findViewById(R.id.chat_input_send);
        inputField = (EditText) findViewById(R.id.chat_input);
        send.setOnClickListener(this);
        more = (Button) findViewById(R.id.chat_more);
        more.setOnClickListener(this);
        moreMenu = (LinearLayout) findViewById(R.id.chat_more_menu);
        request = (SimpleDraweeView) findViewById(R.id.chat_send_request);
        image = (SimpleDraweeView) findViewById(R.id.chat_photo_pick);
        request.setOnClickListener(this);
        image.setOnClickListener(this);

        loadChatHistory();
    }

    void sendTextMessage(ChatList.Chat chat) {
        Call<SendResult> sendTextMessageCall = chatService.sendTextMessage(chat.to,
                SessionPref.getInstance(this).getAccessToken(), chat.message);
        sendTextMessageCall.enqueue(new Callback<SendResult>() {
            @Override
            public void onResponse(Call<SendResult> call, Response<SendResult> response) {
                if(response.isSuccessful() && response.body().status.equals("OK")) {
                    //  update adapter to add sent time.
                    adapter.addSentTime(response.body().data.time, adapter.getItemCount() - 1);
                    chatList.scrollToPosition(adapter.getItemCount() - 1);
                } else {
                    onFailure(call, new Throwable());
                }
            }

            @Override
            public void onFailure(Call<SendResult> call, Throwable t) {

            }
        });
    }

    void sendPhotoMessage(File photo) {
        ChatList.Chat msg = new ChatList.Chat();
        msg.type = "photo";
        msg.from = SessionPref.getInstance(this).getUserId();
        msg.to = user_id;
        adapter.addData(msg, photo);
//        ((ChatAdapter.Chat) chatList.findViewHolderForAdapterPosition(adapter.getItemCount() - 1))
//                .photo.setImageURI(resultUri);
        RequestBody id = RequestBody.create(MediaType.parse(TEXT), user_id);
        RequestBody token = RequestBody.create(MediaType.parse(TEXT),
                SessionPref.getInstance(this).getAccessToken());
        RequestBody file = RequestBody.create(MediaType.parse(PHOTO), photo);
        chatService.sendPhotoMessage(id, token, file)
        .enqueue(new Callback<SendResult>() {
            @Override
            public void onResponse(Call<SendResult> call, Response<SendResult> response) {
                if(response.isSuccessful() && response.body().status.equals("OK")) {
                    //  update adapter to add sent time.
                    adapter.addSentTime(response.body().data.time, adapter.getItemCount() - 1);
                    chatList.scrollToPosition(adapter.getItemCount() - 1);
                } else {
                    onFailure(call, new Throwable());
                }
            }

            @Override
            public void onFailure(Call<SendResult> call, Throwable t) {
                Toast.makeText(Chat.this, "메시지를 보내는 데 문제가 발생했습니다.",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.chat_input_send:
                if(inputField.getText().toString().isEmpty())
                    break;
                ChatList.Chat msg = new ChatList.Chat();
                msg.message = inputField.getText().toString();
                msg.from = SessionPref.getInstance(this).getUserId();
                msg.to = user_id;
                msg.type = ChatAdapter.TEXT_MESSAGE;
                adapter.addData(msg);
                inputField.setText("");
                sendTextMessage(msg);
                break;
            //  show option fragment
            case R.id.chat_more:
                switchMenu();
                break;
            case R.id.chat_send_request:
                Intent request = new Intent(Chat.this, Request.class);
                request.putExtra(Request.USER_ID, user_id);
                startActivity(request);
                break;
            case R.id.chat_photo_pick:
                if(Utils.checkPermission(this)) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, REQUEST_CODE_PHOTO);
                }
                break;
            case R.id.toolbar_title:
                Intent user = new Intent(this, UserPage.class);
                user.putExtra(UserPage.USER_ID, user_id);
                startActivity(user);
                break;
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        loadChatHistory();
    }

    void switchMenu() {
        if(moreMenu.getVisibility() == View.VISIBLE && isMenuActive) {
            moreMenu.setVisibility(View.GONE);
            isMenuActive = false;
        } else {
            moreMenu.setVisibility(View.VISIBLE);
            isMenuActive = true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //  continue process
        if(Utils.REQUEST_PERMISSION_CODE == requestCode && grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, REQUEST_CODE_PHOTO);
        } else {
            Toast.makeText(Chat.this, "앱 실행을 위해서는 저장소 권한을 설정해야 합니다. ",
                    Toast.LENGTH_SHORT).show();
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_CODE_PHOTO && resultCode == RESULT_OK && data != null) {
            imgUri = data.getData();
            DateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date nowDate = new Date();
            resultUri = Uri.fromFile(new File(ImageUtils.getDirPath(), sdFormat.format(nowDate)));
            UCrop.of(imgUri, resultUri).start(this);
        }
        if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            File imageFile = ImageUtils.saveImage(this, UCrop.getOutput(data));
            sendPhotoMessage(imageFile);
        } else if (resultCode == UCrop.RESULT_ERROR) {
            Toast.makeText(this, "사진을 불러오지 못했습니다.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void initMenu() {

    }

    @Override
    public void onBackPressed() {
        if(isMenuActive) {
            switchMenu();
            return;
        }
        super.onBackPressed();
    }
}
