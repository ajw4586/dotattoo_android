package com.dotattoo.android.dialog;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.dotattoo.android.Chat;
import com.dotattoo.android.R;
import com.dotattoo.android.net.req.ChatService;
import com.dotattoo.android.net.res.SendResult;
import com.dotattoo.android.pref.SessionPref;
import com.dotattoo.android.util.Utils;
import com.facebook.drawee.view.SimpleDraweeView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ajw45 on 2016-03-19.
 */
public class ConsultDialog extends DialogFragment implements View.OnClickListener {


    private static final int DIALOG_LAYOUT_RESOURCE = R.layout.dialog_consult;
    private static final String DIALOG_TAB = "consult_dialog";
    private static final String PHOTO_DATA = "photo";

    private View rootView;
    private SimpleDraweeView photo;
    private ImageView cancel, send;
    private EditText comment;

    private String post_id, user_id, user_name;

    @Override
    public void onStart() {
        super.onStart();
        int width = getResources().getDimensionPixelSize(R.dimen.comment_dialog_width);

        WindowManager.LayoutParams size = getDialog().getWindow().getAttributes();
        size.width = width;
        getDialog().getWindow().setAttributes(size);
    }

    public static void showConsultDialog(FragmentManager fm, String user_id, String user_name, String post_id, String photoUrl) {
        ConsultDialog d = new ConsultDialog();
        d.user_id = user_id;
        d.user_name = user_name;
        d.post_id = post_id;
        Bundle data = new Bundle();
        data.putString(PHOTO_DATA, photoUrl);
        d.setArguments(data);
        d.show(fm, DIALOG_TAB);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // remove dialog title
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        rootView = inflater.inflate(DIALOG_LAYOUT_RESOURCE, container, false);
        initView();
        return rootView;
    }

    void initView() {
        cancel = (ImageView) rootView.findViewById(R.id.consult_dialog_cancel);
        send = (ImageView) rootView.findViewById(R.id.consult_dialog_send);
        photo = (SimpleDraweeView) rootView.findViewById(R.id.consult_dialog_image);
        comment = (EditText) rootView.findViewById(R.id.consult_dialog_field);
        comment.setSelection(comment.getText().length());
        cancel.setOnClickListener(this);
        send.setOnClickListener(this);

        Bundle data = getArguments();
        if(data != null) {
            photo.setImageURI(Utils.createImageURI(data.getString(PHOTO_DATA)));
        } else {
            Toast.makeText(getContext(), "문제가 발생했습니다. 다시 시도해주세요.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.consult_dialog_cancel:
                getDialog().dismiss();
                break;
            case R.id.consult_dialog_send:
                consultRequest();
                break;
        }
    }

    void consultRequest() {
        if(user_id != null) {
            final ChatService.Chat chat = ChatService.getInstance();
            chat.sendAppLinkMessage(user_id, SessionPref.getInstance(getContext()).getAccessToken(),
                    post_id).enqueue(new Callback<SendResult>() {
                @Override
                public void onResponse(Call<SendResult> call, Response<SendResult> response) {
                    if (response.isSuccessful() && response.body().status.equals("OK")) {
                        chat.sendTextMessage(user_id,
                                SessionPref.getInstance(getContext()).getAccessToken(),
                                comment.getText().toString()).enqueue(new Callback<SendResult>() {
                            @Override
                            public void onResponse(Call<SendResult> call, Response<SendResult> response) {
                                if(response.isSuccessful() && response.body().status.equals("OK")) {
                                    Intent consult = new Intent(getContext(), Chat.class);
                                    consult.putExtra(Chat.USER_ID, user_id);
                                    consult.putExtra(Chat.USER_NAME, user_name);
                                    getDialog().dismiss();
                                    startActivity(consult);
                                } else {
                                    onFailure(call, new Throwable());
                                }
                            }

                            @Override
                            public void onFailure(Call<SendResult> call, Throwable t) {
                                Toast.makeText(getContext(), "문제가 발생했습니다. 잠시 후에 다시 시도해주세요.",
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        onFailure(call, new Throwable());
                    }
                }

                @Override
                public void onFailure(Call<SendResult> call, Throwable t) {
                    Toast.makeText(getContext(), "문제가 발생했습니다. 잠시 후에 다시 시도해주세요.",
                            Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getContext(), "문제가 발생했습니다. 잠시 후에 다시 시도해주세요.",
                    Toast.LENGTH_SHORT).show();
        }
    }
}
