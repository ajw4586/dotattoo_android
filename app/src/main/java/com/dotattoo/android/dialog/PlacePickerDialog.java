package com.dotattoo.android.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.dotattoo.android.R;
import com.dotattoo.android.adapter.SimpleListAdapter;

/**
 * Created by ajw45 on 2016-04-10.
 */
public class PlacePickerDialog extends DialogFragment {


    private static final int DIALOG_LAYOUT_RESOURCE = R.layout.dialog_place_picker;

    private static final int[] placeLevel2 = {
            R.array.place_2_seoul, R.array.place_2_gyeonggi
    };

    private View rootView;
    private RecyclerView list;

    private OnPlaceSelectedListener listener;

    private SimpleListAdapter[] adapters = new SimpleListAdapter[2];
    private SimpleListAdapter.OnListItemClickListener[] adapterListeners =
            new SimpleListAdapter.OnListItemClickListener[2];
    private int[] selection;

    private int level1 = -1, level2 = -1;

    public interface OnPlaceSelectedListener {
        public void onPlaceSelected(String name, int[] indexs);
    }

    public static PlacePickerDialog newInstance() {
        return new PlacePickerDialog();
    }

    public void show(android.support.v4.app.FragmentManager fm) {
        this.show(fm, "placePickerFragment");
    }

    public void setSelection(int[] selection) {
        this.selection = selection;
    }

    public void setOnPlaceSelectedListener(OnPlaceSelectedListener listener) {
        this.listener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // remove dialog title
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow()
                .setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        rootView = inflater.inflate(DIALOG_LAYOUT_RESOURCE, container, false);
        init();
        initView();
        return rootView;
    }

    void init() {
        //  init place level adapters
        adapterListeners[0] = new SimpleListAdapter.OnListItemClickListener() {
            @Override
            public void onListItemClick(int position, String text) {
                level1 = position;

                //  call level 2 adapter
                if(position < 2) {
                    Log.d("place picker", "onListItemClick: call level 2 adapter");

                    adapters[1] = new SimpleListAdapter(getResources()
                            .getStringArray(placeLevel2[position]));
                    if(selection != null) {
                        //  restore previous state if the pre-selected position
                        // and current selected position are same.
                        if(selection[0] == position) {
                            adapters[1].setSelected(selection[1]);
                        }
                    }
                    adapters[1].setOnListItemClickListener(adapterListeners[1]);
                    list.setAdapter(adapters[1]);
                } else {
                    if(listener != null) {
                        listener.onPlaceSelected(text, new int[]{level1});
                    }
                    dismiss();
                }
            }
        };
        adapterListeners[1] = new SimpleListAdapter.OnListItemClickListener() {
            @Override
                public void onListItemClick(int position, String text) {
                level2 = position;
                if(listener != null) {
                    listener.onPlaceSelected(text, new int[]{level1, level2});
                }
                dismiss();
            }
        };
    }

    void initView() {
        list = (RecyclerView) rootView.findViewById(R.id.place_picker_list);
        list.setLayoutManager(new LinearLayoutManager(getContext()));

        adapters[0] = new SimpleListAdapter(getResources().getStringArray(R.array.place_1));
        adapters[0].setOnListItemClickListener(adapterListeners[0]);

        if(selection != null) {
            adapters[0].setSelected(selection[0]);
        }
        list.setAdapter(adapters[0]);



    }
}
