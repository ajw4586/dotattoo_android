package com.dotattoo.android.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dotattoo.android.R;
import com.dotattoo.android.adapter.CommentAdapter;
import com.dotattoo.android.core.base.BaseActivity;
import com.dotattoo.android.data.TimelineData;
import com.dotattoo.android.net.req.PostService;
import com.dotattoo.android.net.res.CommentList;
import com.dotattoo.android.net.res.Common;
import com.dotattoo.android.pref.SessionPref;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ajw45 on 2016-03-01.
 */
public class CommentDialog extends DialogFragment {


    private static final String DIALOG_TAG = "comment_dialog";
    private static final String POST_ID = "post_id";
    private static final int DIALOG_LAYOUT_RESOURCE = R.layout.dialog_comment;

    private View rootView;

    private RecyclerView commentList;
    private CommentAdapter adapter;
    private LinearLayoutManager llManager;
    private EditText commentField;
    private Button submit;

    private PostService.Post postService;

    private String postId;

    public static void showCommentDialog(FragmentManager fm, String postId) {
        CommentDialog d = new CommentDialog();
        Bundle data = new Bundle();
        data.putString(POST_ID, postId);
        d.setArguments(data);
        d.show(fm, DIALOG_TAG);
    }

    @Override
    public void onStart() {
        super.onStart();
        int width = getResources().getDimensionPixelSize(R.dimen.comment_dialog_width);
        int height = getResources().getDimensionPixelSize(R.dimen.comment_dialog_height);
        WindowManager.LayoutParams size = getDialog().getWindow().getAttributes();
        size.width = width;
        size.height = height;
        getDialog().getWindow().setAttributes(size);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // remove dialog title
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        rootView = inflater.inflate(DIALOG_LAYOUT_RESOURCE, container, false);
        init();
        initView();
        return rootView;
    }

    void init() {
        //  get args
        Bundle extra = getArguments();
        postId = extra.getString(POST_ID);

        postService = PostService.getInstance();
        llManager = new LinearLayoutManager(getContext());
        adapter = new CommentAdapter(getContext());
    }

    void initView() {
        commentList = (RecyclerView) rootView.findViewById(R.id.comment_list);
        commentList.setLayoutManager(llManager);
        commentList.setAdapter(adapter);

        commentField = (EditText) rootView.findViewById(R.id.comment_field);
        submit = (Button) rootView.findViewById(R.id.comment_submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendComment();
            }
        });

        //  after sync resources, perform load comment
        if(postId != null)
            loadComment();
    }

    void loadComment() {
        postService.getPostComment(postId)
                .enqueue(new Callback<CommentList>() {
                    @Override
                    public void onResponse(Call<CommentList> call, Response<CommentList> response) {
                        if(response.isSuccessful()) {
                            //  update adapter
                            adapter.updateData(response.body().data.reaction.comment);
                            commentList.scrollToPosition(adapter.getItemCount() - 1);
                            TimelineData.getInstance((BaseActivity) getActivity()).requestTimeline();
                        } else {
                            onFailure(call, new Throwable());
                        }
                    }

                    @Override
                    public void onFailure(Call<CommentList> call, Throwable t) {
                        Toast.makeText(getContext(), "댓글을 불러올 수 없습니다.", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    void sendComment() {
        postService.comment(postId, SessionPref.getInstance(getContext()).getAccessToken(),
                commentField.getText().toString())
        .enqueue(new Callback<Common>() {
            @Override
            public void onResponse(Call<Common> call, Response<Common> response) {
                if(response.isSuccessful() && response.body().status.equals("OK")) {
                    loadComment();
                } else {
                    onFailure(call, new Throwable());
                }
            }

            @Override
            public void onFailure(Call<Common> call, Throwable t) {
                Toast.makeText(getContext(), "댓글을 게시할 수 없습니다.", Toast.LENGTH_SHORT).show();
            }
        });
        commentField.setText("");
    }
}
