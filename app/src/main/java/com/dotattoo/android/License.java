package com.dotattoo.android;

import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;

import com.dotattoo.android.core.base.BaseActivity;

public class License extends BaseActivity {


    private static final int LAYOUT_RESOURCE = R.layout.activity_license;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setLayoutResourceId(LAYOUT_RESOURCE);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void init() {

    }

    @Override
    public void initView() {
        TextView title = (TextView) findViewById(R.id.toolbar_title);
        title.setText("이용약관");
        title.setTextSize(20);
        title.setTypeface(null, Typeface.BOLD);
    }
}
