package com.dotattoo.android.tab.search;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;

import com.dotattoo.android.R;
import com.dotattoo.android.core.base.BaseActivity;
import com.dotattoo.android.net.req.SearchService;
import com.dotattoo.android.net.res.Search;
import com.dotattoo.android.pref.SessionPref;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchResult extends BaseActivity {


    public static final String SEARCH_KEY = "search_key";

    private static final int LAYOUT_RESOURCE = R.layout.activity_search_result;
    private static final int RESULT_CONTAINER = R.id.search_result_container;
    private static final int SEARCH_TYPE_POST = 1;
    private static final int SEARCH_TYPE_MEMBER = 2;

    private TabLayout searchTypeTab;

    private PostSearchResultFragment postResult;
    private MemberSearchResultFragment memberResult;

    private SearchService.Search searchService;

    private String searchKey;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setLayoutResourceId(LAYOUT_RESOURCE);
        super.onCreate(savedInstanceState);
    }

    void getSearchInfo() {
        searchKey = getIntent().getStringExtra(SEARCH_KEY);
    }

    @Override
    public void init() {
        getSearchInfo();

        searchService = SearchService.getInstance();
        postResult = new PostSearchResultFragment();
        memberResult = new MemberSearchResultFragment();
        getSupportFragmentManager().beginTransaction()
                .add(RESULT_CONTAINER, postResult)
                .commit();
    }

    public void search(int searchType) {
        if(searchKey == null) {
            Log.d(getClass().getSimpleName(), "search: No search key");
            return;
        }
        switch (searchType) {
            case SEARCH_TYPE_MEMBER:
                searchService.searchMember(searchKey, SessionPref.getInstance(this).getAccessToken())
                        .enqueue(new Callback<Search.MemberSearchResult>() {
                            @Override
                            public void onResponse(Call<Search.MemberSearchResult> call,
                                                   Response<Search.MemberSearchResult> response) {
                                if(response.isSuccessful() && response.body().status.equals("OK")) {
                                    getSupportFragmentManager().beginTransaction()
                                            .replace(RESULT_CONTAINER, memberResult)
                                            .commit();
                                    memberResult.updateSearchResultData(response.body().data);
                                } else {
                                    onFailure(call, new Throwable());
                                }
                            }

                            @Override
                            public void onFailure(Call<Search.MemberSearchResult> call, Throwable t) {
                                Toast.makeText(SearchResult.this, "검색을 할 수 없습니다.",
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
                break;
            case SEARCH_TYPE_POST:
                searchService.searchPost(searchKey, SessionPref.getInstance(this).getAccessToken())
                        .enqueue(new Callback<Search.PostSearchResult>() {
                            @Override
                            public void onResponse(Call<Search.PostSearchResult> call,
                                                   Response<Search.PostSearchResult> response) {
                                if(response.isSuccessful() && response.body().status.equals("OK")) {
                                    getSupportFragmentManager().beginTransaction()
                                            .replace(RESULT_CONTAINER, postResult)
                                            .commit();
                                    postResult.updateSearchResultData(response.body().data);
                                } else {
                                    onFailure(call, new Throwable());
                                }
                            }

                            @Override
                            public void onFailure(Call<Search.PostSearchResult> call, Throwable t) {
                                Toast.makeText(SearchResult.this, "검색을 할 수 없습니다.",
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
                break;
        }
    }

    @Override
    public void initView() {
        search(SEARCH_TYPE_POST);
    }

    @Override
    public void initMenu() {
        searchTypeTab = (TabLayout) findViewById(R.id.search_result_type_tab);

        //  get string resources from xml file
        String[] labels = getResources().getStringArray(R.array.tab_search);
        TabLayout.Tab[] searchType = new TabLayout.Tab[2];
        for(int i = 0; i < 2; i++) {
            searchType[i] = searchTypeTab.newTab();
            searchType[i].setText(labels[i]);
            searchTypeTab.addTab(searchType[i]);
        }
    }
}
