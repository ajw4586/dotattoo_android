package com.dotattoo.android.tab.mypage;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dotattoo.android.R;
import com.dotattoo.android.core.base.BaseFragment;
import com.dotattoo.android.data.DataUpdateCallback;
import com.dotattoo.android.data.LikedPostData;
import com.dotattoo.android.util.GridMarginDecoration;

import app.dinus.com.loadingdrawable.LoadingDrawable;
import app.dinus.com.loadingdrawable.render.circle.jump.LightGuardLoadingRenderer;

/**
 * A simple {@link Fragment} subclass.
 */
public class LikedPostFragment extends BaseFragment implements DataUpdateCallback {


    private static final int LAYOUT_RESOURCE = R.layout.fragment_liked_post;

    private RecyclerView list;
    private GridLayoutManager glManager;

    private ImageView loadingIndicator;
    private LightGuardLoadingRenderer loadingIndicatorAnim;
    private ImageView emptyView;

    private LikedPostData likedPostData;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d("likedpost", "onAttach: initialize start");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setLayoutResourceId(LAYOUT_RESOURCE);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void init() {
        likedPostData = LikedPostData.getInstance(getContext());
        likedPostData.setCallback(this);
        glManager = new GridLayoutManager(getContext(), 4);

        loadingIndicatorAnim = new LightGuardLoadingRenderer(getContext());
    }

    @Override
    public void onResume() {
        super.onResume();
        updateList();
    }

    @Override
    public void initView() {
        list = (RecyclerView) findViewById(R.id.liked_post_list);
        list.setLayoutManager(glManager);
        list.addItemDecoration(new GridMarginDecoration(3, false));
        list.setAdapter(likedPostData.getAdapter());

        loadingIndicator = (ImageView) findViewById(R.id.liked_post_loading_indicator);
        loadingIndicator.setImageDrawable(new LoadingDrawable(loadingIndicatorAnim));
        loadingIndicatorAnim.start();

        emptyView = (ImageView) findViewById(R.id.empty_view);
    }

    void updateList() {
        likedPostData.updateData();
    }

    /**
     * Handle likedpost data update
     */
    @Override
    public void onUpdated() {
        loadingIndicatorAnim.stop();
        loadingIndicator.setVisibility(View.INVISIBLE);
        emptyView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onFailed() {
        loadingIndicatorAnim.stop();
        loadingIndicator.setVisibility(View.INVISIBLE);
        emptyView.setVisibility(View.VISIBLE);
    }
}
