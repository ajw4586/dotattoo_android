package com.dotattoo.android.tab.home;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dotattoo.android.R;
import com.dotattoo.android.adapter.viewpager.MypageSectionPageAdapter;
import com.dotattoo.android.core.base.BaseFragment;
import com.dotattoo.android.net.req.MemberService;
import com.dotattoo.android.net.res.Profile;
import com.dotattoo.android.pref.AccountPref;
import com.dotattoo.android.util.ImageUtils;
import com.dotattoo.android.util.Utils;
import com.facebook.drawee.view.SimpleDraweeView;


/**
 * Created by ajw45 on 2016-01-21.
 */
public class MypageFragment extends BaseFragment {


    private static final int LAYOUT_RESOURCE = R.layout.fragment_mypage;

    private ViewPager section;
    private MypageSectionPageAdapter sectionAdapter;
    private SimpleDraweeView profileImg, profileBg;
    private TextView userName;

    private TabLayout sectionTab_container;

    private MemberService.Member member;

    private int[] tabsMenuIds = {
            R.drawable.icon_liked,
            R.drawable.icon_reservation,
            R.drawable.icon_tattoo_request,
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setLayoutResourceId(LAYOUT_RESOURCE);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void init() {
        sectionAdapter = new MypageSectionPageAdapter(getFragmentManager(), getContext());
        member = MemberService.getInstance();
    }

    @Override
    public void initView() {
        userName = (TextView) findViewById(R.id.mypage_user_name);
        profileImg = (SimpleDraweeView) findViewById(R.id.mypage_user_img);
        profileBg = (SimpleDraweeView) findViewById(R.id.mypage_profile_image_bg);
        profileBg.getHierarchy().setActualImageColorFilter(new PorterDuffColorFilter(0x66000000, PorterDuff.Mode.DARKEN));

        section = (ViewPager) findViewById(R.id.mypage_section);
        section.setAdapter(sectionAdapter);
    }

    void getUserInfo() {
        Profile info = AccountPref.getInstance(getContext()).getUserInfo();
        if(!info.name.isEmpty()) {
            userName.setText(info.name);
            profileImg.setImageURI(Utils.createImageURI(info.profile_img));
            profileBg.setController(ImageUtils.requestBlurImage(getContext(), info.profile_img,
                    profileBg.getWidth(), profileBg.getHeight(), profileBg));
        }
    }

    @Override
    public void initMenu() {
        sectionTab_container = (TabLayout) findViewById(R.id.mypage_section_tab);
        final View[] tabsMenu = new View[3];
        LayoutInflater inflater = LayoutInflater.from(getContext());
        //  Add tabs
        sectionTab_container.setupWithViewPager(section);
        String[] tabsMenuLabels = getResources().getStringArray(R.array.mypage_tab_name);
        for(int i = 0; i < 3; i++) {
            tabsMenu[i] = inflater.inflate(R.layout.mypage_tab, null);
            ((ImageView) tabsMenu[i].findViewById(R.id.mypage_tab_icon)).setImageResource(tabsMenuIds[i]);
            ((TextView) tabsMenu[i].findViewById(R.id.mypage_tab_label)).setText(tabsMenuLabels[i]);
            if(i == 0) {
                tabsMenu[i].findViewById(R.id.mypage_tab_icon).setAlpha(1f);
                ((TextView) tabsMenu[i].findViewById(R.id.mypage_tab_label))
                        .setTextColor(Color.BLACK);
                ((TextView) tabsMenu[i].findViewById(R.id.mypage_tab_count))
                        .setTextColor(Color.BLACK);
            }
            sectionTab_container.getTabAt(i).setCustomView(tabsMenu[i]);
        }
        sectionTab_container.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(section) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);
                switch (tab.getPosition()) {
                    case 0:
                        //  image alpha set
                        tabsMenu[0].findViewById(R.id.mypage_tab_icon).setAlpha(1f);
                        tabsMenu[1].findViewById(R.id.mypage_tab_icon).setAlpha(0.3f);
                        tabsMenu[2].findViewById(R.id.mypage_tab_icon).setAlpha(0.3f);

                        ((TextView) tabsMenu[0].findViewById(R.id.mypage_tab_label))
                                .setTextColor(Color.BLACK);
                        ((TextView) tabsMenu[1].findViewById(R.id.mypage_tab_label))
                                .setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                        ((TextView) tabsMenu[2].findViewById(R.id.mypage_tab_label))
                                .setTextColor(getResources().getColor(R.color.colorPrimaryDark));

                        ((TextView) tabsMenu[0].findViewById(R.id.mypage_tab_count))
                                .setTextColor(Color.BLACK);
                        ((TextView) tabsMenu[1].findViewById(R.id.mypage_tab_count))
                                .setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                        ((TextView) tabsMenu[2].findViewById(R.id.mypage_tab_count))
                                .setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                        break;
                    case 1:
                        //  image alpha set
                        tabsMenu[1].findViewById(R.id.mypage_tab_icon).setAlpha(1f);
                        tabsMenu[0].findViewById(R.id.mypage_tab_icon).setAlpha(0.3f);
                        tabsMenu[2].findViewById(R.id.mypage_tab_icon).setAlpha(0.3f);

                        ((TextView) tabsMenu[1].findViewById(R.id.mypage_tab_label))
                                .setTextColor(Color.BLACK);
                        ((TextView) tabsMenu[0].findViewById(R.id.mypage_tab_label))
                                .setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                        ((TextView) tabsMenu[2].findViewById(R.id.mypage_tab_label))
                                .setTextColor(getResources().getColor(R.color.colorPrimaryDark));

                        ((TextView) tabsMenu[1].findViewById(R.id.mypage_tab_count))
                                .setTextColor(Color.BLACK);
                        ((TextView) tabsMenu[0].findViewById(R.id.mypage_tab_count))
                                .setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                        ((TextView) tabsMenu[2].findViewById(R.id.mypage_tab_count))
                                .setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                        break;
                    case 2:
                        //  image alpha set
                        tabsMenu[2].findViewById(R.id.mypage_tab_icon).setAlpha(1f);
                        tabsMenu[0].findViewById(R.id.mypage_tab_icon).setAlpha(0.3f);
                        tabsMenu[1].findViewById(R.id.mypage_tab_icon).setAlpha(0.3f);

                        ((TextView) tabsMenu[2].findViewById(R.id.mypage_tab_label))
                                .setTextColor(Color.BLACK);
                        ((TextView) tabsMenu[0].findViewById(R.id.mypage_tab_label))
                                .setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                        ((TextView) tabsMenu[1].findViewById(R.id.mypage_tab_label))
                                .setTextColor(getResources().getColor(R.color.colorPrimaryDark));

                        ((TextView) tabsMenu[2].findViewById(R.id.mypage_tab_count))
                                .setTextColor(Color.BLACK);
                        ((TextView) tabsMenu[0].findViewById(R.id.mypage_tab_count))
                                .setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                        ((TextView) tabsMenu[1].findViewById(R.id.mypage_tab_count))
                                .setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                super.onTabUnselected(tab);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                super.onTabReselected(tab);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getUserInfo();
        try {
            Log.d("mypage", "onResume: main tab selected");
            sectionTab_container.getTabAt(0).select();
        } catch (NullPointerException e) {
            e.printStackTrace();
            Log.d("mypage", "mypage nullpointer exception");

        }
    }
}
