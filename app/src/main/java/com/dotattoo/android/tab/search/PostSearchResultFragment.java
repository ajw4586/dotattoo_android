package com.dotattoo.android.tab.search;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dotattoo.android.R;
import com.dotattoo.android.adapter.SearchResultAdapter;
import com.dotattoo.android.core.base.BaseFragment;
import com.dotattoo.android.net.res.Search;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostSearchResultFragment extends BaseFragment {


    private static final int LAYOUT_RESOURCE = R.layout.fragment_search_result;

    private RecyclerView resultList;
    private LinearLayoutManager llManager;
    private SearchResultAdapter adapter;

    public PostSearchResultFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setLayoutResourceId(LAYOUT_RESOURCE);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void init() {
        adapter = new SearchResultAdapter(SearchResultAdapter.POST_RESULT);
        llManager = new LinearLayoutManager(getContext());
    }

    @Override
    public void initView() {
        resultList = (RecyclerView) findViewById(R.id.search_result_list);
        resultList.setAdapter(adapter);
        resultList.setLayoutManager(llManager);
    }

    public void updateSearchResultData(List<Search.SearchPost> data) {
        adapter.updateData(data);
    }

}
