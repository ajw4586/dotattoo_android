package com.dotattoo.android.tab.mypage;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dotattoo.android.R;
import com.dotattoo.android.adapter.TattooReservationAdapter;
import com.dotattoo.android.core.base.BaseFragment;
import com.dotattoo.android.data.DataUpdateCallback;
import com.dotattoo.android.data.TattooReservationData;

/**
 * A simple {@link Fragment} subclass.
 */
public class TattooReservationFragment extends BaseFragment implements DataUpdateCallback {


    private static final int LAYOUT_RESOURCE = R.layout.fragment_tattoo_reservation;

    private RecyclerView list;
    private TattooReservationAdapter adapter;
    private LinearLayoutManager llManager;

    private TattooReservationData reservationData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setLayoutResourceId(LAYOUT_RESOURCE);
        // Inflate the layout for this fragment
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateList();
    }

    @Override
    public void init() {
        reservationData = TattooReservationData.getInstance(getContext());
        reservationData.setCallback(this);
        adapter = reservationData.getAdapter();
        llManager = new LinearLayoutManager(getContext());
    }

    @Override
    public void initView() {
        list = (RecyclerView) findViewById(R.id.reservation_list);
        list.setLayoutManager(llManager);
        list.setAdapter(adapter);
    }

    void updateList() {
        reservationData.updateData();
    }

    @Override
    public void onUpdated() {
    }

    @Override
    public void onFailed() {

    }
}
