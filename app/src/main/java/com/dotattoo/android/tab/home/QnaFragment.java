package com.dotattoo.android.tab.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.dotattoo.android.Chat;
import com.dotattoo.android.R;
import com.dotattoo.android.adapter.ChatRoomAdapter;
import com.dotattoo.android.core.base.BaseFragment;
import com.dotattoo.android.data.DataUpdateCallback;
import com.dotattoo.android.net.req.ChatService;
import com.dotattoo.android.net.res.ChatRoom;
import com.dotattoo.android.pref.SessionPref;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ajw45 on 2016-01-21.
 */
public class QnaFragment extends BaseFragment implements View.OnClickListener, DataUpdateCallback {


    private static final int LAYOUT_RESOURCE = R.layout.fragment_chat;
    public static final String ADMIN_ID = "56c489aa7629c0002fcdae1b";


    private RelativeLayout chatDirect;
    private RecyclerView chatList;
    private LinearLayoutManager llManager;
    private ChatRoomAdapter roomAdapter;

    private ImageView emptyView;

    private ChatService.Chat chatService;
    private DataUpdateCallback updateCallback;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        setLayoutResourceId(LAYOUT_RESOURCE);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void init() {
        chatService = ChatService.getInstance();
        llManager = new LinearLayoutManager(getContext());
        roomAdapter = new ChatRoomAdapter(getContext());
        updateCallback = this;
    }

    @Override
    public void onResume() {
        super.onResume();
        getChatroom();
    }

    void getChatroom() {
        //  get the chat list
        Call<ChatRoom> chatRoomCall = chatService.getChatRoom(SessionPref.getInstance(getContext()).getAccessToken());
        chatRoomCall.enqueue(new Callback<ChatRoom>() {
            @Override
            public void onResponse(Call<ChatRoom> call, Response<ChatRoom> response) {
                if(response.isSuccessful() && response.body().status.equals("OK")) {
                    //  update adapter
                    roomAdapter.updateData(response.body().data);
                    if(updateCallback != null) {
                        updateCallback.onUpdated();
                    }
                } else {
                    onFailure(call, new Throwable());
                }
            }

            @Override
            public void onFailure(Call<ChatRoom> call, Throwable t) {
                if(updateCallback != null) {
                    updateCallback.onFailed();
                }
            }
        });
    }

    @Override
    public void initView() {
        chatList = (RecyclerView) findViewById(R.id.chat_room_list);
        chatList.setLayoutManager(llManager);
        chatList.setAdapter(roomAdapter);
        chatDirect = (RelativeLayout) findViewById(R.id.chat_direct);
        chatDirect.setOnClickListener(this);
        emptyView = (ImageView) findViewById(R.id.empty_view);
    }

    @Override
    public void onUpdated() {
        emptyView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onFailed() {
        emptyView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.chat_direct:
                Intent chat = new Intent(getActivity(), Chat.class);
                chat.putExtra(Chat.USER_ID, ADMIN_ID);
                startActivity(chat);
                break;
        }
    }
}
