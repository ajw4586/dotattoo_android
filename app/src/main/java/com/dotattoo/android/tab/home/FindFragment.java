package com.dotattoo.android.tab.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.dotattoo.android.R;
import com.dotattoo.android.adapter.TopUserAdapter;
import com.dotattoo.android.core.base.BaseFragment;
import com.dotattoo.android.data.DataUpdateCallback;
import com.dotattoo.android.net.req.MemberService;
import com.dotattoo.android.net.res.TopUser;
import com.dotattoo.android.tab.search.SearchResult;
import com.dotattoo.android.util.LinearMarginDecoration;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ajw45 on 2016-01-21.
 */
public class FindFragment extends BaseFragment implements View.OnClickListener, DataUpdateCallback {


    private static final int LAYOUT_RESOURCE = R.layout.fragment_find;
    private static final String SEARCH_RESULT_FRAGMENT = "search_result";

    private EditText find;
    private Button execute;

    private RecyclerView topUserList;
    private LinearLayoutManager llManager;
    private TopUserAdapter topUserAdapter;
    private LinearMarginDecoration divider;
    private ImageView emptyView;

    private MemberService.Member member;
    private DataUpdateCallback updateCallback;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setLayoutResourceId(LAYOUT_RESOURCE);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void init() {
        topUserAdapter = new TopUserAdapter(getContext());
        llManager = new LinearLayoutManager(getContext());
        member = MemberService.getInstance();
        divider = new LinearMarginDecoration(15);
        updateCallback = this;

        member.getTopUser()
                .enqueue(new Callback<TopUser>() {
                    @Override
                    public void onResponse(Call<TopUser> call, Response<TopUser> response) {
                        if(response.isSuccessful() && response.body().status.equals("OK")) {
                            topUserAdapter.updateData(response.body());
                            if(updateCallback != null) {
                                updateCallback.onUpdated();
                            }
                        } else {
                            onFailure(call, new Throwable("Failed to get top users."));
                        }
                    }

                    @Override
                    public void onFailure(Call<TopUser> call, Throwable t) {
                        if(updateCallback != null) {
                            updateCallback.onFailed();
                        }
                    }
                });
    }

    @Override
    public void initView() {
        find = (EditText) findViewById(R.id.find_field);
        execute = (Button) findViewById(R.id.find_button);
        execute.setOnClickListener(this);
        topUserList = (RecyclerView) findViewById(R.id.find_top_user_list);
        topUserList.setAdapter(topUserAdapter);
        topUserList.setLayoutManager(llManager);
        topUserList.addItemDecoration(divider);

        emptyView = (ImageView) findViewById(R.id.empty_view);
    }

    @Override
    public void onUpdated() {
        emptyView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onFailed() {
        emptyView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.find_button:
                //  Handle to do not search for blank query.
                if(find.getText().toString().equals("")) {
                    break;
                }
                //  intent result page and continue search
                Intent search = new Intent(getActivity(), SearchResult.class);
                search.putExtra(SearchResult.SEARCH_KEY, find.getText().toString());
                startActivity(search);
                break;
        }
    }
}
