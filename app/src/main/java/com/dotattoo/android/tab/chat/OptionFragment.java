package com.dotattoo.android.tab.chat;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dotattoo.android.R;
import com.dotattoo.android.core.base.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class OptionFragment extends BaseFragment implements View.OnClickListener {


    private static final int LAYOUT_RESOURCE = R.layout.fragment_option;

    public OptionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setLayoutResourceId(LAYOUT_RESOURCE);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void init() {
    }

    @Override
    public void initView() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            //  send photo (move to gallery)
            //case R.id.option_photo:
            //    break;
            ////  send bill (Only for tattooist)
            //case R.id.option_bill:
            //    break;
        }
    }
}
