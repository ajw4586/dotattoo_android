package com.dotattoo.android.tab.home;


import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bartoszlipinski.recyclerviewheader2.RecyclerViewHeader;
import com.dotattoo.android.Chat;
import com.dotattoo.android.Home;
import com.dotattoo.android.R;
import com.dotattoo.android.WritePost;
import com.dotattoo.android.adapter.TimelineAdapter;
import com.dotattoo.android.adapter.viewpager.BannerPageAdapter;
import com.dotattoo.android.core.base.BaseFragment;
import com.dotattoo.android.data.DataUpdateCallback;
import com.dotattoo.android.data.TimelineData;
import com.dotattoo.android.net.req.AdService;
import com.dotattoo.android.net.res.Banners;
import com.dotattoo.android.pref.AccountPref;
import com.dotattoo.android.pref.SessionPref;
import com.dotattoo.android.util.FABAnimationBehavior;
import com.dotattoo.android.util.LinearMarginDecoration;
import com.dotattoo.android.util.OnBackKeyPressedListener;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;

import app.dinus.com.loadingdrawable.LoadingDrawable;
import app.dinus.com.loadingdrawable.render.circle.jump.GuardLoadingRenderer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class TimelineFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener,
        View.OnClickListener, OnBackKeyPressedListener, DataUpdateCallback {


    private static final int LAYOUT_RESOURCE = R.layout.fragment_timeline;

    private RecyclerView timeline;
    private SwipeRefreshLayout timelineRefresher;
    private ImageView loadingIndicator;
    private GuardLoadingRenderer loadingIndicatorAnim;

    private LinearLayoutManager llManager;
    private TimelineAdapter adapter;

    private TimelineData timelineData;
    private AdService.Ad adService;

    private FloatingActionButton menu;
    private FABAnimationBehavior menuBehavior;
    private Animation fabOpenAnim, fabCloseAnim, fabActiveAnim, fabDeactiveAnim;
    private boolean isFABActive = false;

    private LinearLayout additionalMenu;
    private TransitionDrawable menuOverlay;
    private ImageView writePost, question;
    private TextView writePostLabel, questionLabel;

    private ImageView emptyView;

    private RecyclerViewHeader bannerHolder;
    private ViewPager banner;
    private CirclePageIndicator bannerIndicator;
    private BannerPageAdapter bannerAdapter;

    private ArrayList<OnMenuFABStateChangeListener> onMenuFABStateChangeListener;

    private boolean needShowFAB = false;

    public TimelineFragment() {
        // Required empty public constructor
        onMenuFABStateChangeListener = new ArrayList<>();
    }


    public interface OnMenuFABStateChangeListener {
        public void onMenuFABStateChanged(boolean isActive);
    }

    public void addOnMenuFABStateChangeListener(OnMenuFABStateChangeListener listener) {
        this.onMenuFABStateChangeListener.add(listener);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ((Home) context).setOnKeyBackPressedListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setLayoutResourceId(LAYOUT_RESOURCE);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onStop() {
        super.onStop();
        if(isFABActive)
            animateFAB();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        onMenuFABStateChangeListener.clear();
        try {
            timelineData.removeCallback();
        } catch (NullPointerException e) {}
    }

    @Override
    public void init() {
        llManager = new LinearLayoutManager(getContext());

        timelineData = TimelineData.getInstance(this);
        timelineData.requestTimeline();
        timelineData.setCallback(this);
        adapter = timelineData.getTimelineAdapter();
        bannerAdapter = new BannerPageAdapter(getContext());
        adService = AdService.getInstance();

        //  load animations
        fabOpenAnim = AnimationUtils.loadAnimation(getContext(), R.anim.fab_open);
        fabCloseAnim = AnimationUtils.loadAnimation(getContext(), R.anim.fab_close);
        fabActiveAnim = AnimationUtils.loadAnimation(getContext(), R.anim.fab_rotate_forward);
        fabDeactiveAnim = AnimationUtils.loadAnimation(getContext(), R.anim.fab_rotate_backward);
        menuOverlay = (TransitionDrawable) getResources().getDrawable(R.drawable.dim_transition);

        loadingIndicatorAnim = new GuardLoadingRenderer(getContext());
    }

    @Override
    public void initView() {
        timelineRefresher = (SwipeRefreshLayout) findViewById(R.id.timeline_refresher);
        timelineRefresher.setOnRefreshListener(this);
        timelineRefresher.setNestedScrollingEnabled(true);

        loadingIndicator = (ImageView) findViewById(R.id.timeline_loading_indicator);
        loadingIndicator.setImageDrawable(new LoadingDrawable(loadingIndicatorAnim));
        //  start animating
        loadingIndicatorAnim.start();

        timeline = (RecyclerView) findViewById(R.id.timeline);
        timeline.setAdapter(adapter);
        timeline.setLayoutManager(llManager);
        timeline.addItemDecoration(new LinearMarginDecoration(15));
        //  Enable to swipe refresh.
        timeline.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int topRowVerticalPosition =
                        (recyclerView == null || recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();
                timelineRefresher.setEnabled(topRowVerticalPosition >= 0);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        emptyView = (ImageView) findViewById(R.id.empty_view);

        bannerHolder = (RecyclerViewHeader) findViewById(R.id.home_banner_holder);
        bannerHolder.attachTo(timeline);
        banner = (ViewPager) findViewById(R.id.home_banner);
        banner.setAdapter(bannerAdapter);
        bannerIndicator = (CirclePageIndicator) findViewById(R.id.home_banner_indicator);
        bannerIndicator.setCentered(true);
        bannerIndicator.setViewPager(banner);

        menu = (FloatingActionButton) findViewById(R.id.show_menu);
        menu.setBackgroundTintList(new ColorStateList(new int[][]{new int[]{0}}, new int[]{Color.BLACK}));
        menu.startAnimation(fabOpenAnim);
        menu.setOnClickListener(this);
        //  Add FAB behavior
        CoordinatorLayout.LayoutParams menuLayoutParams = (CoordinatorLayout.LayoutParams) menu.getLayoutParams();
        menuBehavior = new FABAnimationBehavior(needShowFAB);
        menuLayoutParams.setBehavior(menuBehavior);

        //  Additional menu init
        additionalMenu = (LinearLayout) findViewById(R.id.home_additional_menu);
        menuOverlay = (TransitionDrawable) additionalMenu.getBackground();
        writePost = (ImageView) findViewById(R.id.home_write_post);
        writePost.setOnClickListener(this);
        question = (ImageView) findViewById(R.id.home_question);
        question.setOnClickListener(this);
        writePostLabel = (TextView) findViewById(R.id.home_write_post_label);
        questionLabel = (TextView) findViewById(R.id.home_question_label);

        adService.getBanners(SessionPref.getInstance(getContext()).getAccessToken())
                .enqueue(new Callback<Banners>() {
                    @Override
                    public void onResponse(Call<Banners> call, Response<Banners> response) {
                        if(response.isSuccessful() && response.body().status.equals("OK")) {
                            bannerAdapter.updateData(response.body().data);
                        } else {
                            onFailure(call, new Throwable());
                        }
                    }

                    @Override
                    public void onFailure(Call<Banners> call, Throwable t) {

                    }
                });
    }

    @Override
    public void onUpdated() {
        loadingIndicatorAnim.stop();
        loadingIndicator.setVisibility(View.INVISIBLE);
        timelineRefresher.setRefreshing(false);
        emptyView.setVisibility(View.INVISIBLE);
        Log.d("Home", "onUpdated: User Type: " + AccountPref.getInstance(getContext()).getUserInfo().type);
        if(AccountPref.getInstance(getContext()).getUserInfo().type.equals(AccountPref.TATTOOIST)) {
            needShowFAB = true;
            menu.setVisibility(View.VISIBLE);
            menuBehavior.updateFABShowState(true);
        }
    }

    @Override
    public void onFailed() {
        timelineRefresher.setRefreshing(false);
        loadingIndicatorAnim.stop();
        loadingIndicator.setVisibility(View.INVISIBLE);
        emptyView.setVisibility(View.VISIBLE);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.show_menu:
                //  animate fab proper status
                animateFAB();
                break;
            case R.id.home_write_post:
                startActivity(new Intent(getActivity(), WritePost.class));
                break;
            case R.id.home_question:
                Intent qna = new Intent(getActivity(), Chat.class);
                qna.putExtra(Chat.USER_ID, QnaFragment.ADMIN_ID);
                startActivity(qna);
                break;
        }
    }

    @Override
    public void onRefresh() {
        Log.d("SwipeRefresher", "Refresh requested.");
        timelineData.requestTimeline();
    }

    /**
     * Animate the fab when fab is clicked.
     */
    void animateFAB() {
        if(!needShowFAB) {
            return;
        }
        //  notify change for listener
        if(!onMenuFABStateChangeListener.isEmpty()) {
            for(OnMenuFABStateChangeListener listener : onMenuFABStateChangeListener) {
                listener.onMenuFABStateChanged(!isFABActive);
            }
        }
        //  Deactive it!
        if(isFABActive) {
            //  disable recyclerview scrolling
            //llManager.setScrollMode(isFABActive);

            //  animation
            menu.startAnimation(fabDeactiveAnim);
            menuOverlay.reverseTransition(500);
            writePost.startAnimation(fabCloseAnim);
            writePostLabel.startAnimation(fabCloseAnim);
            question.startAnimation(fabCloseAnim);
            questionLabel.startAnimation(fabCloseAnim);
            //  make overlay button clickable
            writePost.setClickable(false);
            question.setClickable(false);
            fabCloseAnim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    if(additionalMenu.getVisibility() == View.VISIBLE)
                        additionalMenu.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        } else {
            //  Active!
            if(additionalMenu.getVisibility() == View.INVISIBLE)
                additionalMenu.setVisibility(View.VISIBLE);
            //  enable recyclerview scrolling
            //timelineRefresher.setOnTouchListener(null);
            //llManager.setScrollMode(isFABActive);
            //  animation
            menu.startAnimation(fabActiveAnim);
            menuOverlay.startTransition(500);
            writePost.startAnimation(fabOpenAnim);
            writePostLabel.startAnimation(fabOpenAnim);
            question.startAnimation(fabOpenAnim);
            questionLabel.startAnimation(fabOpenAnim);
            //  set overlay button's click state
            writePost.setClickable(true);
            question.setClickable(true);
        }
        isFABActive = !isFABActive;
    }

    public void scrollToTop() {
        if(timeline != null) {
            timeline.smoothScrollToPosition(0);
        }
    }

    @Override
    public void onBackKeyPressed() {
        if(isFABActive) {
            animateFAB();
        } else {
            ((Home) getActivity()).normalBackPress();
        }
    }
}
