package com.dotattoo.android.tab.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dotattoo.android.R;
import com.dotattoo.android.adapter.AlarmAdapter;
import com.dotattoo.android.core.base.BaseFragment;
import com.dotattoo.android.data.DataUpdateCallback;
import com.dotattoo.android.data.NotificationData;

/**
 * Created by ajw45 on 2016-01-21.
 */
public class AlarmFragment extends BaseFragment implements DataUpdateCallback {


    private static final int LAYOUT_RESOURCE = R.layout.fragment_alarm;

    private RecyclerView alarmList;
    private LinearLayoutManager llManager;
    private AlarmAdapter alarmAdapter;
    private ImageView emptyView;

    private NotificationData notificationData;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setLayoutResourceId(LAYOUT_RESOURCE);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void init() {
        llManager = new LinearLayoutManager(getContext());
        notificationData = NotificationData.getInstance(getContext());
        notificationData.setCallback(this);
        alarmAdapter = notificationData.getAdapter();
    }

    @Override
    public void initView() {
        alarmList = (RecyclerView) findViewById(R.id.alarm_list);
        alarmList.setLayoutManager(llManager);
        alarmList.setAdapter(alarmAdapter);
        emptyView = (ImageView) findViewById(R.id.empty_view);
    }

    @Override
    public void onResume() {
        super.onResume();
        notificationData.updateData();
    }

    @Override
    public void onUpdated() {
        emptyView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onFailed() {
        emptyView.setVisibility(View.VISIBLE);
    }
}
