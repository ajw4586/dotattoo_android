package com.dotattoo.android.tab.mypage;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dotattoo.android.R;
import com.dotattoo.android.adapter.TattooRequestAdapter;
import com.dotattoo.android.core.base.BaseFragment;
import com.dotattoo.android.data.DataUpdateCallback;
import com.dotattoo.android.data.TattooRequestData;

/**
 * A simple {@link Fragment} subclass.
 */
public class TattooRequestFragment extends BaseFragment implements DataUpdateCallback {


    private static final int LAYOUT_RESOURCE = R.layout.fragment_tattoo_request;

    private RecyclerView list;
    private TattooRequestAdapter adapter;
    private LinearLayoutManager llManager;

    private TattooRequestData requestData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setLayoutResourceId(LAYOUT_RESOURCE);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateList();
    }

    @Override
    public void init() {
        requestData = TattooRequestData.getInstance(getContext());
        requestData.setCallback(this);
        adapter = requestData.getAdapter();
        llManager = new LinearLayoutManager(getContext());
    }

    @Override
    public void initView() {
        list = (RecyclerView) findViewById(R.id.request_list);
        list.setLayoutManager(llManager);
        list.setAdapter(adapter);
    }

    void updateList() {
        requestData.updateData();
    }

    @Override
    public void onUpdated() {

    }

    @Override
    public void onFailed() {

    }
}
