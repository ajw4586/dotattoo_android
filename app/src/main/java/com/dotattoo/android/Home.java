package com.dotattoo.android;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dotattoo.android.adapter.viewpager.HomePageAdapter;
import com.dotattoo.android.core.base.BaseActivity;
import com.dotattoo.android.net.req.MemberService;
import com.dotattoo.android.net.res.Profile;
import com.dotattoo.android.pref.AccountPref;
import com.dotattoo.android.pref.SessionPref;
import com.dotattoo.android.push.RegistrationTokenService;
import com.dotattoo.android.tab.home.TimelineFragment;
import com.dotattoo.android.util.OnBackKeyPressedListener;
import com.dotattoo.android.util.Utils;
import com.dotattoo.android.util.view.ModableViewPager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Home extends BaseActivity implements TimelineFragment.OnMenuFABStateChangeListener,
        View.OnClickListener {


    private static final int LAYOUT_RESOURCE = R.layout.activity_home;

    private Toolbar toolbar;
    private TextView toolbar_title; 
    private TabLayout tab_container;
    private ModableViewPager content;
    private ImageView setting;

    private HomePageAdapter pageAdapter;

    public static final String[] TAB_TITLE = {"AT THE REAL", "FIND PRO", "DOTATTOO", "ALARM", ""};
    public static final int[] tabIconResources = {
            R.drawable.tab_home,
            R.drawable.tab_find,
            R.drawable.tab_qna,
            R.drawable.tab_alarm,
            R.drawable.tab_mypage
    };

    private OnBackKeyPressedListener backPressedListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setLayoutResourceId(LAYOUT_RESOURCE);
        activeFresco(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void init() {
        pageAdapter = new HomePageAdapter(getSupportFragmentManager(), this);
        saveUserInfo();

        //  Check GCM is available.
        if(Utils.checkPlayServiceAvailable(this)) {
            new RegistrationTokenService.RegisterThread(this).start();
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(Home.this, "기기에서 푸시 서비스를 사용핤 수 없습니다.", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    //  TEMPORARY SAVE USER INFORMATION FOR USER_TYPE
    void saveUserInfo() {
        MemberService.getInstance().getUserInfo(SessionPref.getInstance(this).getUserId())
                .enqueue(new Callback<Profile>() {
                    @Override
                    public void onResponse(Call<Profile> call, Response<Profile> response) {
                        AccountPref.getInstance(Home.this).saveUserInfo(response.body());
                    }

                    @Override
                    public void onFailure(Call<Profile> call, Throwable t) {}
                });
    }

    @Override
    public void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setting = (ImageView) findViewById(R.id.toolbar_setting);
        setting.setOnClickListener(this);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar_title.setTextSize(20);
        toolbar_title.setText(TAB_TITLE[0]);
        toolbar_title.setTypeface(null, Typeface.BOLD);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        getSupportActionBar().hide();

        content = (ModableViewPager) findViewById(R.id.content);
        content.setAdapter(pageAdapter);

        //  register fab state listener
        ((TimelineFragment) pageAdapter.getItem(0)).addOnMenuFABStateChangeListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toolbar_setting:
                startActivity(new Intent(this, Setting.class));
                break;
        }
    }

    @Override
    public void onMenuFABStateChanged(boolean isActive) {
        if(isActive) {
            //  make view not to allow touch
            content.setTouchMode(false);
            tab_container.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    Log.d(getClass().getSimpleName(), "Tab clicked.");
                    return true;
                }
            });
        } else {
            //  remove all constraints
            content.setTouchMode(true);
            tab_container.setOnTouchListener(null);
        }
    }

    @Override
    public void initMenu() {
        tab_container = (TabLayout) findViewById(R.id.tab_container);
        View[] tabView = new View[5];
        TextView tabLabel[] = new TextView[5];
        ImageView tabIcon[] = new ImageView[5];
        for(int i = 0; i < 5; i++) {
            //  dotattoo tab
            if(i == 2) {
                tabView[i] = LayoutInflater.from(this).inflate(R.layout.layout_tab_no_text, null);
                continue;
            }
            tabView[i] = LayoutInflater.from(this).inflate(R.layout.layout_main_tab, null);
            tabLabel[i] = (TextView) tabView[i].findViewById(R.id.main_tab_label);
            tabIcon[i] = (ImageView) tabView[i].findViewById(R.id.main_tab_icon);
        }
        String labels[] = getResources().getStringArray(R.array.tab_name);

        tab_container.setupWithViewPager(content);
        for(int i = 0; i < 5; i++) {
            //  dotattoo tab
            if(i == 2) {
                tab_container.getTabAt(i).setCustomView(tabView[i]);
                continue;
            }
            //  Set each tab's value.
            tabLabel[i].setText(labels[i]);
            tabIcon[i].setImageResource(tabIconResources[i]);

            //  Add customized view
            tab_container.getTabAt(i).setCustomView(tabView[i]);
        }
        tab_container.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(content) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);
                toolbar_title.setText(TAB_TITLE[tab.getPosition()]);
                if(tab.getPosition() < 2) {
                    getSupportActionBar().hide();
                } else {
                    getSupportActionBar().show();
                }
                if(tab.getPosition() == 4 && setting.getVisibility() == View.GONE) {
                    setting.setVisibility(View.VISIBLE);
                } else {
                    setting.setVisibility(View.GONE);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                super.onTabUnselected(tab);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                super.onTabReselected(tab);
                //  scroll to top
                if(tab.getPosition() == 0) {
                    ((TimelineFragment) pageAdapter.getItem(0)).scrollToTop();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        backPressedListener = null;
    }

    @Override
    public void onBackPressed() {
        if(backPressedListener != null) {
            backPressedListener.onBackKeyPressed();
        }
    }

    public void normalBackPress() {
        super.onBackPressed();
    }

    public void setOnKeyBackPressedListener(OnBackKeyPressedListener listener) {
        this.backPressedListener = listener;
    }
}
