package com.dotattoo.android.util.notification;

import android.app.KeyguardManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.dotattoo.android.Chat;
import com.dotattoo.android.Home;
import com.dotattoo.android.PostDetail;
import com.dotattoo.android.R;

/**
 * Created by ajw45 on 2016-04-04.
 */
public class PushNotificationFactory {

    public static Notification createChatNotification(Context context,
                                                      String userName,
                                                      String message) {
        Intent chat = new Intent(context, Chat.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(Home.class);
        stackBuilder.addNextIntent(chat);
        PendingIntent resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(userName + "님이 " + context.getString(R.string.receive_new_chat_message))
                .setContentText(message)
                .setContentIntent(resultIntent);
        return builder.build();
    }

    public static Notification createCommentNotification(Context context, String userName) {
        Intent post = new Intent(context, PostDetail.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(Home.class);
        stackBuilder.addNextIntent(post);
        PendingIntent resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(userName + "님이 " + context.getString(R.string.receive_new_comment))
                .setContentIntent(resultIntent);
        return builder.build();
    }

    public static Notification createAdNotification(Context context, String push_message,
                                                    String contentMessage) {
        Intent app = new Intent(context, Home.class);
        PendingIntent intent = PendingIntent.getActivity(context, 0, app, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(push_message)
                .setContentText(contentMessage)
                .setVisibility(Notification.VISIBILITY_PUBLIC)
                .setPriority(Notification.PRIORITY_HIGH)
                .setVibrate(new long[] { 1000, 1000 })
                .setContentIntent(intent);
        return builder.build();
    }


    public static class PushWakeLock {
        private static PowerManager.WakeLock sCpuWakeLock;
        private static KeyguardManager.KeyguardLock mKeyguardLock;
        private static boolean isScreenLock;

        public static void acquireCpuWakeLock(Context context) {
            Log.e("PushWakeLock", "Acquiring cpu wake lock");
            Log.e("PushWakeLock", "wake sCpuWakeLock = " + sCpuWakeLock);

            if (sCpuWakeLock != null) {
                return;
            }
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            sCpuWakeLock = pm.newWakeLock(
                    PowerManager.SCREEN_BRIGHT_WAKE_LOCK |
                            PowerManager.ACQUIRE_CAUSES_WAKEUP |
                            PowerManager.ON_AFTER_RELEASE, "hello");

            sCpuWakeLock.acquire();
        }

        public static void releaseCpuLock() {
            Log.e("PushWakeLock", "Releasing cpu wake lock");
            Log.e("PushWakeLock", "relase sCpuWakeLock = " + sCpuWakeLock);
            Log.e("PushWakeLock", "relase sCpuWakeLock = " + sCpuWakeLock);

            if (sCpuWakeLock != null) {
                sCpuWakeLock.release();
                sCpuWakeLock = null;
            }
        }
    }

}
