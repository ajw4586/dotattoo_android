package com.dotattoo.android.util;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;

/**
 * Created by ajw45 on 2016-03-12.
 */
public class ScrollDisabler implements RecyclerView.OnItemTouchListener {

    private boolean isScrollable = true;

    public void setScrollable(boolean mode) {
        this.isScrollable = mode;
        Log.d(getClass().getSimpleName(), "setScrollable: " + isScrollable);
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return !isScrollable;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }
}
