package com.dotattoo.android.util;

/**
 * Created by ky8032 on 2016. 4. 15..
 */
public class FinishTrigger {

    public static interface OnConfirmListener {
        public void onConfirm();
    }

    private static FinishTrigger instance;
    private static OnConfirmListener sListener;

    private FinishTrigger() {
        instance = this;
    }

    public static FinishTrigger getInstance() {
        return instance != null ? instance : new FinishTrigger();
    }

    public void registerFinishTrigger(OnConfirmListener listener) {
        sListener = listener;
    }

    public OnConfirmListener getListener() {
        return sListener;
    }
}
