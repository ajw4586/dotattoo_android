package com.dotattoo.android.util;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by ajw45 on 2016-02-18.
 */
public class TimeFormatter {


    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;

    public static final int SEC = 60;
    public static final int MIN = 60;
    public static final int HOUR = 24;
    public static final int DAY = 30;
    public static final int MONTH = 12;

    public static String makePretty(String time) {
        return getTimeAgo(convertStringToTimeMillis(time));
    }

//    public static String getTimeAgo(long time) {
//        if (time < 1000000000000L) {
//            // if timestamp given in seconds, convert to millis
//            time *= 1000;
//        }
//
//        long now = new Date().getTime();
//        if (time > now || time <= 0) {
//            return null;
//        }
//
//        // TODO: localize
//        final long diff = now - time;
//        if (diff < MINUTE_MILLIS) {
//            return "방금";
//        } else if (diff < 50 * MINUTE_MILLIS) {
//            return diff / MINUTE_MILLIS + "분 전";
//        } else if (diff < 24 * HOUR_MILLIS) {
//            return diff / HOUR_MILLIS + "시간 전";
//        } else if (diff < 48 * HOUR_MILLIS) {
//            return "어제";
//        } else {
//            return diff / DAY_MILLIS + "일 전";
//        }
//    }

    public static Date convertStringToTimeMillis(String time) {
        Date result = null;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
//        formatter.setTimeZone(TimeZone.getDefault());
        try {
            Calendar cal = GregorianCalendar.getInstance();
            cal.setTime(new Date(formatter.parse(time).getTime() + TimeZone.getDefault().getRawOffset()));
            result = cal.getTime();
        } catch (ParseException e) {}
        return result;
    }

    public static String getTimeAgo(Date tempDate) {

        long curTime = System.currentTimeMillis();
        long regTime = tempDate.getTime();
        long diffTime = (curTime - regTime) / 1000;

        String msg = null;
        if (diffTime < SEC) {
            // sec
            msg = "방금 전";
        } else if ((diffTime /= SEC) < MIN) {
            // min
            msg = diffTime + "분 전";
        } else if ((diffTime /= MIN) < HOUR) {
            // hour
            msg = (diffTime) + "시간 전";
        } else if ((diffTime /= HOUR) < DAY) {
            // day
            msg = (diffTime) + "일 전";
        } else if ((diffTime /= DAY) < MONTH) {
            // day
            msg = (diffTime) + "달 전";
        } else {
            msg = (diffTime) + "년 전";
        }

        return msg;
    }

    public static Calendar parseDateString(String dateString) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try {
            cal.setTime(format.parse(dateString));
        } catch (ParseException e) {
            Log.d("TimeFormatter", "parseDateString: Error while parsing the date");
        }
        return cal;
    }

}
