package com.dotattoo.android.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.backends.pipeline.PipelineDraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import jp.wasabeef.fresco.processors.BlurPostprocessor;

/**
 * Created by ajw45 on 2016-03-22.
 */
public class ImageUtils {

    public static File saveImage(Context context, Uri imgUri) {
        //  save cropped image
        File f = new File(getDirPath(), new Date().toString() + ".png");
        FileOutputStream out = null;
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(),imgUri);
            out = new FileOutputStream(f);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return f;
    }

    public static File getDirPath() {
        if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/dotattoo");
            if(dir.exists()) {
                return dir;
            } else {
                dir.mkdirs();
                return dir;
            }
        }
        return null;
    }

    public static Bitmap convertDrawable(Drawable d) {
        Bitmap bitmap = null;

        if (d instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) d;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(d.getIntrinsicWidth() <= 0 || d.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(d.getIntrinsicWidth(), d.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        d.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        d.draw(canvas);
        return bitmap;
    }

    static ImageRequest makeBlurImageRequest(Context c, String url, int width, int height) {
        if(width != 0 && height != 0) {
            return ImageRequestBuilder.fromRequest(makeResizeImageRequest(url, width, height))
                    .setPostprocessor(new BlurPostprocessor(c))
                    .build();
        }
        return ImageRequestBuilder.newBuilderWithSource(Utils.createImageURI(url))
                .setPostprocessor(new BlurPostprocessor(c))
                .build();
    }

    static ImageRequest makeResizeImageRequest(String url, int width, int height) {
        return ImageRequestBuilder.newBuilderWithSource(Utils.createImageURI(url))
                .setResizeOptions(new ResizeOptions(width, height))
                .build();
    }

    public static PipelineDraweeController requestBlurImage(Context c, String url,
                                                            SimpleDraweeView iv) {
        return (PipelineDraweeController) Fresco.newDraweeControllerBuilder()
                .setImageRequest(makeBlurImageRequest(c, url, 0, 0))
                .setOldController(iv.getController())
                .build();
    }

    public static PipelineDraweeController requestBlurImage(Context c,String url,
                                                            int width, int height,
                                                            SimpleDraweeView iv) {
        return (PipelineDraweeController) Fresco.newDraweeControllerBuilder()
                .setImageRequest(makeBlurImageRequest(c, url, width, height))
                .setOldController(iv.getController())
                .build();
    }

    public static PipelineDraweeController requestResizedImage(String url, int width, int height) {
        return (PipelineDraweeController) Fresco.newDraweeControllerBuilder()
                .setImageRequest(makeResizeImageRequest(url, width, height))
                .build();
    }

}
