package com.dotattoo.android.util.view;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

import com.facebook.drawee.drawable.ProgressBarDrawable;

/**
 * Created by ajw45 on 2016-03-27.
 */
public class CircleProgressDrawable extends ProgressBarDrawable {

    float level;

    Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

    int color = 0x26272C;

    final RectF oval = new RectF();

    int radius = 50;

    public CircleProgressDrawable(){
        paint.setStrokeWidth(10);
        paint.setStyle(Paint.Style.STROKE);
    }

    @Override
    protected boolean onLevelChange(int level) {
        this.level = level;
        invalidateSelf();
        return true;
    }

    @Override
    public void draw(Canvas canvas) {
        oval.set(canvas.getWidth() / 2 - radius, canvas.getHeight() / 2 - radius,
                canvas.getWidth() / 2 + radius, canvas.getHeight() / 2 + radius);

        drawCircle(canvas, 1, color);
        drawCircle(canvas, level, 0x800080FF);
    }


    private void drawCircle(Canvas canvas, float level, int color) {
        paint.setColor(color);
        float angle;
        angle = 360 / 1f;
        angle = level * angle;
        canvas.drawArc(oval, 0, Math.round(angle), false, paint);
    }

}
