package com.dotattoo.android.util.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by ajw45 on 2016-03-12.
 */
public class ModableViewPager extends ViewPager {


    private boolean isTouchable = true;

    public ModableViewPager(Context context) {
        super(context);
    }

    public ModableViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setTouchMode(boolean mode) {
        isTouchable = mode;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if(isTouchable) {
            return super.onInterceptTouchEvent(ev);
        }
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if(isTouchable) {
            return super.onTouchEvent(ev);
        }
        return false;
    }
}
