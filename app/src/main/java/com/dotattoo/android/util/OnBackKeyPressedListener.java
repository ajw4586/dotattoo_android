package com.dotattoo.android.util;

/**
 * Created by ajw45 on 2016-06-10.
 */
public interface OnBackKeyPressedListener {

    public void onBackKeyPressed();
}
