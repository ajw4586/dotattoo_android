package com.dotattoo.android.util;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Build;
import android.util.Base64;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;

import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * Created by ajw45 on 2016-03-25.
 */
public class Utils {


    public static final int REQUEST_PERMISSION_CODE = 11;
    public static final int PLAY_SERVICE_REQUEST_CODE = 12;
    private static final String TEXT_PART = "text/plain";
    private static final String PHOTO_PART = "image/jpg";

    public static boolean checkPermission(Activity activity) {
        if(Build.VERSION.SDK_INT < 23) {
            return true;
        }
        if(activity.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            activity.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_PERMISSION_CODE);
            return false;
        }
    }

    public static Uri createImageURI(String url) {
        return Uri.parse(url);
    }

    public static boolean checkPlayServiceAvailable(Activity activity) {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);
        if(resultCode != ConnectionResult.SUCCESS) {
            if(GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, activity,
                        PLAY_SERVICE_REQUEST_CODE).show();
            }
            return false;
        }
        return true;
    }


    public static RequestBody createTextRequestBody(String message) {
        return RequestBody.create(MediaType.parse(TEXT_PART), message);
    }

    public static RequestBody createPhotoRequestBody(File photoFile) {
        return RequestBody.create(MediaType.parse(PHOTO_PART), photoFile);
    }


    public  static String formatWon(String string) {
        DecimalFormat format = new DecimalFormat("##,###");
        return format.format(Integer.parseInt(string));
    }

    public static String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            Log.e("Package Name=", context.getApplicationContext().getPackageName());

            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.e("Key Hash=", key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        }
        catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return key;
    }

    public static String createPostAppLink(String post_id) {
        return "dotattoo://post?post_id=" + post_id;
    }

}
