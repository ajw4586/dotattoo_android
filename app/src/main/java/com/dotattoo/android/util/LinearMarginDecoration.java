package com.dotattoo.android.util;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;

/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class LinearMarginDecoration extends RecyclerView.ItemDecoration {


    private static final int ROW_MARGIN_SIZE = 45;

    private int gap;

    public LinearMarginDecoration(int gap) {
        this.gap = gap;
    }

    public LinearMarginDecoration() {
        gap = ROW_MARGIN_SIZE;
    }

    @Override
    public void getItemOffsets(Rect outRect, int itemPosition, RecyclerView parent) {
        outRect.set(0, 0, 0, gap);
    }
}