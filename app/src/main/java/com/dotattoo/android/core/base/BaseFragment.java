package com.dotattoo.android.core.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by ajw45 on 2016-01-20.
 */
public abstract class BaseFragment extends Fragment {


    private int resId = 0;
    protected View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(resId <= 0)
            throw new RuntimeException("Fragment's layout resource id must be set up before inflate layout.");
        rootView = inflater.inflate(resId, container, false);
        init();
        initView();
        initMenu();
        return rootView;
    }

    public void setLayoutResourceId(int resId) {
        this.resId = resId;
    }

    public View findViewById(int id) {
        return rootView.findViewById(id);
    }

    public abstract void init();
    public abstract void initView();

    public void initMenu() {}
}
