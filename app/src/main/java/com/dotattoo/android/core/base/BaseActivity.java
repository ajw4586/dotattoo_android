package com.dotattoo.android.core.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.dotattoo.android.core.CommonApp;
import com.facebook.drawee.backends.pipeline.Fresco;

/**
 * Created by ajw45 on 2016-01-20.
 */
public abstract class BaseActivity extends AppCompatActivity {


    private int resId = 0;
    private boolean fresco = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CommonApp.setCurrentActivity(this);
        if(resId <= 0) {
            throw new RuntimeException("Activity's layout resource id must be set up before inflate layout.");
        }
        if(fresco) {
            Fresco.initialize(this);
        }
        setContentView(resId);
        init();
        initView();
        initMenu();
    }

    public void setLayoutResourceId(int id) {
        this.resId = id;
    }

    public abstract void init();
    public abstract void initView();
    public void initMenu() {}

    public void activeFresco(boolean state) {
        fresco = state;
    }
}
