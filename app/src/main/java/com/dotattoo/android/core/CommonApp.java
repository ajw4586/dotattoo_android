package com.dotattoo.android.core;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import com.dotattoo.android.data.TimelineData;
import com.dotattoo.android.data.UserInfoData;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.kakao.auth.ApprovalType;
import com.kakao.auth.AuthType;
import com.kakao.auth.IApplicationConfig;
import com.kakao.auth.ISessionConfig;
import com.kakao.auth.KakaoAdapter;
import com.kakao.auth.KakaoSDK;

/**
 * Created by ajw45 on 2016-01-20.
 */
public class CommonApp extends Application {

    private static Context globalContext;
    private static Activity currentActivity;

    @Override
    public void onCreate() {
        super.onCreate();
        globalContext = getApplicationContext();
        //  init sdk
        KakaoSDK.init(new KakaoSDKAdapter());
        FacebookSdk.sdkInitialize(globalContext);
        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();

        //  clear the static timeline and user info data
        TimelineData.clear();
        UserInfoData.clear();

        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }

    public static Context getGlobalApplicationContext() {
        return globalContext;
    }

    public static Activity getCurrentActivity() {
        return currentActivity;
    }

    // Activity가 올라올때마다 Activity의 onCreate에서 호출해줘야한다.
    public static void setCurrentActivity(Activity currentActivity) {
        CommonApp.currentActivity = currentActivity;
    }

    private static class KakaoSDKAdapter extends KakaoAdapter {
        /**
         * Session Config에 대해서는 default값들이 존재한다.
         * 필요한 상황에서만 override해서 사용하면 됨.
         * @return Session의 설정값.
         */
        @Override
        public ISessionConfig getSessionConfig() {
            return new ISessionConfig() {
                @Override
                public AuthType[] getAuthTypes() {
                    return new AuthType[] {AuthType.KAKAO_TALK  };
                }

                @Override
                public boolean isUsingWebviewTimer() {
                    return false;
                }

                @Override
                public ApprovalType getApprovalType() {
                    return ApprovalType.INDIVIDUAL;
                }

                @Override
                public boolean isSaveFormData() {
                    return true;
                }
            };
        }

        @Override
        public IApplicationConfig getApplicationConfig() {
            return new IApplicationConfig() {
                @Override
                public Activity getTopActivity() {
                    return CommonApp.getCurrentActivity();
                }

                @Override
                public Context getApplicationContext() {
                    return CommonApp.getGlobalApplicationContext();
                }
            };
        }
    }


}
