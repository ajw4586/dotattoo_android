package com.dotattoo.android.pref;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by ajw45 on 2016-04-07.
 */
public class GlobalPref {

    private static final String GLOBAL = "global";
    private static final String REGISTRATION_ID = "registration_id";

    private static GlobalPref instance;
    private static Context context;
    private static SharedPreferences pref;

    private GlobalPref(Context c) {
        context = c;
        pref = c.getSharedPreferences(GLOBAL, Context.MODE_PRIVATE);
        instance = this;
    }

    private GlobalPref updateInstance(Context c) {
        instance.context = c;
        return instance;
    }

    public static final GlobalPref getInstance(Context c) {
        return instance != null ? instance.updateInstance(c) : new GlobalPref(c);
    }

    public final boolean saveRegistrationID(String reg_id) {
        SharedPreferences.Editor edit = pref.edit();
        edit.putString(REGISTRATION_ID, reg_id);
        return edit.commit();
    }

    public final boolean clear() {
        return pref.edit().clear().commit();
    }
}
