package com.dotattoo.android.pref;

import android.content.Context;
import android.content.SharedPreferences;

import com.dotattoo.android.net.res.Session;

/**
 * Created by ajw45 on 2016-02-02.
 */
public class SessionPref {

    private static final String SESSION = "session";
    private static final String USER_ID = "user_id";
    private static final String ACCESS_TOKEN = "access_token";

    private static SessionPref instance;
    private static Context sContext;
    private static SharedPreferences sPref;

    public static SessionPref getInstance(Context context) {
        return instance != null ? instance : new SessionPref(context);
    }

    private SessionPref(Context context) {
        sContext = context;
        sPref = sContext.getSharedPreferences(SESSION, Context.MODE_PRIVATE);
        instance = this;
    }

    public boolean saveSession(Session sessionInfo) {
        SharedPreferences.Editor edit = sPref.edit();
        edit.putString(USER_ID, sessionInfo.user_id);
        edit.putString(ACCESS_TOKEN, sessionInfo.access_token);
        return edit.commit();
    }

    public boolean clear() {
        return sPref.edit().clear().commit();
    }

    public String getAccessToken() {
        return sPref.getString(ACCESS_TOKEN, "");
    }

    public String getUserId() {
        return sPref.getString(USER_ID, "");
    }

}
