package com.dotattoo.android.pref;

import android.content.Context;
import android.content.SharedPreferences;

import com.dotattoo.android.net.res.Profile;

/**
 * Created by ajw45 on 2016-03-01.
 */
public class AccountPref {

    private static AccountPref instance;

    public static final String USER = "user";
    public static final String TATTOOIST = "tattooist";

    private static final String PREFERENCE = "account_pref";
    private static final String NAME = "name";
    private static final String COMMENT = "comment";
    private static final String PLACE = "place";
    private static final String PHONE = "phone";
    private static final String PROFILE_IMG = "profile_img";
    private static final String TYPE = "type";
    private static final String PUSH_ALARM = "push_alarm";

    private SharedPreferences pref;

    private AccountPref(Context context) {
        pref = context.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
        instance = this;
    }

    public static AccountPref getInstance(Context context) {
        return instance != null ? instance : new AccountPref(context);
    }

    public void saveUserInfo(Profile info) {
        SharedPreferences.Editor edit = pref.edit();
        edit.putString(NAME, info.name);
        edit.putString(PROFILE_IMG, info.profile_img);
        edit.putString(TYPE, info.type);
        try {
            edit.putString(COMMENT, info.tattooist.comment);
        } catch (NullPointerException e) {}
        try {
            edit.putString(PLACE, info.tattooist.place);
        } catch (NullPointerException e) {}
        try {
            edit.putString(PHONE, info.phone);
        } catch (NullPointerException e) {}
        edit.apply();
    }

    public Profile getUserInfo() {
        Profile info = new Profile();
        info.name = pref.getString(NAME, "");
        info.type = pref.getString(TYPE, "");
        try {
            info.tattooist.comment = pref.getString(COMMENT, "");
        } catch (NullPointerException e) {}
        try {
            info.tattooist.place = pref.getString(PLACE, "");
        } catch (NullPointerException e) {}
        try {
            info.phone = pref.getString(PHONE, "");
        } catch (NullPointerException e) {}

        info.profile_img = pref.getString(PROFILE_IMG, "");
        return info;
    }

    public boolean savePushAlarmState(boolean enabled) {
        return pref.edit().putBoolean(PUSH_ALARM, enabled).commit();
    }

    public boolean getPushAlarmState() {
        return pref.getBoolean(PUSH_ALARM, true);
    }

    public boolean clear() {
        return pref.edit().clear().commit();
    }
}
