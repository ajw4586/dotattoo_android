package com.dotattoo.android;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.dotattoo.android.core.base.BaseActivity;
import com.dotattoo.android.util.Utils;
import com.facebook.drawee.view.SimpleDraweeView;

import java.io.File;

public class ReservationResult extends BaseActivity {


    private static final int LAYOUT_RESOURCE = R.layout.activity_reservation_result;

    public static final String PRICE = "price";
    public static final String TATTOO_NAME = "tattoo_name";
    public static final String PHOTO = "photo";

    private Button ok;
    private TextView price;
    private TextView tattooName;
    private SimpleDraweeView tattooPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setLayoutResourceId(LAYOUT_RESOURCE);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void init() {
    }

    @Override
    public void initView() {
        ok = (Button) findViewById(R.id.reservation_result_ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tattooName = (TextView) findViewById(R.id.reservation_result_tattoo_name);
        tattooPhoto = (SimpleDraweeView) findViewById(R.id.photo);
        price = (TextView) findViewById(R.id.reservation_result_price);

        getReservationData();
    }

    void getReservationData() {
        Intent data = getIntent();
        if(data != null) {
            tattooPhoto.setImageURI(Utils.createImageURI(data.getStringExtra(PHOTO)));
            price.setText(data.getCharSequenceExtra(PRICE));
            tattooName.setText(data.getCharSequenceExtra(TATTOO_NAME));
        } else {
            Toast.makeText(ReservationResult.this, "문제가 발생했습니다.", Toast.LENGTH_SHORT).show();
        }
    }
}
